<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
		
		<script type="text/javascript">
            jQuery(document).ready(function() {

                //show filter on click
                jQuery('.filter-button').click(function(e) {
                    e.preventDefault();
                    if(jQuery(this).hasClass('closed')) {
                        jQuery('.filter-dropdown').hide();
                        jQuery(this.rel).slideDown(200);
                        jQuery(this).toggleClass('closed open');
                    } else {
                        jQuery(this.rel).slideUp(200);
                        jQuery(this).toggleClass('closed open');
                    }
                });

                //change calendar to next or previous month
                jQuery(document).on("click", '.load-cal', function(e) {
                    e.preventDefault();
                    jQuery('#gl-cal').load(jQuery(this).attr('href'));
                });
            });
        </script>
    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Daily Truth - Date Here</title>
	
</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>

  		
       
       
		<!-- Page Header
		============================================= -->
        <?php include "include/pageheader-dailytruth.php";?>

		<a name="anchor2"></a>
		
		<!-- Search Daily #Truth
		============================================= -->
        <section id="daily-search">
        	<div class="content-wrap toppadding-sm bottompadding-sm center">
      			<div class="container clearfix">
                   <!-- Brand and toggle get grouped for better mobile display -->
                    <div data-animate="fadeIn" class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
        
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div data-animate="fadeIn" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-center nobottommargin">
                        
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle tcolorgold" data-toggle="dropdown">This Week <b class="caret"></b></a>
							<ul class="dropdown-menu" role="menu">
                                <li>
								<?php
                                //include the function to draw the calendar
                                include 'func.php';
                                
                                //set current year and month
                                $current_month = date('n');
                                $current_year = date('Y');
                                
                                //spit out the calendar
                                echo'<div id="cal">' . draw_calendar($current_month,$current_year) . '</div>'; ?>
								</li>
							</ul>
								
							
                        </li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle tcolorgold" data-toggle="dropdown">All Types <b class="caret"></b></a>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Devotional</a></li>
                            <li><a href="#">Video</a></li>
                            <li><a href="#">Thought for the Day</a></li>
							<li><a href="#">Bible Verse</a></li>
                            <li><a href="#">Christian Music</a></li>
                            <li><a href="#">Article/Short Story</a></li>
							<li><a href="#">Testimony/Praise Report</a></li>
                            <li><a href="#">Image of the Day</a></li>
                          </ul>
                        </li>
                      </ul>
                      <form class="navbar-form navbar-center nomargin" role="search">
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="Search something specific?">
                        </div>
                        <button type="submit" class="button button-3d button-small">Submit</button>
                      </form>
                      
                    </div><!-- /.navbar-collapse -->
				</div>
			</div>
		</section><!-- #search end -->


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini bottommargin-sm">
            <div class="container clearfix">
                <ol class="breadcrumb">
                   	<li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">Encouragement</a></li>
                    <li><a href="daily-truth.php">Daily #Truth</a></li>
					<li class="active">September 25, 2015</li>
                </ol>
            </div>
        </section><!-- #page-title end -->
		
		<!-- Selections
        ============================================= -->
        <section id="content">

            <div class="content-wrap notoppadding nobottompadding notopmargin">
               


				<div class="container clearfix">
				
 					<!-- Pagination
                    ============================================= -->
                    <ul class="pager bottommargin-sm">
                        <li class="previous"><a href="daily-truth-single-day.php">&larr; September 25, 2015</a></li>
                        
                    </ul><!-- .pager end -->
					
					<div class="single-post nobottommargin">

                        <!-- Single Post
                        ============================================= -->
                        <div data-animate="fadeIn" class="entry border-full bottommargin clearfix">

							
                            <!-- Entry Title
                            ============================================= -->
                            <div class="entry-title">
                                <h2 class="tcolorltblue">Hope Thou In God! <span>- Pastor John Hagee</span></h2>
                            </div><!-- .entry-title end -->

                            
							<!-- Entry Meta
                            ============================================= -->
                            <ul class="entry-meta bottommargin-xsm clearfix">
                                <li><i class="icon-comments-alt"></i> Quote</li>
                            </ul><!-- .entry-meta end -->

                            <div class="clear"></div>

                            <!-- Post Single - Share
                            ============================================= -->
                            
							<!-- Entry Image
                            ============================================= -->
                          	<div class="entry-image bottommargin">
                                <img src="images/daily-truth-single-day-johnhagee.jpg" alt="John Hagee">
                            </div><!-- .entry-image end -->

                            <!-- Entry Content
                            ============================================= -->
                            <div class="entry-content notopmargin">
                                <blockquote>
                                    <p>"Hope Sees the invisible. Hope Feels the intangible. Hope Achieves the impossible. HOPE THOU IN GOD!"</p>
                                    <footer>John C. Hagee</footer>
                                </blockquote>
                                
                            </div>
							
							<div class="clear"></div>

							<div class="col_half nobottommargin fleft col_respon">
    							<div class="si-share clearfix">
                            	<span>Share this Daily #Truth:</span>
                                    <div>
                                        <a href="#" class="social-icon si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                                            <i class="icon-facebook"></i>
                                            <i class="icon-facebook"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                                            <i class="icon-twitter"></i>
                                            <i class="icon-twitter"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Pinterst">
                                            <i class="icon-pinterest"></i>
                                            <i class="icon-pinterest"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-gplus" data-toggle="tooltip" data-placement="top" title="Google Plus">
                                            <i class="icon-gplus"></i>
                                            <i class="icon-gplus"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-rss" data-toggle="tooltip" data-placement="top" title="RSS">
                                            <i class="icon-rss"></i>
                                            <i class="icon-rss"></i>
                                        </a>
                                        <a href="#" class="social-icon si-borderless si-email3" data-toggle="tooltip" data-placement="top" title="Email">
                                            <i class="icon-email3"></i>
                                            <i class="icon-email3"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div><!-- #posts end -->

                </div>

           	</div>
    </section><!-- #content end -->
            
	<div class="clear"></div>
            

        <!-- Footer
        ============================================= -->
        <?php include "include/footer.php";?>
            
	</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>