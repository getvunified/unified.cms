<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

	<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries - The Difference</title>
</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	<div id="side-panel" class="dark">

		<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	</div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">


        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
		
         <!-- Page Title
        ============================================= -->
        <section id="page-title">

            <div class="container clearfix">
                <h1>The Difference</h1>
                <span>There's so much more...</span>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li class="active">The Difference</li>
                </ol>
            </div>

        </section><!-- #page-title end -->
        
        <!-- Content
		============================================= -->
		<section id="content">
        
			<div class="section noborder notopmargin nopadding nobottommargin">
				<div class="container clearfix">
					
                    
                        <div class="heading-block noborder topmargin-sm">
                            <h1 class="tcolorgold">Matthew and Kendal Hagee</h1>
                            <span>The Difference</span>
                        </div>
                        <img src="images/The-Difference-325.jpg" alt="Sr. Pastor C. Hagee" class="alignleft" style="max-width: 275px;">
                        <span><strong>The Difference is a faith-based lifestyle show</strong> where biblical truths and everyday life intertwine in a fun and insightful way. On each episode Pastor Matthew and Kendal discuss trends, culture, and most importantly how faith applies to every person's life every day.</span>
                        <div>&nbsp;</div>
						
						<h3 class="tcolorgold"><strong>The Difference</strong> with Matthew and Kendal Hagee Wednesdays at 8:00pm CST on <a href="http://www.getv.org">GETV.org</a></h3>
                    
              </div>
				
				<div class="line"></div>
                           		<!-- JHM Connect
					============================================= -->
                    <?php include "include/jhm-connect.php";?>


        
        </section><!-- #content end -->

		<!-- Footer
		============================================= -->
        
					<!-- Footer
					============================================= -->
                    <?php include "include/footer.php";?>
                    
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>

</body>
</html>