<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Wholesale Policy</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       

		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li class="active">Wholesale Policy</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="content-wrap noborder topmargin nobottommargin nopadding">
	 
                <div data-animate="fadeIn" class="container bottommargin clearfix">

                    <div class="fancy-title title-bottom-border">
                        <h3>JHM Wholesale Policy</h3>
                    </div>

					<h4 class="tcolorgold heading">Eligibility</h4>
                    <p>JHM sells wholesale to brick and  mortar retail establishments or physical churches ONLY 
(collectively, “Retailers”). JHM does not sell wholesale to online retailers or online ministries.  </p>
					<p>JHM will need the following credentials prior to a Retailer’s initial purchase of JHM product and JHM reserves the right to periodically request such information for any additional purchases:</p>
					<ul class="iconlist">
                        <li><i class="icon-line-circle-check"></i> Current and valid business license </li>
                        <li><i class="icon-line-circle-check"></i> Current and valid resale certificate </li>
                        <li><i class="icon-line-circle-check"></i> Current and valid tax id number (or in the case of a church, exemption certificate) </li>
                        <li><i class="icon-line-circle-check"></i> A commercially zoned brick and mortar address of all store locations </li>
                    </ul>
					<p>It is understood that upon submission of these documents, JHM is given the full right to verify and inquire as to the authenticity and veracity of any and all information provided by the Retailer. Once JHM, to its full and complete satisfaction, has verified  Retailer’s  credentials, JHM will provide wholesale pricing information to Retailer at which point Retailer will be eligible to purchase JHM products at wholesale prices and resell them to Retailer’s customers. </p>

					<div class="line notopmargin bottommargin-sm"></div>

					<h4 class="tcolorgold heading">Approved Retailers must abide by the following terms and conditions</h4>
					<p>By purchasing JHM product, Retailer understands and agrees that it is a non-exclusive Retailer of JHM product. Retailer further understands and agrees that its purchase of JHM product for resale shall not constitute a sale of a franchise, distributorship, or security. Nothing contained in these terms and conditions shall be deemed or construed as creating a joint venture,  employer/employee relationship, agency or partnership between the Retailer and JHM. Neither party is authorized as an agent, employee or  legal representative of the other party. </p>

					<p>Retailer smay create advertising and/or promotional materials for JHM products that it has purchased by submitting a  written request of approval to JHM prior to use of such advertising and/or promotional materials. Retailer is prohibited from using JHM’s trade names, logos, copyrights, trademarks or service marks without the prior written consent from the Marketing Director of JHM.  </p>

					<ul class="iconlist">
                        <li><i class="icon-line-circle-check"></i> Retailer shall not sell JHM products below the JHM suggested retail price </li>
                        <li><i class="icon-line-circle-check"></i> Retailer shall not sell JHM products on any Internet auction website such as eBay or similar website </li>
                        <li><i class="icon-line-circle-check"></i> Retailer will acquire JHM products by placing orders in a form 
approved by JHM. JHM reserves the right to reject any order in its sole discretion </li>
                    </ul>

					<p>Sales, coupons, discounts or any other promotions (collectively “sales”) JHM runs for the benefit of its retail customers does not and shall not apply to Retailer purchases. Under no circumstances will such sales be applied to JHM’s wholesale prices.</p>
					<div class="line notopmargin bottommargin-sm"></div>

                    <h4 class="tcolorgold heading">Payment Terms</h4>
                    <p>Payment is due in full at time of order.</p>

					<div class="line notopmargin bottommargin-sm"></div>
 
                    <h4 class="tcolorgold heading">Returns</h4>
                    <p>Understand that any damaged, defective or non-conforming JHM product may be exchanged for the same product within ten (10) days of the product ship date (Under no circumstances will JHM be liable for any errors in order instructions provided by Retailer).</p>
                    <ul class="iconlist">
                        <li><i class="icon-line-circle-check"></i> Damaged or defective items must be sent back to JHM in order to process an exchange </li>
                        <li><i class="icon-line-circle-check"></i> JHM is not responsible for products returned for exchange that become lost in the mail </li>
                        <li><i class="icon-line-circle-check"></i> Prior to submitting any products for exchange, a Return Merchandise Authorization (RMA) number is required. To obtain your RMA number, please call Customer Service at (800)600-9743, Monday–Friday, 8:30-5:00 CST. Products received without an RMA number will be considered a donation to JHM Missions Outreach </li>
                    </ul>

					<p>Other than the exceptions set forth above, there are no returns and all sales are final.</p>

					<div class="line notopmargin bottommargin-sm"></div>

                    <h4 class="tcolorgold heading">Shipping</h4> 
                    <p>Please allow 1-3 weeks for processing of your order. Most orders will ship within two weeks, but due to seasonal fluctuations, in demand and peak ordering times right after new releases, this time may be extended. We will give you a tentative ship date for your order when you place it. Shipment and arrival dates cannot be guaranteed. </p>

					<div class="line notopmargin bottommargin-sm"></div>

                    <h4 class="tcolorgold heading">Termination</h4>
                    <p>JHM may terminate Retailer’s participation in the JHM Wholesale Program at any time without notice and with or without cause. </p>

					<div class="line notopmargin bottommargin-sm"></div>

                    <h4 class="tcolorgold heading">Billing Policy</h4>
                    <p><strong>We accept Visa, MasterCard, Discover, and American Express.</strong> All online orders are processed through 128 bit SSL encryption to protect your credit card information during your online transaction. All prices and figures are listed in US Dollars. </p>

					<div class="line notopmargin bottommargin-sm"></div>

                    <h4 class="tcolorgold heading">Non-disclosure and Confidentiality</h4>
                    <p>Retailer shall keep confidential and not use, copy or disclose, directly or indirectly, to any third party any 
confidential information provided to Retailer by JHM. Any and all confidential information provided hereunder shall be and remain the property of JHM. </p>
					
					<div class="line notopmargin bottommargin-sm"></div>

                    <h4 class="tcolorgold heading">Confidential Information</h4>
                    <p>For the purposes of this Wholesale Policy, the term “confidential information” shall include, without limitation, all trade secrets and all other information and material that relates or refers to the plans, policies, finances, corporate developments,  products, pricing, sales, services, procedures, suppliers, prospects and customers, as well as financial information relating to such suppliers, prospects and customers, and any other similar confidential information and material which JHM does not make generally available to the public. </p>

					<div class="line notopmargin bottommargin-sm"></div>

                    <p>The terms and conditions with respect to confidentiality and non-disclosure shall survive termination of this Wholesale Policy. JHM may amend or modify these terms and conditions in its sole discretion at any time and without notice. Any such changes to these terms and conditions shall apply as soon as they are posted online at <a href="http://www.jhm.org/">jhm.org.</a> Continued Retailer orders placed with JHM after any such changes are posted shall indicate Retailer’s acceptance of such changes. If you qualify under these terms and conditions, please email <a href="mailto:support@jhm.org">support@jhm.org</a> to request an application. </p>

					<div class="line notopmargin bottommargin-sm"></div>

					<h4 class="tcolorgold heading">My God richly bless you for your continued support of John Hagee Ministries and Our Mission to Take "All the Gospel to all the World and all Generations."</h4>
                    

				</div>
 
				<div class="clear"></div>

				<div class="container clearfix">
                  <!-- Post Content
                            ============================================= -->
       		
					<div class="col_one_third">
						<div class="feature-box fbox-center fbox-plain noborder">
							<div class="fbox-icon">
								<a href="#"><img src="images/contactus-us-flag.png" style="width:100%; height:100%;" alt="JHM Canada"></a>
							</div>
							<h3>United States</h3>
                            <div class="text-left leftmargin topmargin-sm">
								<div><i class="icon-phone"></i> 1-800-854-9899</div>
                            	<div><i class="icon-location"></i> P.O. Box 1400, San Antonto, Texas 78295</div>
							</div>
                            <a href="#" class="button button-3d button-large topmargin-sm">Contact Us</a>
						</div>
					</div>
					<div class="col_one_third">
						<div class="feature-box fbox-center fbox-plain noborder">
							<div class="fbox-icon">
								<a href="#"><img src="images/contactus-canada-flag.png" style="width:100%; height:100%;" alt="JHM Canada"></a>
							</div>
							<h3>Canada</h3>
                            <div class="text-left leftmargin topmargin-sm">
								<div><i class="icon-phone"></i> 1-877-454-6226</div>
                            	<div><i class="icon-location"></i> P.O. Box 9900 Toronto, Ontario M3C 2T9
</div>
							</div>
                            <a href="#" class="button button-3d button-large topmargin-sm">Contact Us</a>
						</div>
					</div>
					<div class="col_one_third col_last">
						<div class="feature-box fbox-center fbox-plain noborder">
							<div class="fbox-icon">
								<a href="#"><img src="images/contactus-uk-flag.png" style="width:100%; height:100%;" alt="JHM Canada"></a>
							</div>
							<h3>United Kingdom</h3>
                            <div class="text-left leftmargin topmargin-sm">
								<div><i class="icon-phone"></i> 44-1793-862146</div>
                            	<div><i class="icon-location"></i> P.O. Box 2959 Swindon, UK SN6 7WS</div>
							</div>
                            <a href="#" class="button button-3d button-large topmargin-sm">Contact Us</a>
						</div>
					</div>
				
				</div>


			</div>

            <div class="clear"></div>
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>