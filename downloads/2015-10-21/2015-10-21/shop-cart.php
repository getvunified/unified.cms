<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
                <link rel="stylesheet" href="css/shop.css" type="text/css" />
                <script type="text/javascript" src="js/shop.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

                    <!-- Top Links 
                    ============================================= -->
                    <div class="top-links">
                        <ul>
                            <li><a href="#"> <i class="icon-line2-user"></i>Login</a>
                                <div class="top-link-section">
                                    <form id="top-login" role="form">
                                        <div class="input-group" id="top-login-username">
                                            <span class="input-group-addon"><i class="icon-mail"></i></span>
                                            <input type="email" class="form-control" placeholder="Email address" required="">
                                        </div>
                                        <div class="input-group" id="top-login-password">
                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <label class="checkbox">
                                          <input type="checkbox" value="remember-me"> Remember me
                                        </label>
                                        <button class="btn btn-danger btn-block" type="submit">Sign in</button>
                                    </form>
                                </div>
                            </li>
							<li><a href="#"><i class="icon-key"></i> Register</a>
								<div class="top-link-section">
                                    <form id="top-register" role="form">
                                        <div class="input-group" id="top-register-name">
                                            <span class="input-group-addon"><i class="icon-line2-user"></i></span>
                                            <input type="name" class="form-control" placeholder="Name" required="">
                                        </div>
										<div class="input-group" id="top-register-username">
                                            <span class="input-group-addon"><i class="icon-mail"></i></span>
                                            <input type="email" class="form-control" placeholder="Email address" required="">
                                        </div>
                                        <div class="input-group" id="top-register-password">
                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <button class="btn btn-danger btn-block" type="submit">Register</button>
                                    </form>
                                </div>
							</li>
                        </ul>
                    </div> <!--.top-links end -->

                </div>

            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Slider
		============================================= -->
        <?php include "include/slider.php";?>


        <!-- Content
        ============================================= -->
        <section id="content">
            <div id="shop-nav">
                <ul>
                    <li><a rel="#featured" href="#">Featured</a></li>
                    <li class="active"><a class="tab" rel="#categories" href="#">Categories</a></li>
                    <li><a class="tab" rel="#authors" href="#">Authors</a></li>
                    <li><a class="tab" rel="#subjects" href="#">Subject</a></li>
                </ul>
            </div>
            <div class="noborder nomargin nopadding topmargin-sm bottommargin-sm">
                <div class="container clearfix">
                    <div class="shop-category noborder topmargin">
                        <div class="shop-category-header">
                            <h2>Shopping Cart</h2>
                            <div class="shop-item-sort">
                                <a href="/jhm/7.1">Home</a> / <a href="/jhm/7.1/shop.php">Shop</a> / Shopping Cart
                            </div>
                        </div>
                        <div class="shop-table-wrap">
                            <table class="shop-cart">
                                <thead>
                                    <tr>
                                        <th colspan="2">Product</th>
                                        <th>Media Type</th>
                                        <th>Quantity</th>
                                        <th>Gift</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="cart-img"><img src="images/shop/product-sample.jpg" /></td>
                                        <td class="cart-product">
                                            <div class="cart-title yellow">Sin, Sex &amp; Self-Control</div>
                                            <div class="cart-author">Pastor John Hagee</div>
                                        </td>
                                        <td class="cart-media">
                                            <div class="cart-media-type">Paperback</div>
                                            <div class="cart-media-num">Item Number: 8194</div>
                                        </td>
                                        <td class="cart-quantity">
                                            <input type="text" size="4" value="1" />
                                        </td>
                                        <td class="cart-gift">
                                            <input type="checkbox" />
                                        </td>
                                        <td class="cart-total">$12.99</td>
                                    </tr>
                                    <tr>
                                        <td class="cart-img"><img src="images/shop/product-sample.jpg" /></td>
                                        <td class="cart-product">
                                            <div class="cart-title yellow">Sin, Sex &amp; Self-Control</div>
                                            <div class="cart-author">Pastor John Hagee</div>
                                        </td>
                                        <td class="cart-media">
                                            <div class="cart-media-type">Paperback</div>
                                            <div class="cart-media-num">Item Number: 8194</div>
                                        </td>
                                        <td class="cart-quantity">
                                            <input type="text" size="4" value="1" />
                                        </td>
                                        <td class="cart-gift">
                                            <input type="checkbox" />
                                        </td>
                                        <td class="cart-total">$12.99</td>
                                    </tr>
                                    <tr>
                                        <td class="cart-img"><img src="images/shop/product-sample.jpg" /></td>
                                        <td class="cart-product">
                                            <div class="cart-title yellow">Sin, Sex &amp; Self-Control</div>
                                            <div class="cart-author">Pastor John Hagee</div>
                                        </td>
                                        <td class="cart-media">
                                            <div class="cart-media-type">Paperback</div>
                                            <div class="cart-media-num">Item Number: 8194</div>
                                        </td>
                                        <td class="cart-quantity">
                                            <input type="text" size="4" value="1" />
                                        </td>
                                        <td class="cart-gift">
                                            <input type="checkbox" />
                                        </td>
                                        <td class="cart-total">$12.99</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="cart-next">
                            <h3>What would you like to do next?</h3>
                            <p>Choose if you have a Promo Code, Donation or Gift Card or would like to estimate your delivery cost.</p>
                            <div class="slide-wrapper">
                                <div class="slide-group">
                                    <div class="slide-title">
                                        <h3>Use Promo Code</h3>
                                    </div>
                                    <div class="slide-content">
                                        Enter promo code: <input class="input-shop" type="text" placeholder="#000=000" /><input class="button button-blue button-small" type="submit" value="Apply Code" />
                                    </div>
                                </div>
                                <div class="slide-group">
                                    <div class="slide-title">
                                        <h3>One-time Donation</h3>
                                    </div>
                                    <div class="slide-content">
                                        Enter donation here: <input class="input-shop" type="text" placeholder="$50.00" /><input class="button button-blue button-small" type="submit" value="Apply Donation" />
                                    </div>
                                </div>
                                <div class="slide-group">
                                    <div class="slide-title">
                                        <h3>Estimate Shipping</h3>
                                    </div>
                                    <div class="slide-content">
                                        Enter zip code: <input class="input-shop" type="text" placeholder="12345" /><input class="button button-blue button-small" type="submit" value="Estimate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cart-summary">
                            <table class="cart-summary-table">
                                <tbody>
                                    <tr>
                                        <td>SUBTOTAL:</td>
                                        <td>$25.98</td>
                                    </tr>
                                    <tr>
                                        <td>COUPON:</td>
                                        <td>$0.00</td>
                                    </tr>
                                    <tr>
                                        <td>ONE TIME DONATION:</td>
                                        <td>$0.00</td>
                                    </tr>
                                    <tr>
                                        <td>SHIPPING:</td>
                                        <td>$7.99</td>
                                    </tr>
                                    <tr class="cart-total-row">
                                        <td>TOTAL:</td>
                                        <td>$33.97</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="cart-actions">
                            <a class="button button-blue button-3d button-large" href="shop.php"><span>Continue Shopping</span></a>
                            <a class="fright button button-3d button-large" href="shop-checkout.php"><span>Checkout</span></a>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>