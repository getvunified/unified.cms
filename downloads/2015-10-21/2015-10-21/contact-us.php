<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Contact Us</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-contactus.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">About</a></li>
                    <li class="active">Contact Us</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="section noborder notopmargin nobottommargin nobottompadding">
	 
                <div class="container clearfix">
                  <!-- Post Content
                            ============================================= -->
       		
					<div class="col_one_third">
						<div class="feature-box fbox-center fbox-plain noborder">
							<div class="fbox-icon">
								<a href="#"><img src="images/contactus-us-flag.png" style="width:100%; height:100%;" alt="JHM Canada"></a>
							</div>
							<h3>United States</h3>
                            <div class="text-left leftmargin topmargin-sm">
								<div><i class="icon-phone"></i> 1-800-854-9899</div>
                            	<div><i class="icon-location"></i> P.O. Box 1400, San Antonto, Texas 78295</div>
							</div>
                            <!--<a href="#" class="button button-3d button-large topmargin-sm">Contact Us</a>-->
						</div>
					</div>
					<div class="col_one_third">
						<div class="feature-box fbox-center fbox-plain noborder">
							<div class="fbox-icon">
								<a href="#"><img src="images/contactus-canada-flag.png" style="width:100%; height:100%;" alt="JHM Canada"></a>
							</div>
							<h3>Canada</h3>
                            <div class="text-left leftmargin topmargin-sm">
								<div><i class="icon-phone"></i> 1-877-454-6226</div>
                            	<div><i class="icon-location"></i> P.O. Box 9900 Toronto, Ontario M3C 2T9
</div>
							</div>
                            <!--<a href="#" class="button button-3d button-large topmargin-sm">Contact Us</a>-->
						</div>
					</div>
					<div class="col_one_third col_last">
						<div class="feature-box fbox-center fbox-plain noborder">
							<div class="fbox-icon">
								<a href="#"><img src="images/contactus-uk-flag.png" style="width:100%; height:100%;" alt="JHM Canada"></a>
							</div>
							<h3>United Kingdom</h3>
                            <div class="text-left leftmargin topmargin-sm">
								<div><i class="icon-phone"></i> 44-1793-862146</div>
                            	<div><i class="icon-location"></i> P.O. Box 2959 Swindon, UK SN6 7WS</div>
							</div>
                            <!--<a href="#" class="button button-3d button-large topmargin-sm">Contact Us</a>-->
						</div>
					</div>
				<form class="contact-page-form nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">

							<div class="form-process"></div>

							<div class="col_one_third">
								<label for="template-contactform-name">Full Name <small>*</small></label>
								<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />
							</div>

							<div class="col_one_third">
								<label for="template-contactform-email">Email <small>*</small></label>
								<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
							</div>

							<div class="col_one_third col_last">
                                                            <label for="template-contactform-service">Country <small>*</small></label>
                                                            <select id="template-contactform-service" name="template-contactform-service" class="sm-form-control required">
                                                                    <option value="">-- Select One --</option>
                                                                    <option value="Selection here">United States</option>
                                                                    <option value="Selection here">Canada</option>
                                                                    <option value="Selection here">United Kingdom</option>
                                                            </select>
                                                        </div>

							<div class="clear"></div>

							<div class="col_full">
								<label for="template-contactform-message">Your Message <small>*</small></label>
								<textarea class="required sm-form-control bottommargin-sm" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>

								<input type="checkbox" id="template-contactform-name" name="template-contactform-checkbox" value="" class="checkbox-inline" /> <label for="template-contactform-checkbox">Select if you like to be included in our Email Newsletter. </label>
							</div>

							<div class="col_full hidden">
								<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
							</div>

							<div class="col_full">
								<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Your Message</button>
							</div>
							<p>JHM respects your privacy. View our <a href="privacy-termsofuse.php">Privacy Policy</a>.</p>
						</form>
				</div>
			</div>

            <div class="clear"></div>
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>