 <div class="section noborder nomargin notoppadding">
    <div class="container center clearfix showside">
         <div class="heading-block noborder topmargin-sm bottommargin-sm">
            <h1>JHM Upcoming Events</h1>
            <p class="lead">John Hagee Ministries events that are sure to give you insight and faith. </p>
        </div>
        
        <div id="oc-portfolio" class="owl-carousel portfolio-carousel">
        
         	<div class="oc-item">
				<div class="iportfolio">
                    <div class="portfolio-image">
                        <img src="images/jhm-event-atsea.jpg" alt="John Hagee Minsitries">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3><a class="tcolorgold" href="#">JHM At Sea</a></h3>
                                <span class="bottommargin-sm">Free concert and spectacular fireworks display. Join us for incredible entertainment and a fireworks display that will make... </span>
                                <a href="#">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a class="tcolorblue" href="#">JHM At Sea</a></h3>
                        <p class="lead">August 16th, 2015</p>
                    </div>
                </div>
			</div>

            <div class="oc-item">
				<div class="iportfolio">
                    <div class="portfolio-image">
                        <img src="images/jhm-event-branson.jpg" alt="John Hagee Minsitries">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3><a class="tcolorgold" href="#">Branson Rally</a></h3>
                               <span class="bottommargin-sm">Free concert and spectacular fireworks display. Join us for incredible entertainment and a fireworks display that will make... </span>
                                <a href="#">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a class="tcolorblue" href="#">Branson Rally</a></h3>
                        <p class="lead">August 16th, 2015</p>
                    </div>
            	</div>
			</div>
            
			<div class="oc-item">
				<div class="iportfolio">
                    <div class="portfolio-image">
                        <img src="images/jhm-event-4th.jpg" alt="John Hagee Minsitries">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3><a class="tcolorgold" href="#">4th of July Celebration</a></h3>
                                <span class="bottommargin-sm">Free concert and spectacular fireworks display. Join us for incredible entertainment and a fireworks display that will make... </span>
                                <a href="#">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a class="tcolorblue" href="#">4th of July Celebration</a></h3>
                        <p class="lead">August 16th, 2015</p>
                    </div>
            	</div>
			</div>

           <div class="oc-item">
				<div class="iportfolio">
                    <div class="portfolio-image">
                        <img src="images/jhm-event-4th.jpg" alt="John Hagee Minsitries">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3><a class="tcolorgold" href="#">4th of July Celebration</a></h3>
                                <span class="bottommargin-sm">Free concert and spectacular fireworks display. Join us for incredible entertainment and a fireworks display that will make... </span>
                                <a href="#">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a class="tcolorblue" href="#">4th of July Celebration</a></h3>
                        <p class="lead">August 16th, 2015</p>
                    </div>
            	</div>
			</div> 

			<div class="oc-item">
				<div class="iportfolio">
                    <div class="portfolio-image">
                        <img src="images/jhm-event-atsea.jpg" alt="John Hagee Minsitries">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3><a class="tcolorgold" href="#">JHM At Sea</a></h3>
                                <span class="bottommargin-sm">Free concert and spectacular fireworks display. Join us for incredible entertainment and a fireworks display that will make... </span>
                                <a href="#">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a class="tcolorblue" href="#">JHM At Sea</a></h3>
                       <p class="lead">August 16th, 2015</p>
                    </div>
                </div>
			</div>

            <div class="oc-item">
				<div class="iportfolio">
                    <div class="portfolio-image">
                        <img src="images/jhm-event-branson.jpg" alt="John Hagee Minsitries">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3><a class="tcolorgold" href="#">Branson Rally</a></h3>
                                <span class="bottommargin-sm">Free concert and spectacular fireworks display. Join us for incredible entertainment and a fireworks display that will make... </span>
                                <a href="#">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a class="tcolorblue" href="#">Branson Rally</a></h3>
                        <p class="lead">August 16th, 2015</p>
                    </div>
            	</div>
			</div>
            
			<div class="oc-item">
				<div class="iportfolio">
                    <div class="portfolio-image">
                        <img src="images/jhm-event-4th.jpg" alt="John Hagee Minsitries">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3><a class="tcolorgold" href="#">4th of July Celebration</a></h3>
                               <span class="bottommargin-sm">Free concert and spectacular fireworks display. Join us for incredible entertainment and a fireworks display that will make... </span>
                                <a href="#">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a class="tcolorblue" href="#">4th of July Celebration</a></h3>
                       <p class="lead">August 16th, 2015</p>
                    </div>
            	</div>
			</div>

           <div class="oc-item">
				<div class="iportfolio">
                    <div class="portfolio-image">
                        <img src="images/jhm-event-4th.jpg" alt="John Hagee Minsitries">
                        <div class="portfolio-overlay">
                            <div class="portfolio-desc">
                                <h3><a class="tcolorgold" href="#">4th of July Celebration</a></h3>
                                <span class="bottommargin-sm">Free concert and spectacular fireworks display. Join us for incredible entertainment and a fireworks display that will make... </span>
                                <a href="#">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-desc">
                        <h3><a class="tcolorblue" href="#">4th of July Celebration</a></h3>
                        <p class="lead">August 16th, 2015</p>
                    </div>
            	</div>
			</div> 




  
        </div><!-- #portfolio end -->
      </div>
    <div class="container center clearfix topmargin-sm topmargin-sm">
        <a href="#" class="button button-3d button-large">View All Events</a>
    </div>
</div>
<script type="text/javascript">

						jQuery(document).ready(function($) {

							var ocPortfolio = $("#oc-portfolio");

							ocPortfolio.owlCarousel({
								margin: 20,
								nav: true,
								navText: ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
								autoplay: false,
								autoplayHoverPause: true,
								dots: true,
								responsive:{
									0:{ items:1 },
									600:{ items:2 },
									1000:{ items:3 },
									1200:{ items:4 }
								}
							});

						});

					</script>