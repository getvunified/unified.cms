<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Unsubscribe</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-unsubscribe.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">Connect</a></li>
                    <li><a href="#">Email Subscriptions</a></li>
                    <li class="active">Unsubscribe</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        

                    <div class="section noborder notopmargin nobottommargin nopadding" id="unsubscribe">
                        <div class="container">
                        <!-- Email Signup Form
					============================================= -->
                                        <div class="heading-block noborder topmargin bottommargin center">
						<h2>Unsubscribe</h2>
						<p>Use the form above to unsubscribe from JHM email subscriptions.</p>
					</div>
					<div class="nobottommargin">

						<div id="contact-form-result2" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>

						<form class="nobottommargin" id="template-contactform2" name="template-contactform" action="include/sendemail.php" method="post">

							<div class="form-process"></div>

                                <div class="col_one_third topmargin-xsm">
                                    
                                    <div class="fright">
                                        <ul class="list-unstyled">
                                            <li><input type="checkbox" id="template-contactform-name2" name="template-contactform-name2" value="" class="checkbox-inline" checked /> <label for="template-contactform-name2">A Moment with Matt </label></li>
                                    
                                            <li><input type="checkbox" id="template-contactform-name3" name="template-contactform-name3" value="" class="checkbox-inline" checked /> <label for="template-contactform-name3">Daily Blessings and Readings </label></li>
                                    
                                            <li><input type="checkbox" id="template-contactform-name4" name="template-contactform-name4" value="" class="checkbox-inline" checked /> <label for="template-contactform-name4">Event Notifications </label></li>
                                    
                                            <li><input type="checkbox" id="template-contactform-name5" name="template-contactform-name5" value="" class="checkbox-inline" checked /> <label for="template-contactform-name5">Weekly Update </label></li>
                                        </ul>
                                    </div>
                               	</div>
							


							<div class="col_two_third col_last">
                                                                    <label for="template-contactform-email2">Email <small>*</small></label>
                                                                    <input type="text" id="template-contactform-email2" name="template-contactform-email2" value="" class="email sm-form-control required" />
                                                                <div class="topmargin-xsm">
                                                                    <label for="template-contactform-message2">Why are you unsubscribing? (optional)</label>
                                                                    <textarea id="template-contactform-message2" name="template-contactform-message2" value="" class="sm-form-control"></textarea>
                                                                </div>
								<button class="button button-3d topmargin-sm" type="submit" id="template-contactform-submit2" name="template-contactform-submit2" value="submit">Unsubscribe</button>
							</div>

<script type="text/javascript">

							$("#template-contactform2").validate({
								submitHandler: function(form) {
									$('.form-process').fadeIn();
									$(form).ajaxSubmit({
										target: '#contact-form-result2',
										success: function() {
											$('.form-process').fadeOut();
											$(form).find('.sm-form-control').val('');
											$('#contact-form-result2').attr('data-notify-msg', $('#contact-form-result2').html()).html('');
											SEMICOLON.widget.notifications($('#contact-form-result2'));
										}
									});
								}
							});

						</script>
							<div class="clear"></div>
						
						</form>

						

					</div><!-- .email signup form end -->
                        </div>
                    </div>
		</section><!-- #content end -->
			

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>