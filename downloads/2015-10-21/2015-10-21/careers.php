<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Careers</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-careers.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">About</a></li>
                    <li class="active">Careers</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="content-wrap noborder topmargin nobottommargin nopadding">
	 
                <div data-animate="fadeIn" class="container bottommargin clearfix">

                    <div><h2 class="tcolorgold jobs">John Hagee Ministries Open Positions</h2></div>

                    <div class="fancy-title title-bottom-border">
                        <h3>Broadcast Engineer/IT Technician</h3>
                    </div>

                    <p>Setup and assist with live productions in studio and on location. Adequate knowledge of video routing system, switcher systems, and audio equipment is required. Assist in the tracking and updating of firmware and software as it relates to production equipment. Responsible for maintaining the infrastructure of an IT file management system. Ability to trouble shoot equipment issues down to board level is essential. Requires for-year degree or relevant work experience in the IT or Broadcasting industry.</p>

                    <div class="accordion accordion-bg clearfix">

                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Requirements</div>
                        <div class="acc_content clearfix">
                            <ul class="iconlist iconlist-color nobottommargin">
                                <li><i class="icon-ok"></i>B.Tech./ B.E / MCA degree in Computer Science, Engineering or a related stream.</li>
                                <li><i class="icon-ok"></i>3+ years of software development experience.</li>
                                <li><i class="icon-ok"></i>3+ years of Python / Java development projects experience.</li>
                                <li><i class="icon-ok"></i>Minimum of 4 live project roll outs.</li>
                                <li><i class="icon-ok"></i>Experience with third-party libraries and APIs.</li>
                                <li><i class="icon-ok"></i>In depth understanding and experience  of either SDLC or PDLC.</li>
                                <li><i class="icon-ok"></i>Good Communication Skills</li>
                                <li><i class="icon-ok"></i>Team Player</li>
                            </ul>
                        </div>

                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>What we expect from you</div>
                        <div class="acc_content clearfix">
                            <ul class="iconlist iconlist-color nobottommargin">
                                <li><i class="icon-ok"></i>B.Tech./ B.E / MCA degree in Computer Science, Engineering or a related stream.</li>
                                <li><i class="icon-ok"></i>3+ years of software development experience.</li>
                                <li><i class="icon-ok"></i>3+ years of Python / Java development projects experience.</li>
                                <li><i class="icon-ok"></i>Minimum of 4 live project roll outs.</li>
                                <li><i class="icon-ok"></i>Experience with third-party libraries and APIs.</li>
                                <li><i class="icon-ok"></i>In depth understanding and experience  of either SDLC or PDLC.</li>
                                <li><i class="icon-ok"></i>Good Communication Skills</li>
                                <li><i class="icon-ok"></i>Team Player</li>
                            </ul>
                        </div>

                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>What you can offer</div>
                        <div class="acc_content clearfix">Setup and assist with live productions in studio and on location. Adequate knowledge of video routing system, switcher systems, and audio equipment is required. Assist in the tracking and updating of firmware and software as it relates to production equipment. Responsible for maintaining the infrastructure of an IT file management system. Ability to trouble shoot equipment issues down to board level is essential. Requires for-year degree or relevant work experience in the IT or Broadcasting industry.</div>

                    </div>

                    <a href="#" class="button button-3d nomargin">Apply Now</a>

                    <div class="line-short"></div>

                    <div class="fancy-title title-bottom-border">
                        <h3>IT Technician</h3>
                    </div>

                    <p>Responsible for identifying, troubleshooting, researching and resolving technical support problems as submitted to the IT department. Maintain documentation, tracking and monitoring of tech support requests. Basic to intermediate experience and proficiency with Microsoft Operating Systems, Office Application Suites and other mainstream applications and technologies required. Fundamental understanding of PC-based computer hardware, networking technologies and protocol is expected. Experience with Apple Operating Systems and applications is a plus.</p>

                    <div class="accordion accordion-bg clearfix">

                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Requirements</div>
                        <div class="acc_content clearfix">
                            <ul class="iconlist iconlist-color nobottommargin">
                                <li><i class="icon-ok"></i>B.Tech./ B.E / MCA degree in Computer Science, Engineering or a related stream.</li>
                                <li><i class="icon-ok"></i>3+ years of software development experience.</li>
                                <li><i class="icon-ok"></i>3+ years of Python / Java development projects experience.</li>
                                <li><i class="icon-ok"></i>Minimum of 4 live project roll outs.</li>
                                <li><i class="icon-ok"></i>Experience with third-party libraries and APIs.</li>
                                <li><i class="icon-ok"></i>In depth understanding and experience  of either SDLC or PDLC.</li>
                                <li><i class="icon-ok"></i>Good Communication Skills</li>
                                <li><i class="icon-ok"></i>Team Player</li>
                            </ul>
                        </div>

                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>What we expect from you</div>
                        <div class="acc_content clearfix">
                            <ul class="iconlist iconlist-color nobottommargin">
                                <li><i class="icon-ok"></i>B.Tech./ B.E / MCA degree in Computer Science, Engineering or a related stream.</li>
                                <li><i class="icon-ok"></i>3+ years of software development experience.</li>
                                <li><i class="icon-ok"></i>3+ years of Python / Java development projects experience.</li>
                                <li><i class="icon-ok"></i>Minimum of 4 live project roll outs.</li>
                                <li><i class="icon-ok"></i>Experience with third-party libraries and APIs.</li>
                                <li><i class="icon-ok"></i>In depth understanding and experience  of either SDLC or PDLC.</li>
                                <li><i class="icon-ok"></i>Good Communication Skills</li>
                                <li><i class="icon-ok"></i>Team Player</li>
                            </ul>
                        </div>

                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>What you can offer</div>
                        <div class="acc_content clearfix">Responsible for identifying, troubleshooting, researching and resolving technical support problems as submitted to the IT department. Maintain documentation, tracking and monitoring of tech support requests. Basic to intermediate experience and proficiency with Microsoft Operating Systems, Office Application Suites and other mainstream applications and technologies required. Fundamental understanding of PC-based computer hardware, networking technologies and protocol is expected. Experience with Apple Operating Systems and applications is a plus.</div>

                    </div>

                    <a href="#" class="button button-3d nomargin">Apply Now</a>

                    <div class="line"></div>
                    
                    <div><h2 class="tcolorgold jobs">Difference Media Open Positions</h2></div>
                    
                    <div class="fancy-title title-bottom-border">
                        <h3>Domain Administrator</h3>
                    </div>

                    <p>Maintain computer network, network security and internal applications for Ministry domains. Support wireless network, Citrix and VMware environments. Requires five or more years of relevant work experience.</p>

                    <div class="accordion accordion-bg clearfix">

                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Requirements</div>
                        <div class="acc_content clearfix">
                            <ul class="iconlist iconlist-color nobottommargin">
                                <li><i class="icon-ok"></i>B.Tech./ B.E / MCA degree in Computer Science, Engineering or a related stream.</li>
                                <li><i class="icon-ok"></i>3+ years of software development experience.</li>
                                <li><i class="icon-ok"></i>3+ years of Python / Java development projects experience.</li>
                                <li><i class="icon-ok"></i>Minimum of 4 live project roll outs.</li>
                                <li><i class="icon-ok"></i>Experience with third-party libraries and APIs.</li>
                                <li><i class="icon-ok"></i>In depth understanding and experience  of either SDLC or PDLC.</li>
                                <li><i class="icon-ok"></i>Good Communication Skills</li>
                                <li><i class="icon-ok"></i>Team Player</li>
                            </ul>
                        </div>

                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>What we expect from you</div>
                        <div class="acc_content clearfix">
                            <ul class="iconlist iconlist-color nobottommargin">
                                <li><i class="icon-ok"></i>B.Tech./ B.E / MCA degree in Computer Science, Engineering or a related stream.</li>
                                <li><i class="icon-ok"></i>3+ years of software development experience.</li>
                                <li><i class="icon-ok"></i>3+ years of Python / Java development projects experience.</li>
                                <li><i class="icon-ok"></i>Minimum of 4 live project roll outs.</li>
                                <li><i class="icon-ok"></i>Experience with third-party libraries and APIs.</li>
                                <li><i class="icon-ok"></i>In depth understanding and experience  of either SDLC or PDLC.</li>
                                <li><i class="icon-ok"></i>Good Communication Skills</li>
                                <li><i class="icon-ok"></i>Team Player</li>
                            </ul>
                        </div>

                        <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>What you can offer</div>
                        <div class="acc_content clearfix">Maintain computer network, network security and internal applications for Ministry domains. Support wireless network, Citrix and VMware environments. Requires five or more years of relevant work experience.</div>

                    </div>

                    <a href="#" class="button button-3d nomargin">Apply Now</a>
				</div>
			</div>

            <div class="clear"></div>
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>