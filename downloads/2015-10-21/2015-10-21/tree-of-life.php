<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="js/jquery.gmap.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Tree of Life</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>


 		<!-- Page Header
		============================================= -->
        <?php include "include/pageheader-treeoflife.php";?>
       

		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="giving-opportunities.php">Giving Opportunities</a></li>					
                    <li class="active">Tree of Life</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="section noborder nomargin nopadding">
	 
                <div class="container clearfix">
       <p>The Tree of Life is a beautiful sculpture cast in bronze bearing leaves of polished brass permanently located in the lobby of John Hagee Ministries. The leaves that hang from the many branches of this magnificent tree are inscribed with the name of each person who has given a gift of $1000 or more to John Hagee Ministries for the purpose of preaching the Gospel. Each contribution is a memorial of the giver’s devotion to the Great Commission for years to come.</p>

<p>If you would like your name or the name of a loved one commemorated on this magnificent symbol of life please complete the form below.
“He who has an ear, let him hear what the Spirit says to the churches. To him who overcomes, I will give the right to eat from the tree of life, which is in the Paradise of God.” Revelation 2:7</p>

   </div>
</div>

</section>
              <!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>