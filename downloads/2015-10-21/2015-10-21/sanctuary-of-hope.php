<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="js/jquery.gmap.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Sanctuary of Hope</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>


 		<!-- Page Header
		============================================= -->
        <?php include "include/pageheader-sanctuaryofhope.php";?>
       

		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="giving-opportunities.php">Giving Opportunities</a></li>					
                    <li class="active">Sanctuary of Hope</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="section noborder nomargin nopadding">
	 
                <div class="container clearfix">
				<a href="giving-opportunities.php" class="button button-3d button-small bottommargin-sm">Giving Opportunities</a>
				<a href="donate-to-jhm.php" class="button button-3d button-small bottommargin-sm">Donate to JHM</a>

				 	<div class="bottommargin-sm">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/HWLoVqT0iVM" frameborder="0" allowfullscreen></iframe>
					</div>
					
					

					<p>Sanctuary of Hope is an oasis of hope for children that are seeking spiritual guidance and opportunity. It is a comprehensive, long-term concept to save the youth of our city and America.

<p>When Pastor John Hagee was a teenager, he worked many different jobs. One that has left an indelible impression upon him was the time he spent working in an orphanage. "I was privileged in the time that I was there to see life through the eyes of an orphan." He recalls the young boys who would chat amongst themselves saying, "This week, my mom’s going to come get me. My dad is coming finally…and he’s going to be driving a big, new car." The day would come, and no one came. Not even for a visit. Week after week this happened and then it dawned on them. "No one’s coming for me; I’m by myself. I’m completely alone."</p>

<p>This is where the idea for Sanctuary of Hope was born. Pastor says, "I know that it is an ocean of need. But it is wrong to see a need and not try to meet that need. We can help some of them. And because we can, we should. And because this is the Commission of God, we are."
First, saving children begins with saving their physical life. America is losing 1000 babies a day to abortion, hindering our future and hope as nation. Secondly, children need a loving home where they can be nurtured in a Christ-centered environment. Finally, children should be educated in a righteous environment, free from the secular indoctrination that is prevalent in the school systems today.</p>

With these three foundational statements, donated funds for this outreach will go towards the Sanctuary of Hope with three divisions:
</p>
<h3>3 Foundational Elements</h3>

<p>Division One: An unwed mother's facility where young women who are pregnant and do not want to terminate their pregnancy would be invited to stay until the baby is born. The newborn baby could be adopted by a godly family of proven character or raised in the Sanctuary of Hope orphanage.</p>

			

<p>Division Two: The building and development of a state-of-the-art orphanage in the San Antonio area</p>
		

<p>Division Three: The building of a dormitory for a world-class boarding school where students can attend one of the finest Christian schools in the country, Cornerstone Christian Schools.</p>

<p>This cannot be accomplished without you. Love is not what you say. Love is what you do.
Won’t you please join us at John Hagee Ministries in this endeavor to meet the need of these priceless treasures, our children? As Pastor Hagee says, "When I get to heaven, God isn’t going to pat me on the back for erecting buildings to help children. He is going to reward those who gave and made it possible."</p>


				</div>
		</div>

    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>