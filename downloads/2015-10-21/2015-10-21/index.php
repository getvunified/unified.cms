<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
<link rel="stylesheet" href="css/responsive-tabs.css">


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries</title>

 
</head>

<body class="stretched side-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">


                <div class="col_half nomargin fright">

                      <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                <!-- <div class="col_half fright col_last nobottommargin">

                    <!-- Top Social
                    ============================================= 
                    <div id="top-social">
                        <ul>
                            <li><a href="#" class="si-play"><span class="ts-icon"><i class="icon-play"></i></span><span class="ts-text">GETV</span></a></li>
                            <li><a href="#" class="si-sun"><span class="ts-icon"><i class="icon-sun"></i></span><span class="ts-text">Cornerstone Church</span></a></li>
                            <li><a href="#" class="si-facetime-video"><span class="ts-icon"><i class="icon-facetime-video"></i></span><span class="ts-text">Difference Media</span></a></li>
                            <li><a href="#" class="si-pencil2"><span class="ts-icon"><i class="icon-pencil2"></i></span><span class="ts-text">Cornerstone School</span></a></li>
                            <li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
                            <li><a href="#" class="si-twitter2"><span class="ts-icon"><i class="icon-twitter2"></i></span><span class="ts-text">Twitter</span></a></li>
                            <li><a href="#" class="si-youtube"><span class="ts-icon"><i class="icon-youtube"></i></span><span class="ts-text">Youtube</span></a></li>
                            <li><a href="#" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
                            <li><a href="#" class="si-pinterest"><span class="ts-icon"><i class="icon-pinterest2"></i></span><span class="ts-text">Pinterest</span></a></li>
                            <li><a href="#" class="si-gplus"><span class="ts-icon"><i class="icon-gplus"></i></span><span class="ts-text">Google Plus</span></a></li>
                            <li><a href="tel:1-800-854-9899" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text"> 1-800-854-9899</span></a></li>
                            <li><a href="mailto:support@jhm.org" class="si-email3"><span class="ts-icon"><i class="icon-email3"></i></span><span class="ts-text">support@jhm.org</span></a></li>
                        </ul>
                    </div><!-- #top-social end  

                </div> -->

            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Slider
		============================================= -->
        <?php include "include/slider.php";?>


		<!-- Content
		============================================= -->
		<section id="content">
        
            
			<!-- JHM Mission
            ============================================= -->
            <?php include "include/jhm-mission.php";?>

            <div class="clear"></div>

 			
			<!-- GETV
            ============================================= -->
            <?php include "include/getv.php";?>

            <div class="clear"></div>

            
			<div class="col_one_third col_respon nobottommargin hidden-xs">
				<div class="promo promo-dark promo-flat promo" style="background-color: #e2e2e2;">
					<h3><a class="tcolorblue-link" href="#">The Difference </br>with Matthew </br>and Kendal Hagee</a></h3>
				</div>
			</div>
			
			<div class="col_one_third col_respon nobottommargin">
				<div class="promo promo-dark promo-flat promo norightpadding" style="background-color: #fff;">
					<img src="images/cross-icon.png" alt="Daily Truth" style="width:100px; float:left;">
					<h3><a class="tcolorblue-link" href="#">Salvation Message </br>How Can I Be Saved?</a></h3>
				</div>
			</div>

			<div class="col_one_third col_respon col_last nobottommargin hidden-xs">
				<div class="promo promo-dark promo-flat promo" style="background-color: #e2e2e2;">
					<h3><a class="tcolorblue-link" href="#">The Coming </br>Four Blood Moons </br>3 Part Series</a></h3>
				</div>
			</div>
			
			<div class="clear"></div>









		<!--<div class="container-fluid clearfix nobottommargin topmargin-sm">
                <div class="row clearfix common-height">
                   
                    <div class="col_one_third bottommargin-sm" style="background: #e2e2e2;">
                         
                            <div class="nobottommargin nobottomborder center">
                                <h3>The Difference with Matthew and Kendal Hagee</h3>  
                            </div>
                        
                    </div>

                   <div class="col_one_third bottommargin-sm" style="border-right: 1px solid #e2e2e2;">
                         <div class="feature-box fbox-small fbox-plain center">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <div class="heading-block nobottommargin nobottomborder">
                                <h4>Salvation Message</h4>
                            </div>
                        </div>
                    </div>

                  <div class="col_one_third col_last bottommargin-sm" style="border-right: 1px solid #e2e2e2;">
                        
                            <div class="nobottommargin nobottomborder center">
                                <h3>The Coming Four Blood Moons 3 Part Series</h3>  
                            </div>
                        
                    </div>
                </div>
            </div>-->

            
                
            <!-- JHM Shop
            ============================================= -->
            <?php include "include/jhm-shop.php";?>

            <div class="clear"></div>
            

            <!-- Daily Truth
            ============================================= -->
            <?php include "include/dailytruth.php";?>

           
            <div class="clear"></div>
			
            <!-- Bio Slider
            ============================================= -->

 			<?php include "include/slider2.php";?>

			 <div class="clear"></div>
      
            
			<!-- JHM Events
            ============================================= -->
            <?php include "include/events-home.php";?>

			<div class="clear"></div>

             
			<!-- Prayer Requests
            ============================================= -->
            <?php include "include/prayer-requests-home.php";?>

			<div class="clear"></div>

            
			<!-- JHM Connect
            ============================================= -->
            <?php include "include/jhm-connect.php";?>

			<div class="clear"></div>
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>