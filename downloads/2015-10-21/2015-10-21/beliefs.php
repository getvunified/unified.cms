<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

	<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries - Beliefs</title>
</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	<div id="side-panel" class="dark">

		<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	</div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

<!-- Top Bar
        ============================================= 
        <div id="top-bar">

            <div class="container clearfix">

                <div class="col_half nobottommargin">

                    <!-- Top Links
                    ============================================= 
                    <div class="top-links">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            
                            <li><a href="contact.html">Contact</a></li>
                            <li><a href="login-register.html">Login</a>
                                <div class="top-link-section">
                                    <form id="top-login" role="form">
                                        <div class="input-group" id="top-login-username">
                                            <span class="input-group-addon"><i class="icon-user"></i></span>
                                            <input type="email" class="form-control" placeholder="Email address" required="">
                                        </div>
                                        <div class="input-group" id="top-login-password">
                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <label class="checkbox">
                                          <input type="checkbox" value="remember-me"> Remember me
                                        </label>
                                        <button class="btn btn-danger btn-block" type="submit">Sign in</button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div><!-- .top-links end 

                </div>

                <div class="col_half fright col_last nobottommargin">

                    <!-- Top Social
                    ============================================= 
                    <div id="top-social">
                        <ul>
                            <li><a href="#" class="si-play"><span class="ts-icon"><i class="icon-play"></i></span><span class="ts-text">GETV</span></a></li>
                            <li><a href="#" class="si-sun"><span class="ts-icon"><i class="icon-sun"></i></span><span class="ts-text">Cornerstone Church</span></a></li>
                            <li><a href="#" class="si-facetime-video"><span class="ts-icon"><i class="icon-facetime-video"></i></span><span class="ts-text">Difference Media</span></a></li>
                            <li><a href="#" class="si-pencil2"><span class="ts-icon"><i class="icon-pencil2"></i></span><span class="ts-text">Cornerstone School</span></a></li>
                            <li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
                            <li><a href="#" class="si-twitter2"><span class="ts-icon"><i class="icon-twitter2"></i></span><span class="ts-text">Twitter</span></a></li>
                            <li><a href="#" class="si-youtube"><span class="ts-icon"><i class="icon-youtube"></i></span><span class="ts-text">Youtube</span></a></li>
                            <li><a href="#" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
                            <li><a href="#" class="si-pinterest"><span class="ts-icon"><i class="icon-pinterest2"></i></span><span class="ts-text">Pinterest</span></a></li>
                            <li><a href="#" class="si-gplus"><span class="ts-icon"><i class="icon-gplus"></i></span><span class="ts-text">Google Plus</span></a></li>
                            <li><a href="tel:1-800-854-9899" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text"> 1-800-854-9899</span></a></li>
                            <li><a href="mailto:support@jhm.org" class="si-email3"><span class="ts-icon"><i class="icon-email3"></i></span><span class="ts-text">support@jhm.org</span></a></li>
                        </ul>
                    </div><!-- #top-social end 

                </div>

            </div>

        </div><!-- #top-bar end -->
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
		
         <!-- Page Title
        ============================================= -->
        <section id="page-title">

            <div class="container clearfix">
                <h1>Beliefs</h1>
                <span>John Hagee Minsitries' Beliefs</span>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li class="active">Beliefs</li>
                </ol>
            </div>

        </section><!-- #page-title end -->
        
        <!-- Content
		============================================= -->
		<section id="content">
        
			<div class="section noborder notopmargin nobottommargin nobottompadding">
				
                 <div class="container clearfix">
                    <div class="col_one_third">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>The Lord Jesus Christ</h3>
                            <p>We believe in the deity of Jesus Christ as the only begotten Son of God. We believe in His substitutionary death for all men, His resurrection, and His eventual return to judge the world.</p>
                        </div>
                    </div>
                
            	
                    <div class="col_one_third">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>Salvation</h3>
                            <p>We believe all men are born with a sinful nature and that the work of the Cross was to redeem man from the power of sin. We believe that this salvation is available to all who will receive it.</p>
                        </div>
                    </div>
                
                
                    <div class="col_one_third col_last">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>The Holy Spirit</h3>
                            <p>We believe in the existence of the Holy Spirit as the third person of the Trinity and in His interaction with man. We believe in the baptism of the Holy Spirit as manifested by the fruit and the gifts of the Spirit.</p>
                        </div>
                    </div>
                </div>
            
				<div class="container clearfix">
                    <div class="col_one_third">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>The Sacred Scripture</h3>
                            <p>We believe in the scripture as the inspired Word of God and that it is the complete revelation of God's will for mankind. We believe in the absolute authority of the scripture to govern the affairs of men.</p>
                        </div>
                    </div>
                
            	
                    <div class="col_one_third">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>Stewardship</h3>
                            <p>We believe that every man is the steward of his life and resources which ultimately belong to God. We believe that tithing is a measure of obedience to the scriptural principles of stewardship.</p>
                        </div>
                    </div>
                
                
                    <div class="col_one_third col_last">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>The Church</h3>
                            <p>We believe in the Church as the eternal and universal Body of Christ consisting of all those who have accepted the work of the atonement. We believe in the need for a local assembly of believers for the purpose of evangelism and edification.</p>
                        </div>
                    </div>
                </div>
            
				<div class="container clearfix">
                    <div class="col_one_third">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>Prayer and Praise</h3>
                            <p>We believe in the worship of the Lord through singing, clapping, and the lifting of hands. We believe in the authority of the believer to ask freely of the Lord for his needs.</p>
                        </div>
                    </div>
                
            	
                    <div class="col_one_third">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>Body Ministry</h3>
                            <p>We believe in the ministry of the Holy Spirit to the Church body through the anointing of oil by the elders of the church.</p>
                        </div>
                    </div>
                
                
                    <div class="col_one_third col_last">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>Evangelism</h3>
                            <p>We believe that evangelism is the obligation of every follower of Jesus Christ. The Lord commands us to go out and make disciples of all the earth. We believe that each person is first responsible to evangelism in their own family as the Holy Spirit leads them and gives them the ability.</p>
                        </div>
                    </div>
                </div>
            
				<div class="container clearfix">
                    <div class="col_one_third">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>Water Baptism</h3>
                            <p>We believe in the ordinance of water baptism by immersion in obedience to the Word of God. All those who have accepted Jesus Christ as their personal savior should be baptized in water as a public profession of their faith in Christ and to experience what the Bible calls the "Circumcision of the Spirit."</p>
                        </div>
                    </div>
                
            	
                    <div class="col_one_third">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>Our Commitment to Israel</h3>
                            <p>We believe in the promise of Genesis 12:3 regarding the Jewish people and the nation of Israel. We believe that this is an eternal covenant between God and the seed of Abraham to which God is faithful.</p>
                        </div>
                    </div>
                
                
                    <div class="col_one_third col_last">
                        <div class="feature-box fbox-plain fbox-light">
                            <div class="fbox-icon">
                                <i class="icon-plus"></i>
                            </div>
                            <h3>The Priesthood of the Believer</h3>
                            <p>We believe that every believer has a unique relationship to the Lord. As His children, every Christian has immediate access to the throne of Grace and the ability to manifest the power of the Lord Jesus Christ in ministry.</p>
                        </div>
                    </div>
                </div>
			<div class="line"></div>
			</div>
            
           
					
                <!--<div class="col_full nobottommargin common-height">

                    <div class="col-md-3 dark col-padding ohidden" style="background-color: #DAAC01;">
                        <div>
                            <h3 class="uppercase" style="font-weight: 600;">GETV 24/7 Live Stream</h3>
                            <p style="line-height: 1.8;">GETV is an internet based video channel with a mission to preach the Gospel of Jesus Christ to the ends of the Earth.</p>
                            <a href="#" class="button button-border button-light button-rounded button-reveal tright uppercase nomargin"><i class="icon-angle-right"></i><span>Watch Live Now</span></a>
                            <i class="icon-tv bgicon"></i>
                        </div>
                    </div>
                    
                    <div class="col-md-3 dark col-padding ohidden" style="background-color: #055176;">
                        <div>
                            <h3 class="uppercase" style="font-weight: 600;">Cornerstone Church</h3>
                            <p style="line-height: 1.8;">GETV is an internet based video channel with a mission to preach the Gospel of Jesus Christ to the ends of the Earth.</p>
                            <a href="#" class="button button-border button-light button-rounded button-reveal tright uppercase nomargin"><i class="icon-angle-right"></i><span>Visit Us</span></a>
                            <i class="icon-cross bgicon"></i>
                        </div>
                    </div>

                    <div class="col-md-3 dark col-padding ohidden" style="background-color: #DAAC01;">
                        <div>
                            <h3 class="uppercase" style="font-weight: 600;">Difference Media</h3>
                            <p style="line-height: 1.8;">GETV is an internet based video channel with a mission to preach the Gospel of Jesus Christ to the ends of the Earth.</p>
                            <a href="#" class="button button-border button-light button-rounded button-reveal tright uppercase nomargin"><i class="icon-angle-right"></i><span>Visit Us</span></a>
                            <i class="icon-play bgicon"></i>
                        </div>
                    </div>

                    <div class="col-md-3 dark col-padding ohidden" style="background-color: #055176;">
                        <div>
                            <h3 class="uppercase" style="font-weight: 600;">Cornerstone School</h3>
                            <p style="line-height: 1.8;">GETV is an internet based video channel with a mission to preach the Gospel of Jesus Christ to the ends of the Earth.</p>
                            <a href="#" class="button button-border button-light button-rounded button-reveal tright uppercase nomargin"><i class="icon-angle-right"></i><span>Visit Us</span></a>
                            <i class="icon-pencil bgicon"></i>
                        </div>
                    </div>
                    

                </div>-->
           
           <!--<div class="section parallax nomargin notopborder" style="background: url('images/getv-background.jpg') center center; padding: 100px 0;" data-stellar-background-ratio="0.3">
					<div class="container-fluid center clearfix dark">
						
                        <img src="images/getv-logo.png">
						
                        <div class="heading-block">
							<h1>24/7 GETV Stream</h1>
                            <span>In our 24/7 Stream you will see the best of Pastor Hagee, Pastor Matt Hagee, our exclusive Original content and guests of Cornerstone Church. Enjoy anytime anywhere here in GETV</span>
						</div>
                        <a href="#" class="button button-3d button-large">Watch Live Now</a>
					</div>
				
			</div>-->

			<div class="clear"></div>
                           		<!-- JHM Connect
					============================================= -->
                    <?php include "include/jhm-connect.php";?>


        
        </section><!-- #content end -->

		<!-- Footer
		============================================= -->
        
					<!-- Footer
					============================================= -->
                    <?php include "include/footer.php";?>
                    
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>

</body>
</html>