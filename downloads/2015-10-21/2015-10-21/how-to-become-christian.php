<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="js/jquery.gmap.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | How to Become a Christian</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>


 		<!-- Page Header
		============================================= -->
        <?php include "include/pageheader-how-to-become-christian.php";?>
       

		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">Encouragement</a></li>					
                    <li class="active">How to Become a Christian</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="section noborder notopmargin nobottommargin">
	 
                <div data-animate="fadeIn" class="container clearfix">

				 	<div class="bottommargin-sm">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/4pSj_7JU-VI" frameborder="0" allowfullscreen></iframe>
					</div>
					
					<div data-animate="fadeIn" class="container clearfix">

						<div class="heading-block noborder topmargin-sm nobottommargin center">
							<h2 class="tcolorgold">Questions for New Christians</h2>
							<p>We know as a new christian, you have a lot of questions. We’ve answered some of the most common questions below. </p>
						</div>

                          <!-- Post Content
                                    ============================================= -->
                          <div class="postcontent nobottommargin topmargin-sm noborder">
                           
                                <div id="faqs" class="faqs">
                                  <div class="toggle faq faq-shopandorders faq-aboutjhm faq-miscellaneous">
                                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>What does it mean to “be saved”? </div>
                                    <div class="togglec"> We believe all men are born with a sinful nature and that the work of the Cross was to redeem man from the power of sin. We believe that this salvation is available to all who will receive it.</div>
                                  </div>

                                  <div class="toggle faq faq-aboutjhm faq-miscellaneous">
                                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>If God created Satan, and Satan is evil, is evil God's fault?</div>
                                    <div class="togglec">No. God did not create Satan (or men, for that matter) as evil—only with the capacity for evil. (Isaiah 14:12).</div>
                                  </div>
                                  
                                  <div class="toggle faq faq-aboutjhm faq-miscellaneous">
                                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>Could Jesus have sinned?</div>
                                    <div class="togglec">Only if He had wanted to sin.</div>
                                  </div>
            
                                  <div class="toggle faq faq-aboutjhm faq-miscellaneous">
                                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>Could Jesus have really ever wanted to sin?</div>
                                    <div class="togglec">No. Not really. Not ultimately. In His humanity alone, the answer would be “yes,” which is why the temptations that Jesus faced (Matthew 4:1, Hebrews 4:15) were indeed very intense and real. Nevertheless, though Jesus was fully human, He was not only human. And in terms of His Divine nature—when it came right down to it—He would never, and could never, have actually ever willed to sin. To have done so would mean that He (as God) had actually willed to occur what He actually willed not to occur, which makes no sense at all. The Bible says in James 1:13 that God (i.e. by inference, the Divine nature of Christ) cannot be tempted by evil.</div>
                                  </div>
                                  
                                  <div class="toggle faq faq-aboutjhm faq-miscellaneous">
                                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>If God is everywhere, is He in Hell?</div>
                                    <div class="togglec">Yes. Insofar as Divine omnipresence implies access and involves, or interrelates to, the other Divine attributes of infinitude, omnipotence (control), and omniscience (awareness), God is present even in hell. No door anywhere is closed to God. No room is locked. No corner of the universe or reality is off-limits to Deity. The Bible affirms this truth in texts like Psalm 139:7-10, Colossians 1:17, Amos 9:1-4, Isaiah 66:1-2, Jeremiah 23:23-24.

God, however, is not relationally present with, or available to, those in hell. That is, He is not present to bless, deal with, or grant even common grace and light in hell, which explains the terrifying sense of Matthew 25:41, the external extrapolation of a spiritual separation that occurs even now when sin is present (Isaiah 59:2, Matthew 27:46).</div>
                                  </div>
                                  
                                  <div class="toggle faq faq-aboutjhm faq-submitmaterial faq-miscellaneous">
                                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>What is the basic salvation message?</div>
                                    <div class="togglec">The basic salvation message is that Jesus died on the cross for our sins and rose from the dead (1 Cor. 15:1-4). All who would believe in Him will have everlasting life and escape the judgment of God upon those who have sinned against Him (John 3:16-18). Receiving salvation from God is done by faith (Rom. 5:1, Eph. 2:8). Also, by receiving Christ, people are enabled to repent of their sins.</div>
                                  </div>
                                  
                                  <div class="toggle faq faq-aboutjhm">
                                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>Must you be baptized in order to be saved?</div>
                                    <div class="togglec">No, you don't have to be baptized to be saved (Rom. 5:1, Acts 10:44-48).</div>
                                  </div>
            
                                  <div class="toggle faq faq-aboutjhm">
                                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>How do I contact Christians United For Israel?</div>
                                    <div class="togglec">You can reach Christians United for Israel (CUFI) through their website at <a href="http://www.cufi.org" target="_blank">www.cufi.org</a> or by calling 1-210-477-4714.</div>
                                  </div>
            
                                  <div class="toggle faq faq-aboutjhm">
                                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>Can any sin be forgiven?</div>
                                    <div class="togglec">There are at least three passages in the Bible that teach us that all but one sin can be forgiven. Matthew 12:31,32 and Mark 3:28,29 are two of these passages. For illustration, here is the third passage, spoken by Jesus in the book of Luke: Luke 12:8 “Also I say to you, whoever confesses Me before men, him the Son of Man also will confess before the angels of God.9 But he who denies Me before men will be denied before the angels of God. 10 “And anyone who speaks a word against the Son of Man, it will be forgiven him; but to him who blasphemes against the Holy Spirit, it will not be forgiven.”</div>
                                  </div>
            
                                  <div class="toggle faq faq-aboutjhm">
                                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>Is the Holy Spirit God?</div>
                                    <div class="togglec">The Holy Spirit is spoken of throughout the Bible as God’s Spirit. By the time we get to the New Testament, it is clear that the Holy Spirit is seen as God. In Acts 5:3-4, Peter says “Ananias, why has Satan filled your heart to lie to the Holy Spirit and to keep back for your self part of the proceeds of the land? ... You have not lied to men but to God.” The Holy Spirit and God are seen as one.</div>
                                  </div>
                                    
                                  <div class="toggle faq faq-aboutjhm">
                                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>What is the Trinity?</div>
                                    <div class="togglec">God is truly a Trinity - three persons in one. Mark 12:29 does not disprove this.  It says that God is one. But we learn from other parts of the Bible that within this one there are three persons. For example, Jesus said,"I and the Father are one" (John 10:30). This shows that Jesus is fully God, and is bound with the Father in such a close and perfect relationship of love with the Holy Spirit that they are actually one. As a result, when we worship Jesus we are worshipping God the Father. It is impossible to worship Jesus without worshipping the Father, and impossible to worship the Father properly unless you worship Jesus.</div>
                                  </div>

									<div class="toggle faq faq-aboutjhm">
                                    	<div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>Is Jesus going to return or not?</div>
                                        <div class="togglec">Jesus is going to return. Acts 1:10-11 says, "And as they were gazing intently into the sky while He was departing, behold, two men in white clothing stood beside them; and they also said, "Men of Galilee, why do you stand looking into the sky? This Jesus, who has been taken up from you into heaven, will come in just the same way as you have watched Him go into heaven."</div>
                                	</div>
                                
									<div class="toggle faq faq-aboutjhm">
                                    	<div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>How can anyone be sure that they really have eternal life?</div>
                                        <div class="togglec">We can be sure of eternal life because Jesus finished the job of defeating sin and death when he died on the Cross and rose again. That is why he said “it is finished” as he died (John 19:30). That is also why it says that Jesus sat down at God’s right hand after making purification for sins (Hebrews 1:3 and 10:12-14). Jesus is the propitiation (atoning sacrifice) for the sins of the whole world (1 John 2:2). Jesus has done everything!</div>
                                	</div>

									<div class="toggle faq faq-aboutjhm">
                                    	<div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>What will happen in the End Times?</div>
                                        <div class="togglec">The Bible tells us that the Lord Jesus Christ will return ‘The Lord himself will descend from heaven"with the sound of the trumpet of God’ (1 Thessalonians 4:16). No one knows when he will return - we are told that his return will be unanticipated, it will come like"a thief in the night’ (1 Thessalonians 5:24). Christians who have died before Christ returns, will at that time rise from their graves (1 Thess 4:16b). Christians who are alive when Christ returns ‘will be caught up together with them in the clouds to meet the Lord’ (1 Thess 4:17) - and from then on, Christians will always physically be with the risen Christ. Christians will be transformed into Christ’s likeness and raised with an imperishable body (Philippians 3:20-21: 1 Corinithians 15:50-55).</div>
                                	</div>

									<div class="toggle faq faq-aboutjhm">
                                    	<div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>What is John 3:16?</div>
                                        <div class="togglec">The context of the verse is a conversation Jesus has with a Pharisee called Nicodemus. Nicodemus comes to Jesus to find out more about what Jesus is saying and in response Jesus talks about desperately helpless humanity is because of their sin - we have to be “reborn”, renewed, or born of the Spirit, if we are to have any hope of entering heaven. Nicodemus does not understand so Jesus puts it plainly - humanity cannot save themselves so God sent his Son so that through his death (being “lifted up”), whoever believes in him will be forgiven, reborn of the Spirit and enter heaven. It is a fundamental passage that shows us what we need to do if we want to be forgiven by God and spend enternity with him.</div>
                                	</div>

									<div class="toggle faq faq-aboutjhm">
                                    	<div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>Where will we go when we die?</div>
                                        <div class="togglec">God has provided some clear sign-posts on the topic, however he has not revealed every detail. We are invited to search the Bible for answers, but not be dissatisfied when the complete picture is hidden from us. There are two key"lines in the sand". First, the Christian dead are asleep to the world, but are alive to God. Because"for to [God] all are alive." (Luke 20:38b). This is confirmed by the words of Jesus on the cross, when he said to the repentant criminal next to him"today you will be with me in paradise" (Luke 23:43). The Apostle Paul expresses this view as a desire to "depart and be with Christ" (Philippians 1:23). The presentation of the Bible is that Christians are with God after their death. Second, the Christian dead are in a state of conscious, enriched fellowship with Christ. This is an expansion of the previous point, and it highlights that there is not a period of"soul sleep" or purgatory. Paul expresses this view as he would"prefer to be away from the body and at home with the Lord" (2 Cor 5:8).</div>
                                	</div>

									<div class="toggle faq faq-aboutjhm">
                                    	<div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>Who is Satan and Beelzebub?</div>
                                        <div class="togglec">Very little is said about Satan in the Old Testament. Where he is mentioned, he is always subject to God (Job 1-2) and always opposed to man’s best interest. In the New Testament, Satan and Beelzebub are used synonomously and seen as the evil ruler of all that is against God. Christians are warned to always be on their guard against him, using the armour of God, and particularly the Word of God, to oppose him. He is seen as a constant cunning enemy of Christ against whom Christian are always in battle. However the New Testament is also clear about his power and fate. Satan is, since the cross, a defeated enemy, destines for the destruction that comes on the last days. He is still powerful, able to tempt people and lead them astray but Christians are no longer under his control and are able to stand up to him. Where Satan comes from is unclear from the bible. Several theories suggest a fallen angel but this is only speculation. As a Christian we are to fight Satan but not be scared of him.</div>
                                	</div>

									<div class="toggle faq faq-aboutjhm">
                                    	<div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>How do I ask for forgiveness?</div>
                                        <div class="togglec">The great thing about praying to God is that He is more concerned with what is going on in your heart than that you say the exact right thing. When you pray for forgiveness, it is most important that you want to repent or do something about your sins you have committed. And when you pray for forgiveness you need to remember that God has taken care of all our sins in Jesus, and that because of him you can be forgiven. That said, I find it helpful to name my sins, because it helps me to really reflect on them, and pray for God’s help not to do them again. But I also pray for forgiveness for the sins I don’t know I have committed, because sometimes sin makes us blind to the awful things we do. So really, it’s a matter of preference. I think it is a helpful thing to list the sins and think about them, but there are by no means any rules.</div>
                                	</div>

									<div class="toggle faq faq-aboutjhm">
                                    	<div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>How can a loving God send people to hell?</div>
                                        <div class="togglec">Your premise is correct–God loves us. But His love is strong, not weak and permissive. No one admires the parent who never applies any consequences for their child’s selfish or bad behavior. God isn’t that way. His good and righteous character is opposed to sin–He condemns and punishes sin. Because He is loving, He sent His own Son to take that punishment on Himself. God has done everything He can do, without taking away our freedom, to save us from hell. So it comes down to human beings refusing to accept God’s offer of eternal life more than God sending people to hell. How come men reject God’s offer of eternal life?</div>
                                	</div>

									<div class="toggle faq faq-aboutjhm">
                                    	<div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>What about those who have never heard?</div>
                                        <div class="togglec">No one claims to know God exhaustively. If they did they would be God. What we do know about God (what is clearly revealed in the Bible) is that God reveals himself to every man and that the only way that any of us can get to heaven is through Jesus Christ (Acts 4:12). What is not as clearly revealed is how the work of Christ on the cross is applied to every person. Because He is just we can be sure that He will treat fairly those who have not heard about Christ. One final thing that the Bible is clear on is that for those who have heard about Christ and reject him they are choosing to reject God and pay the penalty for their own sins.</div>
                                	</div>


								</div>
                        	</div><!-- .postcontent end -->
                	</div>
                    
                    <div class="line topmargin bottommargin-sm"></div>
                        

						<div class="heading-block noborder topmargin-sm bottommargin center">
							<h2 class="tcolorgold">We're Here For You</h2>
						</div>

                        <div class="col_half">
                            <div class="feature-box fbox-large">
                               <div class="fbox-icon">
                                    <img src="images/icon-representative.jpg" alt="Representative">
                                </div>
                                <h3>Representative</h3>
                                <div class="text-left topmargin-xsm">
                                    <div><i class="icon-phone"></i> <strong>Phone:</strong> 1-800-854-9899</div>
                                    <div><i class="icon-mail"></i> <strong>Email:</strong> <a href="mailto:support@jhm.org">support@jhm.org</a></div>
                                </div>
                            </div>
                        </div>
                    
                        <div class="col_half col_last">
                            <div class="feature-box fbox-large">
                               <div class="fbox-icon">
                                    <img src="images/icon-prayer.jpg" alt="Prayer">
                                </div>
                                <h3>Representative</h3>
                                <div class="text-left topmargin-xsm">
                                    <div><i class="icon-phone"></i> <strong>Phone:</strong> 1-800-854-9899</div>
                                    <div><a href="prayer-requests.php" class="button button-3d topmargin-xsm">Online Form</a></div>
                                </div>
                            </div>
                        </div>
                    
				</div>

			</div>

    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>