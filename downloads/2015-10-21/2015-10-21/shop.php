<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
                <link rel="stylesheet" href="css/shop.css" type="text/css" />
                <script type="text/javascript" src="js/shop.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

                    <!-- Top Links 
                    ============================================= -->
                    <div class="top-links">
                        <ul>
                            <li><a href="#"> <i class="icon-line2-user"></i>Login</a>
                                <div class="top-link-section">
                                    <form id="top-login" role="form">
                                        <div class="input-group" id="top-login-username">
                                            <span class="input-group-addon"><i class="icon-mail"></i></span>
                                            <input type="email" class="form-control" placeholder="Email address" required="">
                                        </div>
                                        <div class="input-group" id="top-login-password">
                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <label class="checkbox">
                                          <input type="checkbox" value="remember-me"> Remember me
                                        </label>
                                        <button class="btn btn-danger btn-block" type="submit">Sign in</button>
                                    </form>
                                </div>
                            </li>
							<li><a href="#"><i class="icon-key"></i> Register</a>
								<div class="top-link-section">
                                    <form id="top-register" role="form">
                                        <div class="input-group" id="top-register-name">
                                            <span class="input-group-addon"><i class="icon-line2-user"></i></span>
                                            <input type="name" class="form-control" placeholder="Name" required="">
                                        </div>
										<div class="input-group" id="top-register-username">
                                            <span class="input-group-addon"><i class="icon-mail"></i></span>
                                            <input type="email" class="form-control" placeholder="Email address" required="">
                                        </div>
                                        <div class="input-group" id="top-register-password">
                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <button class="btn btn-danger btn-block" type="submit">Register</button>
                                    </form>
                                </div>
							</li>
                        </ul>
                    </div> <!--.top-links end -->

                </div>

            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Slider
		============================================= -->
        <?php include "include/slider.php";?>


        <!-- Content
        ============================================= -->
        <section id="content">
            <div id="shop-nav">
                <ul>
                    <li class="active"><a class="tab" rel="#featured" href="#">Featured</a></li>
                    <li><a class="tab" rel="#categories" href="#">Categories</a></li>
                    <li><a class="tab" rel="#authors" href="#">Authors</a></li>
                    <li><a class="tab" rel="#subjects" href="#">Subject</a></li>
                </ul>
            </div>
            <div class="noborder nomargin nopadding topmargin-sm bottommargin-sm">
                <div class="container clearfix">
                    <div id="featured" class="tab-content tab-content-active noborder topmargin">
                        <h1 class="center">Featured Items<span class="sub-header">Items featured across television and GETV</span></h1>
                        <div class="shop-item col4">
                            <img src="images/shop/product-sample.jpg" />
                            <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                            <a href="#" class="view-item">View Item</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/product-sample.jpg" />
                            <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                            <a href="#" class="view-item">View Item</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/product-sample.jpg" />
                            <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                            <a href="#" class="view-item">View Item</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/product-sample.jpg" />
                            <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                            <a href="#" class="view-item">View Item</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/product-sample.jpg" />
                            <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                            <a href="#" class="view-item">View Item</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/product-sample.jpg" />
                            <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                            <a href="#" class="view-item">View Item</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/product-sample.jpg" />
                            <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                            <a href="#" class="view-item">View Item</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/product-sample.jpg" />
                            <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                            <a href="#" class="view-item">View Item</a>
                        </div>
                    </div>
                    <div id="categories" class="tab-content noborder topmargin">
                        <h1 class="center">Categories</h1>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items All Items All Items All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                    </div>
                    <div id="authors" class="tab-content noborder topmargin">
                        <h1 class="center">Authors</h1>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                    </div>
                    <div id="subjects" class="tab-content noborder topmargin">
                        <h1 class="center">Subjects</h1>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                        <div class="shop-item col4">
                            <img src="images/shop/category-sample.jpg" />
                            <h3>All Items</h3>
                            <a href="#" class="view-item view-item-yellow">See All</a>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>