<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Giving Opportunities</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-givingopportunities.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li class="active">Giving Opportunities</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
           <div class="section noborder notopmargin nobottommargin" style="background-color:#e2e2e2;">

				<div data-animate="fadeIn" class="container bottommargin clearfix">

					<div class="heading-block noborder notopmargin bottommargin center">
						<h2>Giving Opportunities</h2>
						<p>Connect with JHM through our Giving Opportunities</p>
					</div>

                    <!-- Posts
                    ============================================= -->
                    <div id="posts" class="post-grid grid-2 clearfix">

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                               <a href="sanctuary-of-hope.php"><img src="images/give-sanctuary-of-hope.jpg" alt="Sanctuary of Hope"></a>
                            </div>
                            <div class="entry-title">
                                <h2>Sanctuary of Hope</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold">One Time</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>Sanctuary of Hope is an oasis of hope for children that are seeking spiritual guidance and opportunity. It is a comprehensive, long-term concept to save the youth of our city and America. When Pastor John Hagee was a teenager, he worked many different jobs...</p>
								<a href="sanctuary-of-hope.php" class="button button-3d">Learn More</a>
                            </div>
                        </div>

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                               <img src="images/give-legacy-giving.jpg" alt="Legacy Giving and Planned Giving">
                            </div>
                            <div class="entry-title">
                                <h2>Legacy Giving and Planned Giving</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold">One Time</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>Legacy Giving is dedicated to helping you discover trusted ways to plan for your future and leave a legacy to the people you love and organizations in which you believe. Legacy Giving is dedicated to helping you discover trusted ways to plan for your future.</p>
								<a href="legacy-giving.php" class="button button-3d">Learn More</a>
                            </div>
                        </div>

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm fleft">
                            <div class="entry-image">
                               <img src="images/give-first-fruits.jpg" alt="First Fruits">
                            </div>
                            <div class="entry-title">
                                <h2>First Fruits</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold">One Time</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>The Kingdom of God has the greatest investment opportunities known to man. You invest and are guaranteed a 30-60-100 fold return. You are guaranteed the return in this life and in the life to come.</p>
								<a href="first-fruits.php" class="button button-3d">Learn More</a>
                            </div>
                        </div>

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                               <img src="images/give-founders-wall.jpg" alt="Founders Wall">
                            </div>
                            <div class="entry-title">
                                <h2>Founders Wall</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold">One Time</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>With a one-time minimum gift of $1,000 toward the John Hagee Ministries International Media Center, your name or the name of a loved one can be commemorated with a beautiful plaque adorning the Founder’s Wall in our prayer room. "Be diligent to present yourself approved to God...</p>
								<a href="founders-wall.php" class="button button-3d">Learn More</a>
                            </div>
                        </div>

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                               <img src="images/give-tree-of-life.jpg" alt="Tree of Life">
                            </div>
                            <div class="entry-title">
                                <h2>Tree of Life</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold">One Time</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>The Tree of Life is a beautiful sculpture cast in bronze bearing leaves of polished brass permanently located in the lobby of John Hagee Ministries. The Tree of Life is a beautiful sculpture cast in bronze bearing leaves of polished brass...</p>
								<a href="tree-of-life.php" class="button button-3d">Learn More</a>
                            </div>
                        </div>

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm fleft">
                            <div class="entry-image">
                               <img src="images/give-walk-of-honor.jpg" alt="Walk of Honor">
                            </div>
                            <div class="entry-title">
                                <h2>Walk of Honor</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold">One Time</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>How beautiful on the mountains are the feet of those who bring good news, who proclaim peace, who bring good tidings, who proclaim salvation, who say to Zion. How beautiful on the mountains are the feet of those who bring good news, who proclaim peace, who bring good tidings, who proclaim salvation, who say to Zion.</p>
								<a href="walk-of-honor.php" class="button button-3d">Learn More</a>
                            </div>
                        </div>

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                               <img src="images/give-donate-to-jhm.jpg" alt="Donate to JHM">
                            </div>
                            <div class="entry-title">
                                <h2>Donate to JHM</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold">One Time or Monthly</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>John Hagee Ministries is now being seen or heard in over 245 nations around the world and being broadcasted into 255 million homes worldwide. Many of the countries currently receiving our program are ravaged by civil war, famine, natural disasters...</p>
								<a href="donate-to-jhm.php" class="button button-3d">Learn More</a>
                            </div>
                        </div>

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                               <a href="support-israel.php"><img src="images/give-support-israel.jpg" alt="Support Israel"></a>
                            </div>
                            <div class="entry-title">
                                <h2>Support Israel</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold">One Time</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>This campaign has the goal of planting trees in Israel to help revitalize the land! Planting a tree is more than mere aesthetics. It is an opportunity to help: Transform barren regions of Israel into fruitful, productive land, create jobs, provide needed irrigation and sustain the local economy...</p>
								<a href="support-israel.php" class="button button-3d">Learn More</a>
                            </div>
                        </div>

					</div>

					<div class="heading-block noborder topmargin bottommargin center">
                        <h2>TV Opportunities</h2>
                        <p>Connect with JHM through our Giving Opportunities</p>
                    </div>

					<div id="posts" class="post-grid grid-2 clearfix">
						
						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                               <img src="images/give-founders-wall.jpg" alt="Founders Wall">
                            </div>
                            <div class="entry-title">
                                <h2>Founders Wall</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold">One Time</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>With a one-time minimum gift of $1,000 toward the John Hagee Ministries International Media Center, your name or the name of a loved one can be commemorated with a beautiful plaque adorning the Founder’s Wall in our prayer room. "Be diligent to present yourself approved to God...</p>
								<a href="#" class="button button-3d">Learn More</a>
                            </div>
                        </div>

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                               <img src="images/give-tree-of-life.jpg" alt="Tree of Life">
                            </div>
                            <div class="entry-title">
                                <h2>Tree of Life</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold">One Time</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>The Tree of Life is a beautiful sculpture cast in bronze bearing leaves of polished brass permanently located in the lobby of John Hagee Ministries...</p>
								<a href="#" class="button button-3d">Learn More</a>
                            </div>
                        </div>
					</div>

				



				</div>
			</div>
		</section><!-- #content end -->
			

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>