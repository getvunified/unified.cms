<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>

		<link rel="stylesheet" href="css/responsive-tabs.css">
    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Ways To watch</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-waystowatch.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">Connect</a></li>
                    <li class="active">Ways To Watch</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
			<div class="content-wrap noborder nomargin nobottompadding toppadding-xsm">

				<div data-animate="fadeIn" class="container clearfix">

					<div class="heading-block noborder center">
                        <h2>How to Watch</h2>
                        <p>Select your viewing device</p>
                    </div>

					<div id="processTabs">
						<ul class="process-steps process-3 bottommargin clearfix">
							<li>
                            	<img src="images/ways-icon-tv.png" style="width:50%; height:50%;" alt="Cable TV">
                                <h3 class="heading">Cable</h3>
                                <a href="#ptab1" class="button button-border">Where to Watch</a>
							</li>
							<li>
                                <img src="images/ways-icon-stream.png" style="width:50%; height:50%;" alt="Online Stream">
                                <h3 class="heading">Online</h3>
                                <a href="#ptab2" class="button button-border">Where to Watch</a>
							</li>
							<li>
                                <img src="images/ways-icon-roku.png" style="width:50%; height:50%;" alt="Roku Device">
                                <h3 class="heading">Roku</h3>
                                <a href="#ptab3" class="button button-border">Where to Watch</a>
							</li>
						</ul>
						<div>

							<div class="line topmargin-sm bottommargin-sm"></div>

							
							
							<div id="ptab1">
								<div class="tabs tabs-bordered clearfix" id="tab-1">
                                    <div class="heading-block noborder center">
                                        <h2>Cable TV</h2>
                                        <p>Some text or description here.</p>
                                    </div>
                                    <ul class="tab-nav clearfix">
                                        <li class="icons"><a href="#tabs-1"><img class="rightmargin-xsm" src="images/ways-us-flag-small.png">United States</a></li>
                                        <li class="icons"><a href="#tabs-2"><img class="rightmargin-xsm" src="images/ways-canada-flag-small.png">Canada</a></li>
                                        <li class="icons"><a href="#tabs-3"><img class="rightmargin-xsm" src="images/ways-uk-flag-small.png">United Kingdom</a></li>
                                    </ul>
                                    <div class="tab-container">
                                        <div class="tab-content clearfix" id="tabs-1">
                                           <h4 class="heading">CABLE TV - United States</h4>
                                        </div>
                                        <div class="tab-content clearfix" id="tabs-2">
                                           <h4 class="heading">CABLE TV - Canada</h4>
                                        </div>
                                        <div class="tab-content clearfix" id="tabs-3">
                                           <h4 class="heading">CABLE TV - United Kingdom</h4>
                                        </div>
                                    </div>
                                </div>							
							</div>
							
							<div id="ptab2">
								<div class="tabs tabs-bordered clearfix" id="tab-2">

                                    <div class="heading-block noborder center">
                                        <h2>Online Stream</h2>
                                        <p>Some text or description here.</p>
                                        <p>Online Stream Content</p>
                                    </div>

                                </div>							
							</div>
							<div id="ptab3">
								<div class="tabs tabs-bordered clearfix" id="tab-3">

									<div class="heading-block noborder center">
                                        <h2>Roku Device</h2>
                                        <p>Some text or description here.</p>
                                        <p>Roku content</p>
                                    </div>

                                    
                                </div>							
							</div>

							<script>
                              $(function() {
                                $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
                                $( ".tab-linker" ).click(function() {
                                    $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
                                    return false;
                                });
                              });
                            </script>
							
							





					<!-- For mobile size
							<div id="ptab1" class="hidden-lg">
								<div class="toggle toggle-bg topmargin-sm">
                                    <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Where to Watch - Cable TV</div>
                                    <div class="togglec">
										<p><strong>United States</strong></p>
										Info here
										<p><strong>UCanada</strong></p>
										Info here
										<p><strong>United Kingdom</strong></p>
										Info here
                                     </div>
								</div>						
							</div>
							
							<div id="ptab2" class="hidden-lg">
								<div class="toggle toggle-bg topmargin-sm">
                                    <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Online Stream</div>
                                    <div class="togglec">
										<p><strong>United States</strong></p>
										Info here
										<p><strong>UCanada</strong></p>
										Info here
										<p><strong>United Kingdom</strong></p>
										Info here
                                     </div>
								</div>							
							</div>

							<div id="ptab3" class="hidden-lg">
								<div class="toggle toggle-bg topmargin-sm">
                                    <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Where to Watch - Roku Device</div>
                                    <div class="togglec">
										<p><strong>United States</strong></p>
										Info here
										<p><strong>UCanada</strong></p>
										Info here
										<p><strong>United Kingdom</strong></p>
										Info here
                                     </div>
								</div>							
							</div>-->

							

						</div>
					</div>

					

					<div class="clear"></div>				

				</div>

            <div class="clear"></div>
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>








						