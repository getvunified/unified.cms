<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Frequently Asked Questions</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-faqs.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">About</a></li>
                    <li class="active">FAQs</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="content-wrap noborder topmargin nobottommargin nopadding">
	 
                <div class="container clearfix">
                  <!-- Post Content
                            ============================================= -->
                  <div class="postcontent nobottommargin topmargin-sm">
                    <ul id="portfolio-filter" class="clearfix">
                      <li class="activeFilter"><a href="#" data-filter="all">All</a></li>
                      <li><a href="#" data-filter=".faq-shopandorders">Shop/Orders</a></li>
                      <li><a href="#" data-filter=".faq-aboutjhm">About JHM</a></li>
                      <li><a href="#" data-filter=".faq-rootsinisrael">Roots In Israel</a></li>
                      <li><a href="#" data-filter=".faq-saltcovenant">Salt Covenant</a></li>
                      <li><a href="#" data-filter=".faq-miscellaneous">Miscellaneous</a></li>
                      <li><a href="#" data-filter=".faq-submitmaterial">Submitting Material</a></li>
                    </ul>
                    <div class="clear"></div>
                    <div id="faqs" class="faqs">
                      <div class="toggle faq faq-shopandorders faq-aboutjhm faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>How do I register for an account?</div>
                        <div class="togglec">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, dolorum, vero ipsum molestiae minima odio quo voluptate illum excepturi quam cum voluptates doloribus quae nisi tempore necessitatibus dolores ducimus enim libero eaque explicabo suscipit animi at quaerat aliquid ex expedita perspiciatis? Saepe, aperiam, nam unde quas beatae vero vitae nulla.</div>
                      </div>
                      <div class="toggle faq faq-aboutjhm faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-line2-book-open"></i><i class="toggle-open icon-line2-book-open"></i>(1) Instructions for Healing Scripture Player - first-time use</div>
                        <div class="togglec">
                            <ul>
                                <li>1. Remove white tab on back of player (this powers player on) you will hear a BEEP</li>
                                <li>2. Press 'play' button</li>
                                <li>3. Use volume button as necessary</li>
                                <li>4. To turn player off, press and hold 'ON/OFF' button until you hear "beep".</li>
                            </ul>
                        </div>
                      </div>
                      <div class="toggle faq faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-line2-book-open"></i><i class="toggle-open icon-line2-book-open"></i>(2) Instructions for Healing Scripture Player</div>
                        <div class="togglec"><ul>
                                <li>1. Press firmly and hold the 'On/Off' button until a BEEP sound occurs (this activates player)</li> 
                                <li>2. Press 'play' button</li>  
                                <li>3. Use volume button as necessary</li>  
                                <li>4. Press 'next message' to skip to desired message (either Lizzy's miracles or healing scriptures)</li>  
                                <li>5. Press 'repeat' button for continual play (red light will come on to indicate this feature is working)</li> 
                                <li>6. To turn player off, press and hold 'ON/OFF' button until you hear "BEEP".</li>
                            </ul>
                        </div>
                      </div>
                      <div class="toggle faq faq-aboutjhm faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-line2-book-open"></i><i class="toggle-open icon-line2-book-open"></i>Are written transcripts available of Pastor's sermons?</div>
                        <div class="togglec">Pastor’s sermons are only available in CD or DVD format. You can order through our online store or by calling 1-800-854-9899.
        </div>
                      </div>
                      <div class="toggle faq faq-aboutjhm faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-credit"></i><i class="toggle-open icon-credit"></i>Can I set up automatic monthly deductions by check or credit card? </div>
                        <div class="togglec">Yes. Please contact our data entry department at 1-800-600-9743 Monday through Friday from 8:30am to 5:00pm CST and we will be happy to assist you.</div>
                      </div>
                      <div class="toggle faq faq-submitmaterial faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-email"></i><i class="toggle-open icon-email"></i>Can I send material to John Hagee Ministries for Randy Travis?</div>
                        <div class="togglec">We cannot accept any material on behalf of Randy Travis. Material sent will not be returned.</div>
                      </div>
                      <div class="toggle faq faq-submitmaterial faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-email"></i><i class="toggle-open icon-email"></i>Can I send products anonymously to another person?</div>
                        <div class="togglec">We are unable to send product anonymously.</div>
                      </div>
                      <div class="toggle faq faq-submitmaterial faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-email"></i><i class="toggle-open icon-email"></i>Can I submit literary or music material for review?</div>
                        <div class="togglec">We are unable to accept unsolicited material of any kind for review or endorsement. Items will not be returned.</div>
                      </div>
                      <div class="toggle faq faq-aboutjhm faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-newspaper"></i><i class="toggle-open icon-newspaper"></i>Do you offer a subscription service for sermons?</div>
                        <div class="togglec">Yes, for more information on our subscription service please contact 1-800-600-9743 and ask for extension 1022.</div>
                      </div>
                      <div class="toggle faq faq-saltcovenant faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>How can I become a salt covenant partner?</div>
                        <div class="togglec">If you would like to talk to someone about becoming a partner you can call 1-800-600-9743 Monday through Friday from 8:30am to 5:00pm CST and ask for our Partner Department.</div>
                      </div>
                      <div class="toggle faq faq-shopandorders">
                        <div class="togglet"><i class="toggle-closed icon-user"></i><i class="toggle-open icon-user"></i>How can I change my account information?</div>
                        <div class="togglec">Please contact our customer service department at 1-800-600-9743 Monday through Friday from 8:30am to 5:00pm CST or email us at <a href="mailto:support@jhm.org">support@jhm.org.</a></div>
                      </div>
                      <div class="toggle faq faq-shopandorders">
                        <div class="togglet"><i class="toggle-closed icon-shopping-cart"></i><i class="toggle-open icon-shopping-cart"></i>How can I check the status of my order?</div>
                        <div class="togglec">To check the status of your order you can email support@jhm.org or call our customer service department at 1-800-600-9743 Monday through Friday from 8:30am to 5:00pm CST. </div>
                      </div>
                      <div class="toggle faq faq-shopandorders">
                        <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>How can I get my defective product replaced?</div>
                        <div class="togglec">Please contact our customer service department at 1-800-600-9743 Monday through Friday from 8:30am to 5:00pm CST or email us at <a href="mailto:support@jhm.org">support@jhm.org.</a> A return merchandise authorization (RMA) number is required before returning an item to the ministry. All damaged or defective material will be replaced with the same item within 60 days of ship date. Any shrink-wrapped item (unopened and intact) may be returned for a refund or exchagne as long as contact has been made with JHM customer service within 60 days of ship date. The following items may NOT be returned for refund or exchange: (1) any item with shrink wrap removed, unless damaged and (2) all books.</div>
                      </div>
                      <div class="toggle faq faq-aboutjhm faq-submitmaterial faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>How can I get my movie script, book manuscript, poem, etc reviewed?</div>
                        <div class="togglec">We do not accept unsolicited material. Any unsolicited pitches, outlines, teleplays, stories, poems, books, transcripts or screenplays will not be reviewed and due to cost will not be returned unless a method of pre-paid postage is included in the original mailing. Unsolicited emails will be deleted without being read. We cannot forward any of the above mentioned material to other companies or people.</div>
                      </div>
                      <div class="toggle faq faq-submitmaterial faq-miscellaneous">
                        <div class="togglet"><i class="toggle-closed icon-envelope2"></i><i class="toggle-open icon-envelope2"></i>How can I send my order/donation through the mail?</div>
                        <div class="togglec">All orders and donations can be mailed to: John Hagee Ministries P.O. Box 1400, San Antonio, Texas 78295-1400. Checks should be made payable to John Hagee Ministries.</div>
                      </div>
                      <div class="toggle faq faq-aboutjhm">
                        <div class="togglet"><i class="toggle-closed icon-envelope2"></i><i class="toggle-open icon-envelope2"></i>How can I sign up for Pastor’s weekly email?</div>
                        <div class="togglec">Send your email address to <a href="mailto:support@jhm.org">support@jhm.org</a> and ask to be added to Pastor’s weekly ministry email updates.</div>
                      </div>
                      <div class="toggle faq faq-aboutjhm">
                        <div class="togglet"><i class="toggle-closed icon-phone"></i><i class="toggle-open icon-phone"></i>How do I contact Christians United For Israel?</div>
                        <div class="togglec">You can reach Christians United for Israel (CUFI) through their website at <a href="http://www.cufi.org" target="_blank">www.cufi.org</a> or by calling 1-210-477-4714.</div>
                      </div>
                      <div class="toggle faq faq-aboutjhm">
                        <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>I am coming to visit Cornerstone Church, how can I see if Pastor will be preaching that Sunday?</div>
                        <div class="togglec">E-mail us at <a href="mailto:support@jhm.org">support@jhm.org</a> and let us know the dates of your visit and we will find out your requested information.</div>
                      </div>
                      <div class="toggle faq faq-aboutjhm">
                        <div class="togglet"><i class="toggle-closed icon-map"></i><i class="toggle-open icon-map"></i>Is there a map and list of service times for Cornerstone Church?</div>
                        <div class="togglec">Follow this link to a map for Cornerstone Church: <a href="http://www.mapquest.com/maps/18755+Stone+Oak+Parkway+San+Antonio+TX+78258/" target="_blank">Map to Cornerstone Church.</a> Our services times are 8:30am and 11:00am CST on Sunday morning and 6:30pm CST on Sunday night.</div>
                      </div>
                      <div class="toggle faq faq-rootsinisrael">
                        <div class="togglet"><i class="toggle-closed icon-leaf"></i><i class="toggle-open icon-leaf"></i>Plant Your Roots In Israel: Do I get a certificate for each tree planted?</div>
                        <div class="togglec">Each donation allows for one certificate.</div>
                      </div>
                      <div class="toggle faq faq-rootsinisrael">
                        <div class="togglet"><i class="toggle-closed icon-leaf"></i><i class="toggle-open icon-leaf"></i>Plant Your Roots In Israel: How large are the trees?</div>
                        <div class="togglec">The trees are all saplings.</div>
                      </div>
                      <div class="toggle faq faq-rootsinisrael">
                        <div class="togglet"><i class="toggle-closed icon-leaf"></i><i class="toggle-open icon-leaf"></i>Plant Your Roots In Israel: How often are the trees being planted?</div>
                        <div class="togglec">Mainly in the New Year (Winter thru Spring).</div>
                      </div>
                      <div class="toggle faq faq-rootsinisrael">
                        <div class="togglet"><i class="toggle-closed icon-leaf"></i><i class="toggle-open icon-leaf"></i>Plant Your Roots In Israel: If I go to Israel, will I get to see which tree(s) are mine?</div>
                        <div class="togglec">The trees are not able to be identified by name, but know that you are blessing Israel and the Jewish people.</div>
                      </div>
                      <div class="toggle faq faq-rootsinisrael">
                        <div class="togglet"><i class="toggle-closed icon-leaf"></i><i class="toggle-open icon-leaf"></i>Plant Your Roots In Israel: Is there any limit to the number of trees I can plant?</div>
                        <div class="togglec">No. You can plant as many trees as you wish.</div>
                      </div>
                      <div class="toggle faq faq-rootsinisrael">
                        <div class="togglet"><i class="toggle-closed icon-leaf"></i><i class="toggle-open icon-leaf"></i>Plant Your Roots In Israel: My question is still not answered, now what do I do?</div>
                        <div class="togglec">Call our John Hagee Ministries Orderline at 1-800-854-9899 and one of our representatives will be glad to assist you. </div>
                      </div>
                      <div class="toggle faq faq-rootsinisrael">
                        <div class="togglet"><i class="toggle-closed icon-leaf"></i><i class="toggle-open icon-leaf"></i>Plant Your Roots In Israel: What kinds of trees are being planted?</div>
                        <div class="togglec">Eucalyptus, Pine, Acacia, Casuarina (resembles a long-needled pine), local species like Carob,
        Pistacia, Oak, Pomegranate, Sycamore Fig, Fig and Almonds.</div>
                      </div>
                      <div class="toggle faq faq-rootsinisrael">
                        <div class="togglet"><i class="toggle-closed icon-leaf"></i><i class="toggle-open icon-leaf"></i>Plant Your Roots In Israel: Where are the trees being planted?</div>
                        <div class="togglec">Primarily across the Negev Desert, the desert region in the southern part of Israel. </div>
                      </div>
                      <div class="toggle faq faq-rootsinisrael">
                        <div class="togglet"><i class="toggle-closed icon-leaf"></i><i class="toggle-open icon-leaf"></i>Plant Your Roots In Israel: Who is going to plant the trees?</div>
                        <div class="togglec">E-mail us at <a href="mailto:support@jhm.org">support@jhm.org</a> and we will find out your requested information.</div>
                      </div>
                      <div class="toggle faq faq-rootsinisrael">
                        <div class="togglet"><i class="toggle-closed icon-leaf"></i><i class="toggle-open icon-leaf"></i>Plant Your Roots In Israel: Why is it important to plant trees in Israel?</div>
                        <div class="togglec">
                                <ul>
                                    <li>To fulfill Isaiah 41:19-20</li>
                                    <li>To transform the barren desert into fruitful, productive land</li>
                                    <li>To create jobs</li> 
                                    <li>To provide needed irrigation</li>
                                    <li>To sustain the local economy</li>
                                </ul>
                        </div>
                      </div>
                      <div class="toggle faq faq-rootsinisrael">
                        <div class="togglet"><i class="toggle-closed icon-leaf"></i><i class="toggle-open icon-leaf"></i>Plant Your Roots In Israel: Will I get to see a picture of the tree(s) I planted?</div>
                        <div class="togglec">We are unable to provide photographs, but you will receive a certificate.</div>
                      </div>
                      <div class="toggle faq faq-aboutjhm">
                        <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>What denomination is Cornerstone Church?</div>
                        <div class="togglec">Cornerstone Church is a non-denominational church that believes that the Bible is the true and inspired Word of God.</div>
                      </div>
                      <div class="toggle faq faq-shopandorders">
                        <div class="togglet"><i class="toggle-closed icon-desktop"></i><i class="toggle-open icon-desktop"></i>What do I do if I receive an error message while placing an internet order?</div>
                        <div class="togglec">Call our Data Entry department at 1-800-600-9743 ask for extension 1032, Monday through Friday from 8:30am to 5:00pm CST or email <a href="mailto:support@jhm.org">support@jhm.org</a></div>
                      </div>
                      <div class="toggle faq faq-saltcovenant">
                        <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>What is a Salt Covenant Partner?</div>
                        <div class="togglec">A Salt Covenant Partner is someone that pledges to support John Hagee Ministries with your prayers and a financial gift each month or on a continual basis each year to proclaim the Gospel of Jesus Christ to the nations of the world.</div>
                      </div>
                      <div class="toggle faq faq-shopandorders">
                        <div class="togglet"><i class="toggle-closed icon-shopping-cart"></i><i class="toggle-open icon-shopping-cart"></i>What is the shipping cost for international orders?</div>
                        <div class="togglec">We do not have a flat rate for international orders. To receive a quote, call our order line at 1-800-854-9899, 7 days a week from 6:00am to 12:00am CST.</div>
                      </div>
                      <div class="toggle faq faq-shopandorders">
                        <div class="togglet"><i class="toggle-closed icon-shopping-cart"></i><i class="toggle-open icon-shopping-cart"></i>What shipping methods do you use for product orders and what is the timeframe? </div>
                        <div class="togglec">We use both regular and RUSH shipping methods for orders withing the US. All international orders are sent RUSH via Fed-Ex. Regular shipping (USPS): 2-3 weeks. Rush Shipping (Fed-Ex): 5-7 work days. WE CANNOT SHIP VIA FED-EX TO A P.O. BOX. International orders (Fed-Ex): 4-6 weeks.</div>
                      </div>
                    </div>
                    <script type="text/javascript">
                                    jQuery(document).ready(function($){
                                        var $faqItems = $('#faqs .faq');
                                        if( window.location.hash != '' ) {
                                            var getFaqFilterHash = window.location.hash;
                                            var hashFaqFilter = getFaqFilterHash.split('#');
                                            if( $faqItems.hasClass( hashFaqFilter[1] ) ) {
                                                $('#portfolio-filter li').removeClass('activeFilter');
                                                $( '[data-filter=".'+ hashFaqFilter[1] +'"]' ).parent('li').addClass('activeFilter');
                                                var hashFaqSelector = '.' + hashFaqFilter[1];
                                                $faqItems.css('display', 'none');
                                                if( hashFaqSelector != 'all' ) {
                                                    $( hashFaqSelector ).fadeIn(500);
                                                } else {
                                                    $faqItems.fadeIn(500);   
                                                }
                                            }                                    
                                        }
        
                                        $('#portfolio-filter a').click(function(){
                                            $('#portfolio-filter li').removeClass('activeFilter');
                                            $(this).parent('li').addClass('activeFilter');
                                            var faqSelector = $(this).attr('data-filter');
                                            $faqItems.css('display', 'none');
                                            if( faqSelector != 'all' ) {
                                                $( faqSelector ).fadeIn(500);
                                            } else {
                                                $faqItems.fadeIn(500);
                                            }
                                            return false;
                                       });
                                    });
                         
               </script>
                  
                </div><!-- .postcontent end -->
       		</div>

            <div class="clear"></div>
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>