<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="js/jquery.gmap.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Salt Covenant Partnership</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>


 		<!-- Page Header
		============================================= -->
        <?php include "include/pageheader-saltcovenant-partnership.php";?>
       

		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="partner.php">Partner</a></li>					
                    <li class="active">Salt Covenant Partnership</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="section noborder notopmargin nobottommargin" style="background-color:#e2e2e2;">
	 
                <div data-animate="fadeIn" class="container clearfix">

				 	<div class="bottommargin-sm">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/S2cUOzzlUAo" frameborder="0" allowfullscreen></iframe>
					</div>
					
					<blockquote>
                    	<p>Season all your grain offerings with salt. Do not leave the salt of the covenant of your God out of your grain offerings; add salt to all your offerings.</p>
                     	<footer>Leviticus 2:13</footer>
                    </blockquote>

					<blockquote>
                    	<p>Don’t you know that the Lord, the God of Israel, has given the kingship of Israel to David and his descendants forever by a covenant of salt?</p>
                     	<footer>2 Chronicles 13:5</footer>
                    </blockquote>

					<p>Over the years the Lord has led many precious friends of this ministry to become <strong>SALT COVENANT PARTNERS</strong> through both prayer and financial support. If you are currently a partner with us, you <strong>“go the extra mile”</strong> by making a monthly financial commitment for the costs of our television outreach and <strong><em>we thank you for your sacrifice!</em></strong>

					<p><strong>If you are not a partner with John Hagee Ministries,</strong> you may be asking the question, <strong>“WHAT IS A SALT COVENANT?”</strong> In biblical times, men carried a pouch of salt on their belts to prevent dehydration as well as to trade it as a form of currency. When two men desired to finalize a business transaction or enter into a covenant of loyalty with each other, they would recite the conditions of their covenant and then exchange salt from each other’s pouch. Once they exchanged salt, they shook the salt grains within their pouches symbolizing the sealing of the covenant which became binding for it was impossible to retrieve their original salt grains from one another.</p>

					<h3 class="tcolorgold heading">Who is a Salt Covenant Partner?</h3>

					<h4 class="heading">Your Commitment To Us</h4>
						<p><strong>A SALT COVENANT PARTNER</strong> of John Hagee Ministries is someone who pledges to send an offering of any amount to John Hagee Ministries each month or on a continual basis each year to help us proclaim the <strong><em>Gospel of Jesus Christ to America and the nations of the world.</em></strong> Our partners also pledge to pray daily for our ministry as together we take the Good News to the lost.</p>

					<h4 class="heading">Our Commitment To You</h4>
						<p><strong>Our Salt Covenant with our partners</strong> is to be absolutely committed to change America and the world by being obedient to the Great Commission, to win the lost to Christ, to take America and the world back to the God of Abraham, Isaac and Jacob. </p>
					
					<div class="line topmargin-sm bottommargin-xsm"></div>

					<h2 class="heading tcolorgold">Pastor Hagee promises to preach an uncompromised message of love, healing and deliverance to the lost, hurting and oppressed. In addition our ministry also pledges to pray for our partners and their loved ones on a daily basis.</h2>

					<h4 class="heading">How Does One Become A Salt Covenant Partner?</h4>
						<p>You may become a <strong>SALT COVENANT PARTNER</strong> today by simply completing the enclosed form. With your initial gift you will receive two salt pouches symbolizing your covenant with Pastor and Diana Hagee, a CD titled What Is a Salt Covenant and a Salt Covenant Partner Card.</p>

					<div class="well">

						<h4 class="heading">The Salt Covenant Partner benefits include:</h4>
                        <ul class="iconlist">
                            <li><i class="icon-check"></i> 10% discount on ministry material</li>
                            <li><i class="icon-check"></i> Our staff prays for you daily</li>
                            <li><i class="icon-check"></i> Toll-free number to our Partner Relations Department for special prayer needs</li>
                            <li><i class="icon-check"></i> Advance seating at special John Hagee Ministry sponsored events</li>
							<li><i class="icon-check"></i> Early notice on ministry partner trips</li>
                            <li><i class="icon-check"></i> We will inform you when Pastor Hagee is planning to be in your area</li>
                            <li><i class="icon-check"></i> Bi-monthly ministry magazine</li>
                            <li><i class="icon-check"></i> Teleministry prayer partners will stay in touch with you</li>
							<li><i class="icon-check"></i> Periodic updates from Pastor and Diana including weekly e-mails from Pastor Hagee’s desk</li>
						</ul>
					</div>

				</div>

    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>