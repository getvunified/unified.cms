<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="js/jquery.gmap.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Donate to JHM</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>


 		<!-- Page Header
		============================================= -->
        <?php include "include/pageheader-donatetojhm.php";?>
       

		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="giving-opportunities.php">Giving Opportunities</a></li>					
                    <li class="active">Donate to JHM</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="section noborder nomargin nopadding">
	 
                <div class="container clearfix">
       <p>John Hagee Ministries is now being seen or heard in over 245 nations around the world and being broadcasted into 255 million homes worldwide. Many of the countries currently receiving our program are ravaged by civil war, famine, natural disasters.
Together we are truly helping to fulfill the Great Commission!</p>

<p>Our annual television air time budget presently exceeds $14,000,000, most of which goes to purchasing time on Christian Networks. Every telecast you see on TV is paid for... there are NO FREE TELECASTS on any network!</p>

<p>We want to continue to take one heart at a time, one soul at a time, one home at a time with the Gospel of Jesus Christ!
Thank you once again for your one time donation to help see this vision accomplished.We have faith and confidence that together, we can "do all things through Christ who strengthens us!"</p>

<p>Dear Partner, 
Thank you for permission to withdraw your monthly gift from your checking or savings account. We will be happy to process this request, but we need a completed authorization form in order to coordinate the transaction with your bank. Please complete the attached Automatic Withdrawal form below with a voided check (deposit slips are not acceptable) or your savings account number. Return to: John Hagee Ministries Attn: Customer Service P.O. Box 1400 San Antonio, Texas 78295 Please call 1-800-854-9899 if you are in need of further assistance.</p>

   </div>
</div>

</section>
              <!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>