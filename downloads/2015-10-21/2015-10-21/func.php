<?php
function draw_calendar($month,$year){
    
    //set up some variables
    $compare_date = $year . '-' . $month . '-1';
    $prev_month = date('n', strtotime($compare_date . '-1 month'));
    $prev_year = date('Y', strtotime($compare_date . '-1 month'));
    $next_month = date('n', strtotime($compare_date . '+1 month'));
    $next_year = date('Y', strtotime($compare_date . '+1 month'));
    
    //start drawing table
    $calendar = '<h2>' . date('F Y', strtotime($compare_date)) . '</h2>';
    $calendar .= '<a class="load-cal load-cal-prev" href="load-cal.php?m=' . $prev_month . '&y=' . $prev_year . '">Previous</a>';
    $calendar .= '<a class="load-cal load-cal-next" href="load-cal.php?m=' . $next_month . '&y=' . $next_year . '">Next</a>';
    $calendar .= '<table cellpadding="0" cellspacing="0" class="calendar">';

    //table headings
    $headings = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
    $calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

    //days and weeks variables
    $running_day = date('w',mktime(0,0,0,$month,1,$year));
    $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
    $days_in_this_week = 1;
    $day_counter = 0;

    //row for week one
    $calendar.= '<tr class="calendar-row">';

    //print "blank" days until the first of the current week
    for($x = 0; $x < $running_day; $x++):
        $calendar.= '<td class="calendar-day-empty"> </td>';
        $days_in_this_week++;
    endfor;

    //keep going with days
    for($list_day = 1; $list_day <= $days_in_month; $list_day++):
        $calendar.= '<td class="calendar-day">';
        /* ADD IN DAY AND LINK */
        /* JOHN - CUSTOMIZE THIS SECTION TO LINK TO WHEREVER YOU NEED
         */
        $link_date = $year . '-' . $month . '-' . $list_day;
        $event_type = isset($_GET['event_type']) && is_numeric($_GET['event_type']) ? '&event_type=' . $_GET['event_type'] : '';
        $day_link = '#';
        
        $calendar.= '<div class="day-number"><a href="' . $day_link . '">'.$list_day.'</a></div>';
        $calendar.= '</td>';
        
        if($running_day == 6):
            $calendar.= '</tr>';
            if(($day_counter+1) != $days_in_month):
                $calendar.= '<tr class="calendar-row">';
            endif;
            $running_day = -1;
            $days_in_this_week = 0;
        endif;
        $days_in_this_week++; $running_day++; $day_counter++;
    endfor;

    // finish the rest of the days in the week
    if($days_in_this_week < 8):
        for($x = 1; $x <= (8 - $days_in_this_week); $x++):
            $calendar.= '<td class="calendar-day-np"> </td>';
        endfor;
    endif;

    //finish off table
    $calendar.= '</tr>';
    $calendar.= '</table>';

    //all done, return result
    return $calendar;
}