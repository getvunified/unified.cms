<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>

		<link rel="stylesheet" href="css/responsive-tabs.css">
    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Ways To watch</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-waystowatch.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">Connect</a></li>
                    <li class="active">Ways To Watch</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
			<div class="content-wrap noborder nomargin nobottompadding toppadding-xsm">

				<div data-animate="fadeIn" class="container clearfix">

					<div class="heading-block noborder center">
                        <h2>How to Watch</h2>
                        <p>Select your viewing device</p>
                    </div>

					<div id="processTabs">
						<ul class="process-steps process-3 bottommargin clearfix">
							<li>
                            	<img src="images/ways-icon-tv.png" style="width:50%; height:50%;" alt="Cable TV">
                                <h3 class="heading">Cable TV</h3>
                                <a href="#ptab1" class="button button-border">Where to Watch</a>
							</li>
							<li>
                                <img src="images/ways-icon-stream.png" style="width:50%; height:50%;" alt="Online Stream">
                                <h3 class="heading">Online</h3>
                                <a href="#ptab2" class="button button-border">Where to Watch</a>
							</li>
							<li>
                                <img src="images/ways-icon-roku.png" style="width:50%; height:50%;" alt="Roku Device">
                                <h3 class="heading">Roku</h3>
                                <a href="#ptab3" class="button button-border">Where to Watch</a>
							</li>
						</ul>
						<div>

							<div class="line topmargin-sm bottommargin-sm"></div>

							
							
							<div id="ptab1">
								<div class="tabs tabs-bordered clearfix" id="tab-1">
                                    <div class="heading-block noborder center">
                                        <h2>Cable TV</h2>
                                        <p>Some text or description here.</p>
                                    </div>
                                    <ul class="tab-nav clearfix">
                                        <li class="icons"><a href="#tabs-1"><img class="rightmargin-xsm" src="images/ways-us-flag-small.png">United States</a></li>
                                        <li class="icons"><a href="#tabs-2"><img class="rightmargin-xsm" src="images/ways-canada-flag-small.png">Canada</a></li>
                                        <li class="icons"><a href="#tabs-3"><img class="rightmargin-xsm" src="images/ways-uk-flag-small.png">United Kingdom</a></li>
                                    </ul>
                                    <div class="tab-container">
                                        <div class="tab-content clearfix" id="tabs-1">
                                           <h4 class="heading">CABLE TV - United States Air Dates and Times</h4>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>San Antonio Local Stations</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>WOAI NBC</td>
                                                            <td>SUN 9:00AM</td>
                                                          </tr>
                                                          <tr>
                                                            <td>2</td>
                                                            <td>KMYS CW 35</td>
                                                            <td>THE DIFFERENCE WITH PASTOR MATT & KENDAL SUN 8:30AM</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>


											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>TBN - Trinity Boradcasting Network</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>TBN</td>
                                                            <td>M-F 6:30AM, 1:00PM PST / SUN 1:00PM PST</td>
                                                          </tr>
                                                          <tr>
                                                            <td>2</td>
                                                            <td>Enlace - en Espanol</td>
                                                            <td>SUN 8:30AM PST</td>
                                                          </tr>
														  <tr>
                                                            <td>3</td>
                                                            <td>The Church Channel</td>
                                                            <td>THURS 5:30PM PST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Daystar Network</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>Daystar Network</td>
                                                            <td>M-F 6:30PM CST, TUE 10:00PM CST / SUN 8:00AM CST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>God TV</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>God TV</td>
                                                            <td>SUN 8:00AM EST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>TCT Network</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
															<th>City and State</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>WDWO</td>
                                                            <td>Detroit, MI</td>
															<td>M-F 7:00AM, 10:00PM EST / SUN 8:00AM EST</td>
                                                          </tr>
														  <tr>
                                                            <td>2</td>
                                                            <td>WINM</td>
                                                            <td>Angola, IN</td>
															<td>M-F 7:00AM, 10:00PM EST / SUN 8:00AM EST</td>
                                                          </tr>
														  <tr>
                                                            <td>3</td>
                                                            <td>WRLM</td>
                                                            <td>Canton, OH</td>
															<td>M-F 7:00AM, 10:00PM EST / SUN 9:00AM EST</td>
                                                          </tr>
														  <tr>
                                                            <td>4</td>
                                                            <td>WAQP</td>
                                                            <td>Saginaw, MI</td>
															<td>M-F 7:00AM, 10:00PM EST / SUN 8:00AM EST</td>
                                                          </tr>
														  <tr>
                                                            <td>5</td>
                                                            <td>WNYB</td>
                                                            <td>Jamestown, NY</td>
															<td>M-F 7:00AM, 10:00PM EST / SUN 8:00AM EST</td>
                                                          </tr>
														  <tr>
                                                            <td>6</td>
                                                            <td>WLXI</td>
                                                            <td>Greensboro, NC</td>
															<td>M-F 7:00AM, 10:00PM EST / SUN 9:00AM EST</td>
                                                          </tr>
														  <tr>
                                                            <td>7</td>
                                                            <td>WRAY</td>
                                                            <td>Wilson, NC</td>
															<td>M-F 7:00AM, 10:00PM EST / SUN 8:00AM EST</td>
                                                          </tr>
														  <tr>
                                                            <td>8</td>
                                                            <td>WTLJ</td>
                                                            <td>Muskegon, MI</td>
															<td>M-F 7:00AM, 10:00PM EST / SUN 8:00AM EST</td>
                                                          </tr>
														  <tr>
                                                            <td>9</td>
                                                            <td>WTCT</td>
                                                            <td>Marion, IL</td>
															<td>M-F 6:00AM, 9:00PM CST / SUN 7:00AM CST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Superchannel WACX TV</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>City and State</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>WACX</td>
															<td>Leesburg, FL</td>
                                                            <td>M-F 11:30 AM EST / SUN 7:00AM EST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>KCHF TV 11</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>City and State</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>KCHF</td>
															<td>Santa Fe, NM</td>
                                                            <td>M-F 6:30AM, 7:30PM MST / SUN 12:00PM MST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>VTN</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>City and State</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>KVTN</td>
															<td>Pine Bluff, AR</td>
                                                            <td>M-F 9:30PM, 12:30AM, 4PM • 2AM WED CST / SUN 12:00PM CST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>WPBM</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>City and State</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>WPBM</td>
															<td>Scottsville, KY</td>
                                                            <td>M-F 7:30AM,4:30PM, 3:30AM CST / SUN 2:00PM, 10:00PM</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>KAJN</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>City and State</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>KAJN</td>
															<td>Lafayette, LA</td>
                                                            <td>M-F 11:30AM CST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>WMTM</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>City and State</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>WMTM</td>
															<td>Winchester, VA</td>
                                                            <td>M-F 11:30AM EST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>WTLW 44</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>City and State</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>WTLW 44</td>
															<td>Lima, OH</td>
                                                            <td>M-F 7:00PM / SUN 7:00AM EST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Sacramento Faith TV</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>City and State</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>Sacramento Faith TV</td>
															<td>Rocklin, CA</td>
                                                            <td>WED 2:00PM / THURS 8:00AM PST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>

											<div class="toggle toggle-bg topmargin-sm">
												<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>KGSC TV</div>
												<div class="togglec">
													<div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>City and State</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>KGSC TV</td>
															<td>Cheyenne, WY</td>
                                                            <td>M-F 1:30PM / SUN 1:30PM MST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
												</div>
                                             </div>
                                        </div>

<!--CANADA--> 							
                                        <div class="tab-content clearfix" id="tabs-2">
                                           <h4 class="heading">CABLE TV - Canada Air Dates and Times</h4>
                                        
                                            <div class="toggle toggle-bg">
                                                <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Daystar Canada Network</div>
                                                <div class="togglec">
                                                    <div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>Daystar/Grace TV</td>
                                                            <td>M-F 6:30PM, TUE 10:00PM EST / SUN 9:00AM EST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </div>
                                             </div>
    
                                            <div class="toggle toggle-bg topmargin-sm">
                                                <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>YES TV</div>
                                                <div class="togglec">
                                                    <div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>CITS Ontario</td>
                                                            <td>SUN 1:00PM EST</td>
                                                          </tr>
                                                          <tr>
                                                            <td>2</td>
                                                            <td>CKCS Calgary</td>
                                                            <td>M-F 10:30PM MST</td>
                                                          </tr>
                                                          <tr>
                                                            <td>3</td>
                                                            <td>CKES Edmonton</td>
                                                            <td>M-F 10:30PM MST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </div>
                                             </div>
    
                                            <div class="toggle toggle-bg topmargin-sm">
                                                <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>CJIL - The Miracle Channel</div>
                                                <div class="togglec">
                                                    <div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>CJIL - The Miracle Channel</td>
                                                            <td>M-F 6:00PM MST / SUN 7:00PM MST</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </div>
                                             </div>
										</div>

                                        <div class="tab-content clearfix" id="tabs-3">
                                           <h4 class="heading">CABLE TV - United Kingdom Air Dates and Times</h4>

                                            <div class="toggle toggle-bg">
                                                <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>TBN UK</div>
                                                <div class="togglec">
                                                    <div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>TBN UK</td>
                                                            <td>M- F 5:30AM, 12:00PM / SUN 12:00PM GMT</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </div>
                                             </div>
    
                                            <div class="toggle toggle-bg">
                                                <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Daystar</div>
                                                <div class="togglec">
                                                    <div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>Daystar</td>
                                                            <td>TUE-SAT 4:30AM, WED 8:00AM / SUN 6:00PM GMT</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </div>
                                             </div>
    
                                            <div class="toggle toggle-bg">
                                                <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Revelation TV</div>
                                                <div class="togglec">
                                                    <div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>Revelation TV</td>
                                                            <td>WED 2:00PM, 7:00PM GMT</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </div>
                                             </div>
    
                                            <div class="toggle toggle-bg">
                                                <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>GOD TV</div>
                                                <div class="togglec">
                                                    <div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>GOD TV</td>
                                                            <td>SUN 8:00AM GMT</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </div>
                                             </div>
    
                                            <div class="toggle toggle-bg">
                                                <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Faith Family Broadcasting of Networks</div>
                                                <div class="togglec">
                                                    <div class="table-responsive">
                                                      <table class="table table-bordered nobottommargin table-striped">
                                                        <thead>
                                                          <tr>
                                                            <th>#</th>
                                                            <th>Station Name</th>
                                                            <th>Broadcast Time</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>1</td>
                                                            <td>Clear TV, Flow TV, Go TV</td>
                                                            <td>Africa, South Africa, UK M-F 5:30PM</td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </div>
                                             </div>
										</div>
                                    
									</div>
                                </div>							
							</div>
							
							<div id="ptab2">
								<div class="tabs tabs-bordered clearfix" id="tab-2">

                                    <div class="heading-block noborder center">
                                        <h2>Online Stream</h2>
                                        <p>Some text or description here.</p>
                                        <h4 class="heading">ONLINE STREAM Information here.</h4>
                                    </div>
                                </div>							
							</div>
							<div id="ptab3">
								<div class="tabs tabs-bordered clearfix" id="tab-3">

                                    <div class="heading-block noborder center">
                                        <h2>Roku Device</h2>
                                        <p>Some text or description here.</p>
                                        <h4 class="heading">Roku Device Information here.</h4>
                                    </div>
                                </div>							
							</div>

							<script>
                              $(function() {
                                $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
                                $( ".tab-linker" ).click(function() {
                                    $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
                                    return false;
                                });
                              });
                            </script>
							
							





					<!-- For mobile size
							<div id="ptab1" class="hidden-lg">
								<div class="toggle toggle-bg topmargin-sm">
                                    <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Where to Watch - Cable TV</div>
                                    <div class="togglec">
										<p><strong>United States</strong></p>
										Info here
										<p><strong>UCanada</strong></p>
										Info here
										<p><strong>United Kingdom</strong></p>
										Info here
                                     </div>
								</div>						
							</div>
							
							<div id="ptab2" class="hidden-lg">
								<div class="toggle toggle-bg topmargin-sm">
                                    <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Online Stream</div>
                                    <div class="togglec">
										<p><strong>United States</strong></p>
										Info here
										<p><strong>UCanada</strong></p>
										Info here
										<p><strong>United Kingdom</strong></p>
										Info here
                                     </div>
								</div>							
							</div>

							<div id="ptab3" class="hidden-lg">
								<div class="toggle toggle-bg topmargin-sm">
                                    <div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Where to Watch - Roku Device</div>
                                    <div class="togglec">
										<p><strong>United States</strong></p>
										Info here
										<p><strong>UCanada</strong></p>
										Info here
										<p><strong>United Kingdom</strong></p>
										Info here
                                     </div>
								</div>							
							</div>-->

							

						</div>
					</div>

					

					<div class="clear"></div>				

				</div>

            <div class="clear"></div>
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>








						