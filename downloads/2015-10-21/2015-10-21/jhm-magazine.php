<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | JHM Magazine</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-jhmmagazine.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">Connect</a></li>
                    <li class="active">JHM Magazine</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content" style="background-color:#f7f7f7;">

			<div class="content-wrap noborder notopmargin bottommargin nopadding">
	 
                <div data-animate="fadeIn" class="container clearfix">

				<div class="heading-block noborder topmargin bottommargin-sm center">
					<h2>Catalogs</h2>
					<p>Select the JHM Catalog to view.</p>
				</div>
                  <!-- Post Content
                        ============================================= -->
        
                    <div class="team team-list clearfix">
                        <div class="col_one_fourth nobottommargin col_respon">
                            <img src="images/jhm-catalog.jpg" style="width:231px; "alt="JHM Catalog">
                        </div>
 						<div class="col_three_fourth nobottommargin col_respon col_last">
                        	<div class="team-desc">
                                <div class="team-title">
									<h3 class="nobottommargin name">2013-2014 Catalog</h3><span>Published 3-29-2014</span>
								</div>
                            </div>
                            
                            <div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="team-content">
                                <p>Quisque sit amet porttitor sapien. Proin sed ante felis. Nulla facilisi. Nulla feugiat volutpat lectus vitae elementum. In pharetra metus ut dolor lacinia consectetur. Proin erat nunc, dignissim placerat nunc id, tempus auctor ipsum. Curabitur sit amet nunc augue. Nulla et quam laoreet, laoreet massa ut, viverra purus.</p>
                                <a href="#" class="button button-3d">View Catalog</a>
                            </div>
                        </div>
                    </div>
			</div>
 		</section><!-- #content end -->

		<section id="content">
			
            <div class="content-wrap noborder notopmargin bottommargin nopadding">
	 
                <div data-animate="fadeIn" class="container clearfix">
                    
                    <div class="heading-block noborder topmargin bottommargin-sm center">
                        <h2>Magazines</h2>
                        <p>Select a JHM Magazine to view.</p>
                    </div>

					<!-- Portfolio Filter
					============================================= -->
					<ul id="portfolio-filter" class="clearfix">

						<li class="activeFilter"><a href="#" data-filter="*">Show All Magazines</a></li>
						<li><a href="#" data-filter=".pf-2015">2015</a></li>
						<li><a href="#" data-filter=".pf-2014">2014</a></li>
						<li><a href="#" data-filter=".pf-2013">2013</a></li>
						<li><a href="#" data-filter=".pf-2012">2012</a></li>
						
					</ul><!-- #portfolio-filter end -->

					<div id="portfolio-shuffle">
						<i class="icon-random"></i>
					</div>

					<div class="clear"></div>

					<!-- Magazine Images
					============================================= -->
					<div id="portfolio" class="clearfix">

					<!--2015-->
						<article class="portfolio-item pf-2015">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_jan-feb-2015.png" alt="January - February 2015">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">January/February</h3>
								<span>2015</span>
							</div>
						</article>

						<article class="portfolio-item pf-2015">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_march-april-2015.png" alt="March - April 2015">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">March/April</h3>
								<span>2015</span>
							</div>
						</article>

						<article class="portfolio-item pf-2015">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_may-june-2015.png" alt="May - June 2015">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">May/June</h3>
								<span>2015</span>
							</div>
						</article>

						<article class="portfolio-item pf-2015">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_july-aug-2015.png" alt="July - August 2015">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">July/August</h3>
								<span>2015</span>
							</div>
						</article>

						<!--2014-->
						<article class="portfolio-item pf-2014">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_jan-feb-2014.png" alt="January - February 2014">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">January/February</h3>
								<span>2014</span>
							</div>
						</article>

						<article class="portfolio-item pf-2014">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_march-april-2014.png" alt="March - April 2014">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">March/April</h3>
								<span>2014</span>
							</div>
						</article>

						<article class="portfolio-item pf-2014">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_may-june-2014.png" alt="May - June 2014">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">May/June</h3>
								<span>2014</span>
							</div>
						</article>

						<article class="portfolio-item pf-2014">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_july-aug-2014.png" alt="July - August 2014">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">July/August</h3>
								<span>2014</span>
							</div>
						</article>

						<article class="portfolio-item pf-2014">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_sept-oct-2014.png" alt="September - October 2014">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">September/October</h3>
								<span>2014</span>
							</div>
						</article>

						<article class="portfolio-item pf-2014">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_nov-dec-2014.png" alt="November - December 2014">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">November/December</h3>
								<span>2014</span>
							</div>
						</article>

						<!--2013-->
						<article class="portfolio-item pf-2013">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_jan-feb-2013.png" alt="January - February 2013">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">January/February</h3>
								<span>2013</span>
							</div>
						</article>

						<article class="portfolio-item pf-2013">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_march-april-2013.png" alt="March - April 2013">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">March/April</h3>
								<span>2013</span>
							</div>
						</article>

						<article class="portfolio-item pf-2013">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_may-june-2013.png" alt="May - June 2013">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">May/June</h3>
								<span>2013</span>
							</div>
						</article>

						<article class="portfolio-item pf-2013">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_july-aug-2013.png" alt="July - August 2013">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">July/August</h3>
								<span>2013</span>
							</div>
						</article>

						<article class="portfolio-item pf-2013">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_sept-oct-2013.png" alt="September - October 2013">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">September/October</h3>
								<span>2013</span>
							</div>
						</article>

						<article class="portfolio-item pf-2013">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_nov-dec-2013.png" alt="November - December 2013">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">November/December</h3>
								<span>2013</span>
							</div>
						</article>
						
						<!--2012-->
						<article class="portfolio-item pf-2012">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_jan-feb-2012.png" alt="January - February 2012">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">January/February</h3>
								<span>2012</span>
							</div>
						</article>

						<article class="portfolio-item pf-2012">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_march-april-2012.png" alt="March - April 2012">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">March/April</h3>
								<span>2012</span>
							</div>
						</article>

						<article class="portfolio-item pf-2012">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_may-june-2012.png" alt="May - June 2012">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">May/June</h3>
								<span>2012</span>
							</div>
						</article>

						<article class="portfolio-item pf-2012">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_july-aug-2012.png" alt="July - August 2012">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">July/August</h3>
								<span>2012</span>
							</div>
						</article>


						<article class="portfolio-item pf-2012">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_sept-oct-2012.png" alt="September - October 2012">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">September/October</h3>
								<span>2012</span>
							</div>
						</article>

						<article class="portfolio-item pf-2012">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/mags/JHMmag_nov-dec-2012.png" alt="November - December 2012">
								</a>
								<div class="portfolio-overlay">
									<a href="#" class="center-icon"><i class="icon-line-link icon-2x"></i></a>
								</div>
							</div>
							<div class="portfolio-desc center bottommargin-sm">
								<h3 class="tcolorgold">November/December</h3>
								<span>2012</span>
							</div>
						</article>

					</div><!-- #portfolio end -->

					<!-- Portfolio Script
					============================================= -->
					<script type="text/javascript">

						jQuery(window).load(function(){

							var $container = $('#portfolio');

							$container.isotope({ transitionDuration: '0.65s' });

							$('#portfolio-filter a').click(function(){
								$('#portfolio-filter li').removeClass('activeFilter');
								$(this).parent('li').addClass('activeFilter');
								var selector = $(this).attr('data-filter');
								$container.isotope({ filter: selector });
								return false;
							});

							$('#portfolio-shuffle').click(function(){
								$container.isotope('updateSortData').isotope({
									sortBy: 'random'
								});
							});

							$(window).resize(function() {
								$container.isotope('layout');
							});

						});

					</script><!-- Portfolio Script End -->

				</div>
			</div>

            <div class="clear"></div>
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>