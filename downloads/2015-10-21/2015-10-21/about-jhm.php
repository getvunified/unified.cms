<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

	<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>About John Hagee Ministries</title>
</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	<div id="side-panel" class="dark">

		<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	</div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

<!-- Top Bar
        ============================================= 
        <div id="top-bar">

            <div class="container clearfix">

                <div class="col_half nobottommargin">

                    <!-- Top Links
                    ============================================= 
                    <div class="top-links">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            
                            <li><a href="contact.html">Contact</a></li>
                            <li><a href="login-register.html">Login</a>
                                <div class="top-link-section">
                                    <form id="top-login" role="form">
                                        <div class="input-group" id="top-login-username">
                                            <span class="input-group-addon"><i class="icon-user"></i></span>
                                            <input type="email" class="form-control" placeholder="Email address" required="">
                                        </div>
                                        <div class="input-group" id="top-login-password">
                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <label class="checkbox">
                                          <input type="checkbox" value="remember-me"> Remember me
                                        </label>
                                        <button class="btn btn-danger btn-block" type="submit">Sign in</button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div><!-- .top-links end 

                </div>

                <div class="col_half fright col_last nobottommargin">

                    <!-- Top Social
                    ============================================= 
                    <div id="top-social">
                        <ul>
                            <li><a href="#" class="si-play"><span class="ts-icon"><i class="icon-play"></i></span><span class="ts-text">GETV</span></a></li>
                            <li><a href="#" class="si-sun"><span class="ts-icon"><i class="icon-sun"></i></span><span class="ts-text">Cornerstone Church</span></a></li>
                            <li><a href="#" class="si-facetime-video"><span class="ts-icon"><i class="icon-facetime-video"></i></span><span class="ts-text">Difference Media</span></a></li>
                            <li><a href="#" class="si-pencil2"><span class="ts-icon"><i class="icon-pencil2"></i></span><span class="ts-text">Cornerstone School</span></a></li>
                            <li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
                            <li><a href="#" class="si-twitter2"><span class="ts-icon"><i class="icon-twitter2"></i></span><span class="ts-text">Twitter</span></a></li>
                            <li><a href="#" class="si-youtube"><span class="ts-icon"><i class="icon-youtube"></i></span><span class="ts-text">Youtube</span></a></li>
                            <li><a href="#" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
                            <li><a href="#" class="si-pinterest"><span class="ts-icon"><i class="icon-pinterest2"></i></span><span class="ts-text">Pinterest</span></a></li>
                            <li><a href="#" class="si-gplus"><span class="ts-icon"><i class="icon-gplus"></i></span><span class="ts-text">Google Plus</span></a></li>
                            <li><a href="tel:1-800-854-9899" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text"> 1-800-854-9899</span></a></li>
                            <li><a href="mailto:support@jhm.org" class="si-email3"><span class="ts-icon"><i class="icon-email3"></i></span><span class="ts-text">support@jhm.org</span></a></li>
                        </ul>
                    </div><!-- #top-social end 

                </div>

            </div>

        </div><!-- #top-bar end -->
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
		
         <!-- Page Title
        ============================================= -->
        <section id="page-title">

            <div class="container clearfix">
                <h1>About JHM</h1>
                <span>Discover more about JHM and its leadership</span>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li class="active">About JHM</li>
                </ol>
            </div>

        </section><!-- #page-title end -->
        
        <!-- Content
		============================================= -->
		<section id="content">
        
			<div class="section noborder notopmargin nopadding nobottommargin">
				<div class="container clearfix">
					
                    <div id="snav-content1">
                        <div class="heading-block noborder topmargin-sm">
                            <h1 class="tcolorgold">Pastor John C. Hagee</h1>
                            <span>Sr. Pastor Cornerstone Church</span>
                        </div>
                        <img src="images/john-hagee-bio.jpg" alt="Sr. Pastor C. Hagee" class="alignleft" style="max-width: 275px;">
                        <span><strong>Pastor John C. Hagee is the founder and Senior Pastor</strong> of Cornerstone Church in San Antonio, Texas, a non-denominational evangelical church with more than 20,000 active members. Pastor Hagee has served the Lord in the gospel ministry for over 57 years. Pastor Hagee is founder and President of John Hagee Ministries, which telecasts his national radio and television teachings throughout America and the nations of the world, and is now seen worldwide twenty-four hours a day, seven days a week on GETV.org.</span>
                        <div>&nbsp;</div>
    
    <span><strong>Pastor John Hagee</strong> received a Bachelor of Science degree from Southwestern Assemblies of God University in Waxahachie, Texas and a second Bachelor of Science degree from Trinity University in San Antonio, Texas. He earned his Master’s Degree in Educational Administration from the University of North Texas in Denton, Texas. Pastor Hagee has received Honorary Doctorates from Oral Roberts University, Canada Christian College, and Netanya Academic College of Israel.</span>
    					<div class="clear"></div>
 						<blockquote>
                        	<h3 class="tcolorgold">Successful Author</h3>
                            <span>John Hagee is the author of 34 major books, many of which were on the New York Times Best Seller’s List including “Four Blood Moons”, which is now featured as a successful docudrama film. His most recent book “The Three Heavens” will be released this spring by Worthy Publishing. Among his other works are commentary study Bibles, novels and numerous devotionals.</span>
                        </blockquote>

						<span><strong>Over the years, John Hagee Ministries</strong> has given more than $85 million toward humanitarian causes in Israel. In 2006, Dr. Hagee founded, and is the National Chairman of Christians United For Israel, a grass roots national association through which every pro—Israel Christian ministry, para-church organization, or individual in America can speak and act with one voice in support of Israel and the Jewish people. Dr. Hagee gains support for this worthy cause by conducting A Night to Honor Israel in every major city in America and by organizing an annual Washington D.C.-Israel Summit where thousands of CUFI delegates from every state in the union have the opportunity to meet the members of congress face to face on behalf of Israel.</span>
                        <div>&nbsp;</div>

						<span><strong>Christians United for Israel</strong> has grown to become the largest Christian pro-Israel organization in the United States – with over 2.1 million members-- and one of the leading Christian grassroots movements in the world. Christians United for Israel has recently opened offices in the United Kingdom and Canada to combat anti-Semitism. Pastor Hagee and his wife Diana are blessed with five children and thirteen grandchildren.</span>
                   <div>&nbsp;</div>
                    <a href="#" class="social-icon inline-block si-small si-light si-rounded si-facebook">
                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                    </a>
                    <a href="#" class="social-icon inline-block si-small si-light si-rounded si-twitter">
                        <i class="icon-twitter"></i>
                        <i class="icon-twitter"></i>
                    </a>
                    <a href="#" class="social-icon inline-block si-small si-light si-rounded si-gplus">
                        <i class="icon-gplus"></i>
                        <i class="icon-gplus"></i>
                    </a>
                </div>
        		<div class="line"></div>
                    
                    
                <div id="snav-content2">
                    <div class="heading-block noborder topmargin-sm">
                        <h1 class="tcolorgold">Diana Castro Hagee</h1>
                        <span>Special Events Coordinator Cornerstone Church</span>
                    </div>
                    <img src="images/photo-diana.jpg" alt="Diana Hagee" class="alignleft" style="max-width: 275px;">
                    <span><strong>Diana Hagee is the wife of Pastor John Hagee,</strong> founder and senior pastor of Cornerstone Church in San Antonio, Texas. She coordinates all special events for John Hagee Ministries, Cornerstone Church, Cornerstone Christian Schools, Christians United for Israel and leads the Women's Ministries at Cornerstone Church.</span>

                    <blockquote>
                        <h3 class="tcolorgold">Successful Author</h3>
                        <span>Diana is the author of the best-selling book The King's Daughter, which was awarded the Retailers Choice Award, The King’s Daughter Workbook, and Not by Bread Alone, a cookbook encouraging creative ministry through food as well as a book she co-authored with her husband entitled, What Every Man Wants in a Woman—What Every Woman Wants in a Man. Her most recent release is Ruth, the Romance of Redemption – A Love Story.</span>
                    </blockquote>

                    <span><strong>She is founder and director of the King’s Daughter,</strong> Becoming a Woman of God National Conferences. She was presented the prestigious Lion of Judah award by the Jewish Federation of Greater Houston for the long-standing work she and Pastor Hagee do on behalf of Israel and the Jewish people. Diana is very active with Christians United for Israel, which Pastor Hagee founded in 2006.</span>
                    <div>&nbsp;</div>

                    <span><strong>Diana holds a Bachelor’s Degree in Science</strong> from Trinity University in San Antonio, Texas. She and Pastor John Hagee have five children and twelve grandchildren.</span>
               <div>&nbsp;</div>
                <a href="#" class="social-icon inline-block si-small si-light si-rounded si-facebook">
                    <i class="icon-facebook"></i>
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="social-icon inline-block si-small si-light si-rounded si-twitter">
                    <i class="icon-twitter"></i>
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="social-icon inline-block si-small si-light si-rounded si-gplus">
                    <i class="icon-gplus"></i>
                    <i class="icon-gplus"></i>
                </a>
            </div>
        		<div class="line"></div>
    
                 <div id="snav-content3">
                    <div class="heading-block noborder topmargin-sm">
                        <h1 class="tcolorgold">Pastor Matthew Hagee</h1>
                        <span>Executive Pastor Cornerstone Church</span>
                    </div>
                    <img src="images/PastorMatthewHagee2015.jpg" alt="Pastor Matthew Hagee" class="alignleft" style="max-width: 275px;">
                    <span><strong>Pastor Matthew Charles Hagee</strong> is the sixth generation in the Hagee family to carry the mantel of Gospel ministry. He serves as the Executive Pastor of the 20,000 member Cornerstone Church in San Antonio, Texas where he partners with his father, founder Pastor John Hagee. Pastor Matthew Hagee’s teachings are telecast over world-wide television through John Hagee Ministries. He is fervently committed to preaching all the Gospel to all the world and to every generation.</span>
                   
                    <blockquote>
                        <h3 class="tcolorgold">Successful Author</h3>
                        <span>As a graduate of Oral Roberts University School of Business, he is an accomplished vocalist and is the author of “Shaken, Not Shattered” and recently released "Response-Able" both published by Strang Communications.</span>
                    </blockquote>
 					<div>&nbsp;</div>
                    <span><strong>Pastor Matthew and his wife Kendal</strong> are blessed with four children, Hannah Rose, John William, Joel Charles and Madison Katherine.</span>
                    <div>&nbsp;</div>

                    <span><strong>As their family and ministry grow,</strong> Pastor Matthew and Kendal have a desire equip the church of tomorrow to be the distinguished treasure God designed them to be. Together they seek to fulfill their divine destiny with the passion and purpose that can only come from the power of family tradition and the anointed call of God on their lives.</span>
               <div>&nbsp;</div>
                <a href="#" class="social-icon inline-block si-small si-light si-rounded si-facebook">
                    <i class="icon-facebook"></i>
                    <i class="icon-facebook"></i>
                </a>
                <a href="#" class="social-icon inline-block si-small si-light si-rounded si-twitter">
                    <i class="icon-twitter"></i>
                    <i class="icon-twitter"></i>
                </a>
                <a href="#" class="social-icon inline-block si-small si-light si-rounded si-gplus">
                    <i class="icon-gplus"></i>
                    <i class="icon-gplus"></i>
                </a>
            </div>
        		
                     
					
				</div>
			<div class="line"></div>
</div>
            
           
					
               

			<div class="clear"></div>
                           		<!-- JHM Connect
					============================================= -->
                    <?php include "include/jhm-connect.php";?>


        
        </section><!-- #content end -->

		<!-- Footer
		============================================= -->
        
					<!-- Footer
					============================================= -->
                    <?php include "include/footer.php";?>
                    
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>

</body>
</html>