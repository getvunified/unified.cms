<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
                <link rel="stylesheet" href="css/shop.css" type="text/css" />
                <script type="text/javascript" src="js/shop.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

                    <!-- Top Links 
                    ============================================= -->
                    <div class="top-links">
                        <ul>
                            <li><a href="#"> <i class="icon-line2-user"></i>Login</a>
                                <div class="top-link-section">
                                    <form id="top-login" role="form">
                                        <div class="input-group" id="top-login-username">
                                            <span class="input-group-addon"><i class="icon-mail"></i></span>
                                            <input type="email" class="form-control" placeholder="Email address" required="">
                                        </div>
                                        <div class="input-group" id="top-login-password">
                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <label class="checkbox">
                                          <input type="checkbox" value="remember-me"> Remember me
                                        </label>
                                        <button class="btn btn-danger btn-block" type="submit">Sign in</button>
                                    </form>
                                </div>
                            </li>
							<li><a href="#"><i class="icon-key"></i> Register</a>
								<div class="top-link-section">
                                    <form id="top-register" role="form">
                                        <div class="input-group" id="top-register-name">
                                            <span class="input-group-addon"><i class="icon-line2-user"></i></span>
                                            <input type="name" class="form-control" placeholder="Name" required="">
                                        </div>
										<div class="input-group" id="top-register-username">
                                            <span class="input-group-addon"><i class="icon-mail"></i></span>
                                            <input type="email" class="form-control" placeholder="Email address" required="">
                                        </div>
                                        <div class="input-group" id="top-register-password">
                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <button class="btn btn-danger btn-block" type="submit">Register</button>
                                    </form>
                                </div>
							</li>
                        </ul>
                    </div> <!--.top-links end -->

                </div>

            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Slider
		============================================= -->
        <?php include "include/slider.php";?>


        <!-- Content
        ============================================= -->
        <section id="content">
            <div id="shop-nav">
                <ul>
                    <li><a rel="#featured" href="#">Featured</a></li>
                    <li class="active"><a class="tab" rel="#categories" href="#">Categories</a></li>
                    <li><a class="tab" rel="#authors" href="#">Authors</a></li>
                    <li><a class="tab" rel="#subjects" href="#">Subject</a></li>
                </ul>
            </div>
            <div class="noborder nomargin nopadding topmargin-sm bottommargin-sm">
                <div class="container clearfix">
                    <div class="shop-checkout noborder topmargin">
                        <div class="shop-header shop-category-header">
                            <h2>Checkout</h2>
                            <div class="shop-item-sort">
                                <a href="#">Home</a> / <a href="#">Shop</a> / <a href="#">Shopping Cart</a> / Checkout
                            </div>
                        </div>
                        <div class="slide-wrapper">
                            <div class="slide-group">
                                <div class="slide-title">
                                    <h3>Step 1: Checkout Options</h3>
                                </div>
                                <div class="slide-content">
                                    <div class="col-xs-6">
                                        <h3>New Customer</h3>
                                        <h5>Checkout Options</h5>
                                        <div><input type="radio" name="checkout_options" /> Register Account</div>
                                        <div class="bottommargin-xsm"><input type="radio" name="checkout_options" /> Guest Checkout</div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eget lorem sed erat sagittis bibendum. Suspendisse ut turpis sed orci varius dictum mollis eget ex.</p>
                                        <a class="button button-3d" href="#"><span>Continue</span></a>
                                    </div>
                                    <div class="col-xs-6">
                                        <h3>Returning Customer</h3>
                                        <h5>I am a returning customer</h5>
                                        <label>Email</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>Password</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="password" />
                                        <input class="button button-blue" type="submit" value="Login" />
                                    </div>
                                </div>
                            </div>
                            <div class="slide-group">
                                <div class="slide-title">
                                    <h3>Step 2: Delivery Details</h3>
                                </div>
                                <div class="slide-content">
                                    <div class="col-xs-6">
                                        <h3 class="heading-border">Your Personal Details</h3>
                                        <label>* First Name</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Last Name</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Email</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Home Phone</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>Cell Phone</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                    </div>
                                    <div class="col-xs-6">
                                        <h3 class="heading-border">Delivery Address</h3>
                                        <label>Company</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Address 1</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>Address 2</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* City</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Zip Code</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Country</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Region/State</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                    </div>
                                    <a class="fright button button-3d" href="#"><span>Continue</span></a>
                                </div>
                            </div>
                            <div class="slide-group">
                                <div class="slide-title">
                                    <h3>Step 3: Delivery Method</h3>
                                </div>
                                <div class="slide-content">
                                    <table class="shop-cart">
                                        <thead>
                                            <tr>
                                                <th>Type</th>
                                                <th>Location</th>
                                                <th>Select Method</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><h4>Primary</h4></td>
                                                <td>John Smith<br />15305 Dallas Parkway<br />Addison, TX 75001</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td><h4>Gift</h4></td>
                                                <td>John Smith<br />15305 Dallas Parkway<br />Addison, TX 75001</td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <a class="fright button button-3d" href="#"><span>Continue</span></a>
                                </div>
                            </div>
                            <div class="slide-group">
                                <div class="slide-title">
                                    <h3>Step 4: Payment Details</h3>
                                </div>
                                <div class="slide-content">
                                    <div class="heading-border">
                                        <input type="checkbox" class="rightmargin-xsm" />My delivery and billing addresses are the same.
                                    </div>
                                    <div class="col-xs-6">
                                        <h3 class="heading-border">Payment Details</h3>
                                        <label>* Name on Card</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Card Number</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Expiration Date</label>
                                        <select class="input-shop input-half bottommargin-xsm">
                                            <option value="Month">Month</option>
                                        </select>
                                        <select class="input-shop input-half bottommargin-xsm">
                                            <option value="Year">Year</option>
                                        </select>
                                        <label>* CV2/CID</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                    </div>
                                    <div class="col-xs-6">
                                        <h3 class="heading-border">Billing Address</h3>
                                        <label>* First Name</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Last Name</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Address 1</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>Address 2</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* City</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Zip Code</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Country</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                        <label>* Region/State</label>
                                        <input class="input-shop input-full bottommargin-xsm" type="text" />
                                    </div>
                                    <a class="fright button button-3d" href="#"><span>Continue</span></a>
                                </div>
                            </div>
                            <div class="slide-group">
                                <div class="slide-title">
                                    <h3>Step 5: Summary</h3>
                                </div>
                                <div class="slide-content">
                                    <div class="shop-table-wrap">
                                        <h3 class="heading-border">Order Summary</h3>
                                        <table class="shop-cart">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Product</th>
                                                    <th>Media Type</th>
                                                    <th>Quantity</th>
                                                    <th>Gift</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="cart-img"><img src="images/shop/product-sample.jpg" /></td>
                                                    <td class="cart-product">
                                                        <div class="cart-title yellow">Sin, Sex &amp; Self-Control</div>
                                                        <div class="cart-author">Pastor John Hagee</div>
                                                    </td>
                                                    <td class="cart-media">
                                                        <div class="cart-media-type">Paperback</div>
                                                        <div class="cart-media-num">Item Number: 8194</div>
                                                    </td>
                                                    <td class="cart-quantity">
                                                        <input type="text" size="4" value="1" />
                                                    </td>
                                                    <td class="cart-gift">
                                                        <input type="checkbox" />
                                                    </td>
                                                    <td class="cart-total">$12.99</td>
                                                </tr>
                                                <tr>
                                                    <td class="cart-img"><img src="images/shop/product-sample.jpg" /></td>
                                                    <td class="cart-product">
                                                        <div class="cart-title yellow">Sin, Sex &amp; Self-Control</div>
                                                        <div class="cart-author">Pastor John Hagee</div>
                                                    </td>
                                                    <td class="cart-media">
                                                        <div class="cart-media-type">Paperback</div>
                                                        <div class="cart-media-num">Item Number: 8194</div>
                                                    </td>
                                                    <td class="cart-quantity">
                                                        <input type="text" size="4" value="1" />
                                                    </td>
                                                    <td class="cart-gift">
                                                        <input type="checkbox" />
                                                    </td>
                                                    <td class="cart-total">$12.99</td>
                                                </tr>
                                                <tr>
                                                    <td class="cart-img"><img src="images/shop/product-sample.jpg" /></td>
                                                    <td class="cart-product">
                                                        <div class="cart-title yellow">Sin, Sex &amp; Self-Control</div>
                                                        <div class="cart-author">Pastor John Hagee</div>
                                                    </td>
                                                    <td class="cart-media">
                                                        <div class="cart-media-type">Paperback</div>
                                                        <div class="cart-media-num">Item Number: 8194</div>
                                                    </td>
                                                    <td class="cart-quantity">
                                                        <input type="text" size="4" value="1" />
                                                    </td>
                                                    <td class="cart-gift">
                                                        <input type="checkbox" />
                                                    </td>
                                                    <td class="cart-total">$12.99</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="cart-shipping-summary shop-table-wrap">
                                        <h3 class="heading-border">Shipping Summary</h3>
                                        <table class="shop-cart">
                                            <thead>
                                                <tr>
                                                    <th>Type</th>
                                                    <th>Location</th>
                                                    <th>Shipping Method</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><h4>Primary</h4></td>
                                                    <td>John Smith<br />15305 Dallas Parkway<br />Addison, TX 75001</td>
                                                    <td><b>Standard</b><br />$7.99</td>
                                                </tr>
                                                <tr>
                                                    <td><h4>Gift</h4></td>
                                                    <td>John Smith<br />15305 Dallas Parkway<br />Addison, TX 75001</td>
                                                    <td><b>Standard</b><br />$7.99</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="cart-payment-summary">
                                        <h3 class="heading-border">Payment Summary</h3>
                                        <table class="payment-method-table">
                                            <thead>
                                                <tr>
                                                    <th>Payment Method</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <p>Visa card ending in <b>3142</b></p>
                                                        <h5>Billing Address</h5>
                                                        <div>John Smith<br />15305 Dallas Parkway<br />Addison, TX 75001</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="cart-summary-table">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Cost Details</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>SUBTOTAL:</td>
                                                    <td>$25.98</td>
                                                </tr>
                                                <tr>
                                                    <td>COUPON:</td>
                                                    <td>$0.00</td>
                                                </tr>
                                                <tr>
                                                    <td>ONE TIME DONATION:</td>
                                                    <td>$0.00</td>
                                                </tr>
                                                <tr>
                                                    <td>SHIPPING:</td>
                                                    <td>$7.99</td>
                                                </tr>
                                                <tr class="cart-total-row">
                                                    <td>TOTAL:</td>
                                                    <td>$33.97</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <a style="clear:right" class="fright button button-3d button-large" href="shop-checkout.php"><span>Confirm Order</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>