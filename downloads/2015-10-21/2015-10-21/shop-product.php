<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
                <link rel="stylesheet" href="css/shop.css" type="text/css" />
                <script type="text/javascript" src="js/shop.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

                    <!-- Top Links 
                    ============================================= -->
                    <div class="top-links">
                        <ul>
                            <li><a href="#"> <i class="icon-line2-user"></i>Login</a>
                                <div class="top-link-section">
                                    <form id="top-login" role="form">
                                        <div class="input-group" id="top-login-username">
                                            <span class="input-group-addon"><i class="icon-mail"></i></span>
                                            <input type="email" class="form-control" placeholder="Email address" required="">
                                        </div>
                                        <div class="input-group" id="top-login-password">
                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <label class="checkbox">
                                          <input type="checkbox" value="remember-me"> Remember me
                                        </label>
                                        <button class="btn btn-danger btn-block" type="submit">Sign in</button>
                                    </form>
                                </div>
                            </li>
							<li><a href="#"><i class="icon-key"></i> Register</a>
								<div class="top-link-section">
                                    <form id="top-register" role="form">
                                        <div class="input-group" id="top-register-name">
                                            <span class="input-group-addon"><i class="icon-line2-user"></i></span>
                                            <input type="name" class="form-control" placeholder="Name" required="">
                                        </div>
										<div class="input-group" id="top-register-username">
                                            <span class="input-group-addon"><i class="icon-mail"></i></span>
                                            <input type="email" class="form-control" placeholder="Email address" required="">
                                        </div>
                                        <div class="input-group" id="top-register-password">
                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <button class="btn btn-danger btn-block" type="submit">Register</button>
                                    </form>
                                </div>
							</li>
                        </ul>
                    </div> <!--.top-links end -->

                </div>

            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Slider
		============================================= -->
        <?php include "include/slider.php";?>


        <!-- Content
        ============================================= -->
        <section id="content">
            <div id="shop-nav">
                <ul>
                    <li class="active"><a class="tab" rel="#" href="#">Featured</a></li>
                    <li><a class="tab" rel="#" href="#">Categories</a></li>
                    <li><a class="tab" rel="#" href="#">Authors</a></li>
                    <li><a class="tab" rel="#" href="#">Subject</a></li>
                </ul>
            </div>
            <div class="noborder nomargin product-detail nopadding topmargin-sm bottommargin-sm">
                <div class="container simple-clear">
                    <div class="shop-category-header">
                        <h4><a href="#">< Bibles &amp; Devotionals</a></h4>
                        <div class="shop-item-sort">
                            <a href="#">Home</a> / <a href="#">Shop</a> / <a href="#">Bibles &amp; Devotionals</a> / Sin, Sex, &amp; Self-Control
                        </div>
                    </div>
                    <div id="product-nav">
                        Choose your media type:
                        <ul>
                            <li class="active"><a href="#">CD - $39.99</a></li>
                            <li><a href="#">DVD - $49.99</a></li>
                            <li><a href="#">MP3 - $29.99</a></li>
                        </ul>
                    </div>
                </div>
                <div class="container simple-clear border-full bottompadding-sm toppadding-sm bottommargin-sm">
                    <div class="product-img">
                        <img src="images/shop/product-sample.jpg" />
                    </div>
                    <div class="product-info">
                        <h1 class="yellow">Sin, Sex &amp; Self-Control</h1>
                        <div class="fleft">Pastor John Hagee</div>
                        <div class="fright">Item number: 8194</div>
                        <div class="product-headline simple-clear">Series Description</div>
                        <p class="product-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent suscipit fermentum ultrices. Proin consequat est eu purus mollis ultrices. Suspendisse faucibus, leo a feugiat commodo, diam nisl imperdiet nisi, a mollis lorem enim id ipsum. Integer malesuada lacinia ante, volutpat aliquet turpis sodales eget. Cras et quam ligula. Mauris condimentum ultricies est, vitae molestie enim eleifend sit amet. Sed at vehicula turpis, a pellentesque lacus. Morbi quis massa felis. Vestibulum fringilla quam sit amet nisl imperdiet, ut maximus dolor ultricies.</p>
                        <div class="fleft">
                            <b>Complete Series - CD</b>
                            <div class="product-block product-price">$39.99</div>
                            <div class="product-block product-status product-status-in-stock">In Stock</div>
                        </div>
                        <div class="product-action fright">
                            <div>Qty: <input type="text" size="4" value="1" /></div>
                            <input type="submit" class="button button-blue" value="Add to Cart" />
                        </div>
                        <div class="product-headline product-ind-headline simple-clear">
                            Order Individual Part
                        </div>
                        <div class="slide-wrapper">
                            <div class="slide-group">
                                <div class="slide-title">
                                    <h3>Part 1: The Deception of Sin</h3>
                                </div>
                                <div class="slide-content">
                                    <h2 class="yellow">Sin, Sex &amp; Self-Control</h2>
                                    <div class="fleft">Pastor John Hagee</div>
                                    <div class="fright">Item number: 8194</div>
                                    <div class="product-separator simple-clear"></div>
                                    <p class="product-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent suscipit fermentum ultrices. Proin consequat est eu purus mollis ultrices. Suspendisse faucibus, leo a feugiat commodo, diam nisl imperdiet nisi, a mollis lorem enim id ipsum. Integer malesuada lacinia ante, volutpat aliquet turpis sodales eget. Cras et quam ligula. Mauris condimentum ultricies est, vitae molestie enim eleifend sit amet. Sed at vehicula turpis, a pellentesque lacus. Morbi quis massa felis. Vestibulum fringilla quam sit amet nisl imperdiet, ut maximus dolor ultricies.</p>
                                    <div class="fleft">
                                        <b>Part 1 - CD</b>
                                        <div class="product-block product-price">$7.99</div>
                                        <div class="product-block product-status product-status-in-stock">In Stock</div>
                                    </div>
                                    <div class="product-action fright">
                                        <div>Qty: <input type="text" size="4" value="1" /></div>
                                        <input type="submit" class="button button-blue" value="Add to Cart" />
                                    </div>
                                </div>
                            </div>
                            <div class="slide-group">
                                <div class="slide-title">
                                    <h3>Part 2: The Deception of Sin</h3>
                                </div>
                                <div class="slide-content">
                                    <h2 class="yellow">Sin, Sex &amp; Self-Control</h2>
                                    <div class="fleft">Pastor John Hagee</div>
                                    <div class="fright">Item number: 8194</div>
                                    <div class="product-separator simple-clear"></div>
                                    <p class="product-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent suscipit fermentum ultrices. Proin consequat est eu purus mollis ultrices. Suspendisse faucibus, leo a feugiat commodo, diam nisl imperdiet nisi, a mollis lorem enim id ipsum. Integer malesuada lacinia ante, volutpat aliquet turpis sodales eget. Cras et quam ligula. Mauris condimentum ultricies est, vitae molestie enim eleifend sit amet. Sed at vehicula turpis, a pellentesque lacus. Morbi quis massa felis. Vestibulum fringilla quam sit amet nisl imperdiet, ut maximus dolor ultricies.</p>
                                    <div class="fleft">
                                        <b>Part 1 - CD</b>
                                        <div class="product-block product-price">$7.99</div>
                                        <div class="product-block product-status product-status-in-stock">In Stock</div>
                                    </div>
                                    <div class="product-action fright">
                                        <div>Qty: <input type="text" size="4" value="1" /></div>
                                        <input type="submit" class="button button-blue" value="Add to Cart" />
                                    </div>
                                </div>
                            </div>
                            <div class="slide-group">
                                <div class="slide-title">
                                    <h3>Part 3: The Deception of Sin</h3>
                                </div>
                                <div class="slide-content">
                                    <h2 class="yellow">Sin, Sex &amp; Self-Control</h2>
                                    <div class="fleft">Pastor John Hagee</div>
                                    <div class="fright">Item number: 8194</div>
                                    <div class="product-separator simple-clear"></div>
                                    <p class="product-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent suscipit fermentum ultrices. Proin consequat est eu purus mollis ultrices. Suspendisse faucibus, leo a feugiat commodo, diam nisl imperdiet nisi, a mollis lorem enim id ipsum. Integer malesuada lacinia ante, volutpat aliquet turpis sodales eget. Cras et quam ligula. Mauris condimentum ultricies est, vitae molestie enim eleifend sit amet. Sed at vehicula turpis, a pellentesque lacus. Morbi quis massa felis. Vestibulum fringilla quam sit amet nisl imperdiet, ut maximus dolor ultricies.</p>
                                    <div class="fleft">
                                        <b>Part 1 - CD</b>
                                        <div class="product-block product-price">$7.99</div>
                                        <div class="product-block product-status product-status-in-stock">In Stock</div>
                                    </div>
                                    <div class="product-action fright">
                                        <div>Qty: <input type="text" size="4" value="1" /></div>
                                        <input type="submit" class="button button-blue" value="Add to Cart" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container simple-clear">
                    <h2 class="heading-border heading-yellow-border">Related Items</h2>
                    <div class="shop-item col5">
                        <img src="images/shop/product-sample.jpg" />
                        <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                        <a href="#" class="view-item">View Item</a>
                    </div>
                    <div class="shop-item col5">
                        <img src="images/shop/product-sample.jpg" />
                        <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                        <a href="#" class="view-item">View Item</a>
                    </div>
                    <div class="shop-item col5">
                        <img src="images/shop/product-sample.jpg" />
                        <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                        <a href="#" class="view-item">View Item</a>
                    </div>
                    <div class="shop-item col5">
                        <img src="images/shop/product-sample.jpg" />
                        <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                        <a href="#" class="view-item">View Item</a>
                    </div>
                    <div class="shop-item col5">
                        <img src="images/shop/product-sample.jpg" />
                        <h4>Sin, Sex &amp; Self-Control<span class="sub-header">By John Hagee</span></h4>
                        <a href="#" class="view-item">View Item</a>
                    </div>
                </div>
            </div>
        </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>