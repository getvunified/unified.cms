<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
		<link rel="stylesheet" href="css/shop.css" type="text/css" />
        <script type="text/javascript" src="js/shop.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Support Israel</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-supportisrael.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="giving-opportunities.php">Give</a></li>
                    <li class="active">Support Israel</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="section noborder nomargin nopadding">
	 
                <div class="container clearfix">
                 	
<h3>Exodus II</h3>
<p>World history is literally forming before our very eyes. It is clear that Israel is in the gravest danger that she has been in since the six Arab armies tried to strangle her at birth in 1948. We want the world and the Jewish people to know that millions of evangelical Christians in America and around the world have a biblical belief to love Israel, to speak up and support her not only with prayer, but financially as well. At time when Israel feels more alone than ever, I want us to let them know “YOU ARE NOT ALONE!” You are God’s Chosen people and have an eternal covenant with God that will stand forever.</p>

<p>Friends and partners, I am awed at what you have done the past and what I know you will continue to do in the future with your generous donations through our Exodus II programs with John Hagee Ministries. To date, you have given $48,127,240.60 to various causes that support the Jewish people. Now that is an Awesome God! Your gifts go for many causes, such as education, helping orphans, the end gathering of exiles from around the world who wish to relocate to Israel and many other humanitarian causes. ALL of our efforts directly benefit Israel and her people. You are the reason we have made such a difference in the lives of so many of God’s people.</p>

<p>As Christians, it is our duty to minister to the Jewish people in material things. “It pleased them indeed, and they are their debtors. For if the Gentiles have been partakers of their spiritual things, their duty is also to minister to them in material things,” Romans 15:27. If you want God’s favor then I challenge you to make the finest contribution you can to Exodus II. I ask that you join me in praying daily for the peace of Jerusalem, and ask that you let your voice heard, “For Zion’s sake I will not keep silent, for Jerusalem’s sake I will not remain quiet, till her righteousness shines out like the dawn, her salvation like a blazing torch.” - Isaiah 62:1. Together – let’s bless Israel and the Jewish people!</p>

<h3>Plant your roots in Israel</h3>
<p>This campaign has the goal of planting trees in Israel to help revitalize the land! Planting a tree is more than mere aesthetics. It is an opportunity to help: Transform barren regions of Israel into fruitful, productive land, create jobs, provide needed irrigation and sustain the local economy.</p>

<p>Each donation of $35 plants a tree in Israel and contributes to a unique water conservation project. With your donation you will receive a certificate to commemorate your participation. You can also plant a tree in memory of a loved one.
You can plant as many trees as you would like! JHM partners have planted tens of thousands of trees with the Land of Promise oversight. We thank you for your support of the land of Israel.</p>


				</div>
			</div>

            <div class="clear"></div>
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>