<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
                <?php include "func.php"; ?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Daily Truth</title>
        <script type="text/javascript" src="js/sticky.js"></script>
	<script type="text/javascript">
            jQuery(document).ready(function() {
			jQuery(document).on("click", '.load-cal', function(e) {
				e.preventDefault();
				jQuery('#gl-cal').load(jQuery(this).attr('href'));
			});
                        //show filter on click
                jQuery('.filter-button').click(function(e) {
                    e.preventDefault();
                    if(jQuery(this).hasClass('closed')) {
                        jQuery('.filter-dropdown').hide();
                        jQuery(this.rel).slideDown(200);
                        jQuery(this).toggleClass('closed open');
                    } else {
                        jQuery(this.rel).slideUp(200);
                        jQuery(this).toggleClass('closed open');
                    }
                });
		});
	</script>
</head>

<body class="stretched side-push-panel daily-truth">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>

  		
       
       
		<!-- Page Header
		============================================= -->
        <?php include "include/pageheader-dailytruth.php";?>

		


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">
            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">Encouragement</a></li>
                    <li class="active blue-text">Daily #Truth</li>
                </ol>
            </div>
        </section><!-- #page-title end -->

		
		<!-- Selections
        ============================================= -->
		<section id="content">
            <div class="container clearfix">

            </div>
        <!-- selections end -->

		
		<!-- Content
		============================================= -->
		
		
            <div class="section noborder notopmargin nobottommargin toppadding">
                <div data-animate="fadeIn" class="container clearfix border-full bottommargin">
                    <div class="entry-content">
                        <div class="entry-title topmargin-xsm">
                            <div class="col_half nobottommargin col_respon">
                            	<h3 class="daily"><a href="daily-truth-single-day.php">September 25th, 2015</a></h3>
							</div>
                            
							<div class="col_half fright col_last nobottommargin col_respon">
                                <div class="si-share">
									<div>
									<span>Share:</span>
                                    <a href="#" class="social-icon si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Pinterst">
                                        <i class="icon-pinterest"></i>
                                        <i class="icon-pinterest"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-vimeo" data-toggle="tooltip" data-placement="top" title="Vimeo">
                                        <i class="icon-vimeo"></i>
                                        <i class="icon-vimeo"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="Instagram">
                                        <i class="icon-instagram"></i>
                                        <i class="icon-instagram"></i>
                                    </a>
                                	</div>
                            	</div>
							</div>
						</div>
					</div>

					<div class="clear"></div>

                    <!-- Daily Truths
                    ============================================= -->
                    <div id="posts" class="post-grid post-masonry grid-3 clearfix">
        
                        <div class="entry clearfix nobottommargin">
							<div class="entry-image nobottommargin">
                                <div class="panel panel-default">
									<i class="i-plain icon-music2 leftmargin-xsm"></i>
                                	<h3 class="topmargin-xsm">Christian Music</h3>
                                    <div class="panel-body">
                                        <a href="daily-truth-single-day.php#anchor1"><img src="images/daily-truth-christianmusic.jpg" alt="Pastor John Hagee"></a>
                                        <h3 class="topmargin-xsm"><a class="blue-text" href="daily-truth-single-day.php#anchor1">Held</a></h3>										
                                        <h5 class="bottommargin-xsm">Natalie Grant</h5>
										Natalie Grant's official music video: "Held", from her album "Awaken".
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="entry clearfix nobottommargin">
							<div class="entry-image nobottommargin">
                                <div class="panel panel-default">
									<i class="i-plain icon-comments-alt leftmargin-xsm"></i>
                                	<h3 class="topmargin-xsm">Quote</h3>
                                    <div class="panel-body">
                                        <a href="daily-truth-single-day.php#anchor2"><img src="images/daily-truth-quotes.jpg" alt="Pastor John Hagee"></a>
                                         <h3 class="topmargin-xsm"><a class="blue-text" href="daily-truth-single-day.php#anchor2">Thrown Into The Fire!</a></h3>									
                                        <h5 class="bottommargin-xsm">Pastor John Hagee</h5>
										When you are thrown into the fire, God doesn’t send someone else. He shows up Himself!
                                    </div>
                                </div>
                            </div>
						</div>
                    	<div class="entry clearfix nobottommargin">
							<div class="entry-image nobottommargin">
                                <div class="panel panel-default">
									<i class="i-plain icon-book leftmargin-xsm"></i>
                                	<h3 class="topmargin-xsm">Bible Verse</h3>
                                    <div class="panel-body">
                                        <a href="daily-truth-single-day.php#anchor3"><img src="images/daily-truth-bibleverse.jpg" alt="Pastor John Hagee"></a>
                                        <h3 class="topmargin-xsm"><a class="blue-text" href="daily-truth-single-day.php#anchor3">Bible Verse</a></h3>										
                                        <h5 class="bottommargin-xsm">Psalm 91:15-16</h5>
										He shall call upon me, and I will answer Him; I will be with Him in trouble...
                                    </div>
                                </div>
                            </div>
						</div>
					</div><!-- Daily Truths end -->
        		</div>

				<div data-animate="fadeIn" class="container clearfix border-full bottommargin">
                    <div class="entry-content">
                        <div class="entry-title topmargin-xsm">
                            <div class="col_half nobottommargin col_respon">
                            	<h3 class="daily"><a href="daily-truth-single-day.php">September 26th, 2015</a></h3>
							</div>
							<div class="col_half fright col_last nobottommargin col_respon">
                                <div class="si-share">
									<div>
									<span>Share:</span>
                                    <a href="#" class="social-icon si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Pinterst">
                                        <i class="icon-pinterest"></i>
                                        <i class="icon-pinterest"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-vimeo" data-toggle="tooltip" data-placement="top" title="Vimeo">
                                        <i class="icon-vimeo"></i>
                                        <i class="icon-vimeo"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="Instagram">
                                        <i class="icon-instagram"></i>
                                        <i class="icon-instagram"></i>
                                    </a>
                                	</div>
                            	</div>
							</div>
						</div>
					</div>

					<div class="clear"></div>

                    <!-- Daily Truths 2
                    ============================================= -->
                    <div id="posts" class="post-grid post-masonry grid-3 clearfix">
        
                       <div class="entry clearfix nobottommargin">
							<div class="entry-image nobottommargin">
                                <div class="panel panel-default">
									<i class="i-plain icon-music2 leftmargin-xsm"></i>
                                	<h3 class="topmargin-xsm">Christian Music</h3>
                                    <div class="panel-body">
                                        <a href="daily-truth-single-day.php#anchor1"><img src="images/daily-truth-christianmusic.jpg" alt="Pastor John Hagee"></a>
                                        <h3 class="topmargin-xsm"><a class="blue-text" href="daily-truth-single-day.php#anchor1">Held</a></h3>										
                                        <h5 class="bottommargin-xsm">Natalie Grant</h5>
										Natalie Grant's official music video: "Held", from her album "Awaken".
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="entry clearfix nobottommargin">
							<div class="entry-image nobottommargin">
                                <div class="panel panel-default">
									<i class="i-plain icon-comments-alt leftmargin-xsm"></i>
                                	<h3 class="topmargin-xsm">Quote</h3>
                                    <div class="panel-body">
                                        <a href="daily-truth-single-day.php#anchor2"><img src="images/daily-truth-quotes.jpg" alt="Pastor John Hagee"></a>
                                         <h3 class="topmargin-xsm"><a class="blue-text" href="daily-truth-single-day.php#anchor2">Thrown Into The Fire!</a></h3>									
                                        <h5 class="bottommargin-xsm">Pastor John Hagee</h5>
										When you are thrown into the fire, God doesn’t send someone else. He shows up Himself!
                                    </div>
                                </div>
                            </div>
						</div>
                    	<div class="entry clearfix nobottommargin">
							<div class="entry-image nobottommargin">
                                <div class="panel panel-default">
									<i class="i-plain icon-book leftmargin-xsm"></i>
                                	<h3 class="topmargin-xsm">Bible Verse</h3>
                                    <div class="panel-body">
                                        <a href="daily-truth-single-day.php#anchor3"><img src="images/daily-truth-bibleverse.jpg" alt="Pastor John Hagee"></a>
                                        <h3 class="topmargin-xsm"><a class="blue-text" href="daily-truth-single-day.php#anchor3">Bible Verse</a></h3>										
                                        <h5 class="bottommargin-xsm">Psalm 91:15-16</h5>
										He shall call upon me, and I will answer Him; I will be with Him in trouble...
                                    </div>
                                </div>
                            </div>
						</div>
					</div><!-- Daily Truths 2 end -->
        		</div>

    		
				<div data-animate="fadeIn" class="container clearfix border-full">
                    <div class="entry-content">
                        <div class="entry-title topmargin-xsm">
                            <div class="col_half nobottommargin col_respon">
                            	<h3 class="daily"><a href="daily-truth-single-day.php">September 27th, 2015</a></h3>
							</div>
							<div class="col_half fright col_last nobottommargin col_respon">
                                <div class="si-share">
									<div>
									<span>Share:</span>
                                    <a href="#" class="social-icon si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Pinterst">
                                        <i class="icon-pinterest"></i>
                                        <i class="icon-pinterest"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-vimeo" data-toggle="tooltip" data-placement="top" title="Vimeo">
                                        <i class="icon-vimeo"></i>
                                        <i class="icon-vimeo"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="Instagram">
                                        <i class="icon-instagram"></i>
                                        <i class="icon-instagram"></i>
                                    </a>
                                	</div>
                            	</div>
							</div>
						</div>
					</div>

					<div class="clear"></div>

                    <!-- Daily Truths 2
                    ============================================= -->
                    <div id="posts" class="post-grid post-masonry grid-3 clearfix">
        
                       <div class="entry clearfix nobottommargin">
							<div class="entry-image nobottommargin">
                                <div class="panel panel-default">
									<i class="i-plain icon-music2 leftmargin-xsm"></i>
                                	<h3 class="topmargin-xsm">Christian Music</h3>
                                    <div class="panel-body">
                                        <a href="daily-truth-single-day.php#anchor1"><img src="images/daily-truth-christianmusic.jpg" alt="Pastor John Hagee"></a>
                                        <h3 class="topmargin-xsm"><a class="blue-text" href="daily-truth-single-day.php#anchor1">Held</a></h3>										
                                        <h5 class="bottommargin-xsm">Natalie Grant</h5>
										Natalie Grant's official music video: "Held", from her album "Awaken".
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="entry clearfix nobottommargin">
							<div class="entry-image nobottommargin">
                                <div class="panel panel-default">
									<i class="i-plain icon-comments-alt leftmargin-xsm"></i>
                                	<h3 class="topmargin-xsm">Quote</h3>
                                    <div class="panel-body">
                                        <a href="daily-truth-single-day.php#anchor2"><img src="images/daily-truth-quotes.jpg" alt="Pastor John Hagee"></a>
                                         <h3 class="topmargin-xsm"><a class="blue-text" href="daily-truth-single-day.php#anchor2">Thrown Into The Fire!</a></h3>									
                                        <h5 class="bottommargin-xsm">Pastor John Hagee</h5>
										When you are thrown into the fire, God doesn’t send someone else. He shows up Himself!
                                    </div>
                                </div>
                            </div>
						</div>
                    	<div class="entry clearfix nobottommargin">
							<div class="entry-image nobottommargin">
                                <div class="panel panel-default">
									<i class="i-plain icon-book leftmargin-xsm"></i>
                                	<h3 class="topmargin-xsm">Bible Verse</h3>
                                    <div class="panel-body">
                                        <a href="daily-truth-single-day.php#anchor3"><img src="images/daily-truth-bibleverse.jpg" alt="Pastor John Hagee"></a>
                                        <h3 class="topmargin-xsm"><a class="blue-text" href="daily-truth-single-day.php#anchor3">Bible Verse</a></h3>										
                                        <h5 class="bottommargin-xsm">Psalm 91:15-16</h5>
										He shall call upon me, and I will answer Him; I will be with Him in trouble...
                                    </div>
                                </div>
                            </div>
						</div>
					</div><!-- Daily Truths 2 end -->
        		</div>



</div>
		</section>
				
			<div class="clear"></div>
            

			<!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>