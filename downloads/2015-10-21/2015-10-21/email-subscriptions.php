<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Email Subscriptions</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-email-subscriptions.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">Connect</a></li>
                    <li class="active">Email Subscriptions</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
           <div class="content-wrap noborder topmargin nobottommargin nopadding">

				<div data-animate="fadeIn" class="container bottommargin clearfix">

                    
					<!-- Email Signup Form
					============================================= -->
					<div class="nobottommargin">

                                            <p>Please select the content that you would like to receive and then enter your email. Descriptions of content can be found below. <a href="unsubscribe.php">To unsubscribe click here</a>.</p>

						<h3><span><span>*Denotes required fields</span></span></h3>
						<div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>

						<form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">

							<div class="form-process"></div>

                                <div class="col_one_third topmargin-xsm">
                                    
                                    <div class="fright">
                                        <ul class="list-unstyled">
                                            <li><input type="checkbox" id="template-contactform-name" name="template-contactform-name" value="" class="checkbox-inline" /> <label for="template-contactform-name">A Moment with Matt </label></li>
                                    
                                            <li><input type="checkbox" id="template-contactform-name" name="template-contactform-name" value="" class="checkbox-inline" /> <label for="template-contactform-name">Daily Blessings and Readings </label></li>
                                    
                                            <li><input type="checkbox" id="template-contactform-name" name="template-contactform-name" value="" class="checkbox-inline" /> <label for="template-contactform-name">Event Notifications </label></li>
                                    
                                            <li><input type="checkbox" id="template-contactform-name" name="template-contactform-name" value="" class="checkbox-inline" /> <label for="template-contactform-name">Weekly Update </label></li>
                                        </ul>
                                    </div>
                               	</div>
							


							<div class="col_two_third col_last">
								<label for="template-contactform-name">Full Name <small>*</small></label>
								<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />
							<div class="topmargin-xsm">
								<label for="template-contactform-email">Email <small>*</small></label>
								<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
							</div>
								<button class="button button-3d topmargin-sm" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Subscribe</button>
							</div>


							<div class="clear"></div>
						
						</form>

						<script type="text/javascript">

							$("#template-contactform").validate({
								submitHandler: function(form) {
									$('.form-process').fadeIn();
									$(form).ajaxSubmit({
										target: '#contact-form-result',
										success: function() {
											$('.form-process').fadeOut();
											$(form).find('.sm-form-control').val('');
											$('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
											SEMICOLON.widget.notifications($('#contact-form-result'));
										}
									});
								}
							});

						</script>

					</div><!-- .email signup form end -->
				</div>

			</div>

    	</section>

        <div class="clear"></div>  
 		
		<section id="content">
           <div class="section noborder notopmargin nobottommargin nopadding">
                <div class="container clearfix">
					
					<div class="heading-block noborder topmargin bottommargin center">
						<h2>JHM Email Subscriptions</h2>
						<p>Use the form above to subscribe to the content displayed below.</p>
					</div>

                    <!-- Posts
                    ============================================= -->
                    <div id="posts" class="post-grid grid-2 clearfix">

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                               <img src="images/emailsub-blog.jpg" alt="A Moment With Matt">
                            </div>
                            <div class="entry-title">
                                <h2>A Moment With Matt</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold"><i class="icon-calendar3"></i> Once a Week</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>A weekly blog from Pastor Matthew that gives insight into his daily life and has spiritual applications and inspiration for readers.</p>
                            </div>
                        </div>

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                               <img src="images/emailsub-dailyblessings.jpg" alt="Daily Blessings">
                            </div>
                            <div class="entry-title">
                                <h2>Daily Blessings and Readings</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold"><i class="icon-calendar3"></i> Everyday</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>Pastors John and Matthew Hagee speak blessings over the congregants at Cornerstone Church and those watching through GETV.org every Sunday. Receive those blessings into your own life in addition to an NKJV daily scripture reading that enables you to read through the entire Bible in one year.</p>
                            </div>
                        </div>

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                               <img src="images/emailsub-weeklyupdate.jpg" alt="Weekly Update">
                            </div>
                            <div class="entry-title">
                                <h2>Weekly Update</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold"><i class="icon-calendar3"></i> Once a Week</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>Receive a weekly update directly from the desk of Pastor John or Pastor Matthew Hagee. Topics range from insight into current events to happenings at John Hagee Ministries.</p>
                            </div>
                        </div>

						<div class="entry clearfix border-full bottommargin pad15 toppadding-sm">
                            <div class="entry-image">
                                <img src="images/emailsub-eventnotification.jpg" alt="Event Notofication">
                            </div>
                            <div class="entry-title">
                                <h2>Event Notification</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li class="tcolorgold"><i class="icon-calendar3"></i> Day event is posted</li>
                            </ul>
							<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="entry-content topmargin-xsm">
                               <p>Be informed of John Hagee Ministries’ events that happen in San Antonio and around the world. We would love for you to come visit us in San Antonio or join us when we travel to your area!</p>
                            </div>
                        </div>
					
					
					

					</div>

				</div>
			</div>
		</section><!-- #content end -->
			

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>