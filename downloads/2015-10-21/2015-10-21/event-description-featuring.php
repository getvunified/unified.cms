<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="js/jquery.gmap.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Event Description - Featuring</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       

		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">Events</a></li>
                    <li class="active">Event Description</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="section noborder notopmargin break-early nobottommargin nobottompadding">
	 
                <div class="container clearfix">
				<a href="events.php" class="button button-3d bottommargin-sm">Events</a>
                  
				<!-- Post Content
                ============================================= -->
       		
                    <div data-animate="fadeIn" class="team team-list clearfix">
                        <div class="team-image">
                            <img src="images/event1.jpg" alt="Canton Junction">
                        </div>
                        <div class="team-desc">
                            <div class="col_two_third nobottommargin col_respon">
                                <div class="team-title"><h3 class="nobottommargin name">Canton Junction's Every Hallelujah Concert</h3><span>September 24, 2015</span></div>
                            </div>
                            <div class="col_one_third col_last nobottommargin col_respon fright">
                                <a href="#" class="social-icon si-small si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-small si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-small si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Pinterst">
                                    <i class="icon-pinterest"></i>
                                    <i class="icon-pinterest"></i>
                                </a>
                                <a href="#" class="social-icon si-small si-borderless si-vimeo" data-toggle="tooltip" data-placement="top" title="Vimeo">
                                    <i class="icon-vimeo"></i>
                                    <i class="icon-vimeo"></i>
                                </a>
                                <a href="#" class="social-icon si-small si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="Instagram">
                                    <i class="icon-instagram"></i>
                                    <i class="icon-instagram"></i>
                                </a>
                            </div>

                            <div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="team-content">
                                <p><strong>Canton Junction's Every Hallelujah Concert Tour with Special Guest Jason Crabb!</strong> Join us for the Tailgate PARTY and Meet & Greet at 5:00PM in the parking lot. Meet Canton Junction &  Jason Crabb before the concert. Invite your friends, come ready for fun, fellowship and awesome GIVEAWAYS! Since their official debut in 2012, the Texas-based quartet has thrilled audiences across the U.S., Canada, Scotland and Israel with their inspiring message and high-energy performances. Featuring powerful new songs alongside well-loved classics, Every Hallelujah is a perfect reflection of both the talent and heart of Canton Junction. </p>
                               
                            </div>
                        </div>
                    </div>
                	<div class="line topmargin-xsm bottommargin-sm"></div>
				
				</div>

				<div data-animate="fadeIn" class="container clearfix">
                  <!-- Post Content
                            ============================================= -->
       		
					<div class="col_one_fourth">
						<div class="feature-box fbox-center fbox-plain noborder">
							<div class="fbox-icon">
								<i class="icon-line-paper tcolorgold"></i>
							</div>
							<h3>Details</h3>
                            <div class="text-left leftmargin topmargin-sm">
								<ul class="iconlist nobottommargin">
                                    <li><i class="icon-calendar3"></i> <strong>Date:</strong> September 24, 2015</li>
                                    <li><i class="icon-time"></i> <strong>Start Time:</strong> 8:00am</li>
 									<li><i class="icon-time"></i> <strong>End Time:</strong> 5:00pm</li>
                                    <li><strong>Children are welcome</strong> </li>
                                </ul>
							</div>
						</div>
					</div>
					<div class="col_one_fourth">
						<div class="feature-box fbox-center fbox-plain noborder">
							<div class="fbox-icon">
								<i class="icon-line-flag tcolorgold"></i>
							</div>
							<h3>Venue</h3>
                            <div class="text-left leftmargin topmargin-sm">
								<ul class="iconlist nobottommargin">
                                    <li><i class="icon-line-location"></i> <strong>Place:</strong> Evangel Temple Assembly of God</li>
                                    <li><i class="icon-map-marker2"></i> <strong>Address:</strong> 5755 Ramona Blvd., Jacksonville, FL 32205</li>
 									<li><i class="icon-phone3"></i> <strong>Phone:</strong> (904) 781-9393</li>
                                    <li><i class="icon-mail"></i> <strong>Email:</strong> <a href="mailto:info@evangeltemple.com">info@EvangelTemple.com</a></li>
                                </ul>
							</div>
						</div>
					</div>
                    <div class="col_one_fourth">
						<div class="feature-box fbox-center fbox-plain noborder">
							<div class="fbox-icon">
								<i class="icon-line2-info tcolorgold"></i>
							</div>
							<h3>Organizer</h3>
                            <div class="text-left leftmargin topmargin-sm">
								<ul class="iconlist nobottommargin">
                                    <li><i class="icon-line-location"></i> <strong>Place:</strong> Evangel Temple Assembly of God</li>
                                    <li><i class="icon-phone3"></i> <strong>Phone:</strong> (904) 781-9393</li>
 									<li><i class="icon-mail"></i> <strong>Email:</strong> <a href="mailto:info@evangeltemple.com">info@EvangelTemple.com</a></li>
                                </ul>
							</div>
						</div>
					</div>
					<div class="col_one_fourth col_last">
						<div class="feature-box fbox-center fbox-plain noborder">
							<div class="fbox-icon">
								<i class="icon-ticket tcolorgold"></i>
							</div>
							<h3>Admission</h3>
                            <div class="text-left leftmargin topmargin-sm">
								<ul class="iconlist nobottommargin">
                                    <li><i class="icon-dollar"></i> <strong>Cost:</strong> FREE</li>
                                    <li><i class="icon-ticket"></i> <strong>Ticket Registration:</strong> <a href="#">buyticketswithus.com</a></li>
 									<li><i class="icon-phone3"></i> <strong>Register by Phone:</strong> (210) 490-1600</li>
                                </ul>
							</div>
						</div>
					</div>
					<div class="line topmargin-xsm bottommargin-sm"></div>
				</div>
				
			
                <div data-animate="fadeIn" class="container featured-speakers clearfix">
                    <div class="heading-block noborder bottommargin-sm">
                        <h2 class="center">Featuring</h2>
                    </div>
					<div class="col_one_fourth bottommargin-sm">
                        <div class="team">
                            <div class="team-image">
                                <img src="images/events-featuring-img1.png" alt="John Hagee">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>John Hagee</h4><span>Senior Pastor</span></div>
                                <p>The founder and Senior Pastor of Cornerstone Church in San Antonio, Texas, a non-denominational evangelical church with more than 20,000 active members.</p>
                                <a href="#" class="button button-border bottommargin-sm">Website</a>
                            </div>
                        </div>
                    </div>

					<div class="col_one_fourth bottommargin-sm">
                        <div class="team">
                            <div class="team-image">
                                <img src="images/events-featuring-img2.png" alt="John Ortberg">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>John Ortberg</h4><span>Menlo Park Presbyterian</span></div>
                                <p>An evangelical Christian author, speaker, and senior pastor of Menlo Park Presbyterian Church in Menlo Park, California, an evangelical church with more than 4,000 members.</p>
                                <a href="#" class="button button-border bottommargin-sm">Website</a>
                            </div>
                        </div>
                    </div>
					
					<div class="col_one_fourth bottommargin-sm">
                        <div class="team">
                            <div class="team-image">
                                <img src="images/events-featuring-img3.png" alt="Joni Eareckson Tada">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>Joni Eareckson Tada</h4><span>Joni & Friends</span></div>
                                <p>An evangelical Christian, author, radio host, and founder of Joni and Friends, an organization "accelerating Christian ministry in the disability community.</p>
                                <a href="#" class="button button-border bottommargin-sm">Website</a>
                            </div>
                        </div>
                    </div>

					<div class="col_one_fourth col_last bottommargin-sm">
                        <div class="team">
                            <div class="team-image">
                                <img src="images/events-featuring-img4.png" alt="John Townsend">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>John Townsend</h4><span>Townsend Institute</span></div>
                                <p>An American Christian self help author. Townsend co-authored Boundaries: When to Say Yes, How to Say No to Take Control of Your Life in 1992 which sold two million copies.</p>
                                <a href="#" class="button button-border bottommargin-sm">Website</a>
                            </div>
                        </div>
                    </div>
                   <div class="line topmargin-xsm bottommargin-sm"></div>
                </div>

			<div data-animate="fadeIn" class="container clearfix bottommargin-sm">

				<!-- Google Map
				============================================= -->
				<div id="google-map" class="gmap" style="margin-bottom: 20px;"></div>

				<script type="text/javascript">

					$('#google-map').gMap({

						address: '5755 Ramona Blvd., Jacksonville, FL 32205',
						maptype: 'ROADMAP',
						zoom: 16,

						markers: [
								{
									latitude: 30.314697,
									longitude: -81.7468279,
								},
								
								{
									address: "5755 Ramona Blvd., Jacksonville, FL 32205",
									icon: {
											image: "images/gmap_pin_grey.png",
											html: "Canton Junction's Every Hallelujah Concert",
											iconsize: [26, 46],
											iconanchor: [12,46]
										}
								}
						],

						doubleclickzoom: true,
						controls: {
							panControl: true,
							zoomControl: true,
							mapTypeControl: true,
							scaleControl: true,
							streetViewControl: true,
							overviewMapControl: true
						}

					});

				</script><!-- Google Map End -->

				<div class="clear"></div>



</div>

          
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>