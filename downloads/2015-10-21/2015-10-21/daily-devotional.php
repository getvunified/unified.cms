<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Daily Devotional</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-dailydevotional.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">Encouragement</a></li>
                    <li class="active">Daily Devotional</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="content-wrap noborder notopmargin nobottommargin nobottompadding">
	 
                <div class="container clearfix bottommargin">

					<nav class="navbar navbar-default" role="navigation">
						  <div class="container-fluid">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
							  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							  </button>
							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							  <ul class="nav navbar-nav nobottommargin">
								<li class="dropdown">
								  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Search Using <b class="caret"></b></a>
								  <ul class="dropdown-menu" role="menu">
									<li><a href="#">All Words</a></li>
									<li><a href="#">Any Word</a></li>
									<li><a href="#">Exact Phrase</a></li>
								  </ul>
								</li>
							  </ul>
							  <form class="navbar-form navbar-left nobottommargin" role="search">
								<div class="form-group">
								  <input type="text" class="form-control" placeholder="Search">
								</div>
								<button type="submit" class="button button-3d button-small">Search</button>
							  </form>
							  
							</div><!-- /.navbar-collapse -->
						  </div><!-- /.container-fluid -->
						</nav>

                    <!-- Posts
                    ============================================= -->
                    <div id="posts" class="post-grid post-masonry post-timeline grid-2 clearfix">

                        <div class="timeline-border"></div>

                        <div class="entry entry-date-section notopmargin"><span>October 2015</span></div>

                        <div class="entry clearfix">
                            <div class="entry-timeline">
                                <div class="timeline-divider"></div>
                            </div>
                            
                            <div class="entry-title">
                                <h2 class="tcolorgold">Philippians 3:13 - October 2, 2015</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 10/2/2015</li>
                            </ul>
                            <div class="entry-content">
                                <p>But Jesus said to him, "No one, having put his hand to the plow, and looking back, is fit for the kingdom of God."...</p>
                                
                                <!-- Philippians 3:13 - October 2, 2015 -->
                                <button class="button button-3d button-small" data-toggle="modal" data-target=".oct22015">Read More</button>
        
                                <div class="modal fade oct22015" tabindex="-1" role="dialog" aria-labelledby="oct22015" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-body">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title heading tcolorgold" id="myModalLabel">Philippians 3:13 - October 2, 2015</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p><strong>Brethren, I do not count myself to have apprehended; but one thing I do, forgetting those things which are behind and reaching forward to those things which are ahead.</strong></p>
                                                    <p>Forget The Good And Forget The Bad, Because You Can’t Go Back Again
        Part Two -</p>
                                                    <p><strong>The story of Israel and their Exodus from Egypt is a classic study of human nature and how people respond to change.  Look very closely and you will find yourself!</strong></p>
        
                                                    <p><strong>That night all the people of the community raised their voices and wept aloud.</strong> All the Israelites grumbled against Moses and Aaron, and the whole assemble said to them, “If only we had died in Egypt! Or in this desert! Why is the Lord bringing us to this land only to let us fall by the sword? Our wives and children will be taken as plunder. Wouldn’t it be better for us to go back to Egypt? And they said to each other, “We should choose a leader and go back to Egypt” (Numbers 14:1-4). </p>  
                                                    
                                                    <p><strong>The Exodus is a classic story of how people respond to change.</strong> The children of Israel were slaves in Egypt. They had been beaten with whips like animals. They had been abused, starved and worked beyond endurance. They were living in shacks, dressed in rags. The Egyptians had taken their male babies and drowned them in the Nile River. Do you see a need for change here? The children of Israel prayed one prayer for 400 years. “Lord God of Abraham, Isaac and Jacob, send us a deliverer. Set us free from Pharaoh.” There was a great need for change, and a great desire for a change. But with change comes challenge. And challenge comes the ability to overcome in Christ Jesus: </p> 
                                                    
                                                    <p><strong>“For whatsoever is born of God overcometh the world; and this is the victory that overcometh the world, even our faith. Who is he that overcometh the world, but he that believeth that Jesus is the Son of God?</strong></p>
                                                    
                                                    <p class="nobottommargin tcolorgold"><strong>Source: 12 Sunday Mornings - Volume 2</strong></p>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
                        </div>

						<div class="entry clearfix">
                            <div class="entry-timeline">
                                <div class="timeline-divider"></div>
                            </div>
                            
                            <div class="entry-title">
                                <h2 class="tcolorgold">Philippians 3:13 - October 1, 2015</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 10/1/2015</li>
                            </ul>
                            <div class="entry-content">
                                <p>But Jesus said to him, "No one, having put his hand to the plow, and looking back, is fit for the kingdom of God."...</p>
                                
                                <!-- Philippians 3:13 - October 1, 2015 -->
                                <button class="button button-3d button-small" data-toggle="modal" data-target=".oct12015">Read More</button>
        
                                <div class="modal fade oct12015" tabindex="-1" role="dialog" aria-labelledby="oct12015" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-body">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title heading tcolorgold" id="myModalLabel">Philippians 3:13 - October 1, 2015</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p><strong>Brethren, I do not count myself to have apprehended; but one thing I do, forgetting those things which are behind and reaching forward to those things which are ahead.</strong></p>
                                                    <p>Forget The Good And Forget The Bad, Because You Can’t Go Back Again
        Part Two -</p>
                                                    <p><strong>The story of Israel and their Exodus from Egypt is a classic study of human nature and how people respond to change.  Look very closely and you will find yourself!</strong></p>
        
                                                    <p><strong>That night all the people of the community raised their voices and wept aloud.</strong> All the Israelites grumbled against Moses and Aaron, and the whole assemble said to them, “If only we had died in Egypt! Or in this desert! Why is the Lord bringing us to this land only to let us fall by the sword? Our wives and children will be taken as plunder. Wouldn’t it be better for us to go back to Egypt? And they said to each other, “We should choose a leader and go back to Egypt” (Numbers 14:1-4). </p>  
                                                    
                                                    <p><strong>The Exodus is a classic story of how people respond to change.</strong> The children of Israel were slaves in Egypt. They had been beaten with whips like animals. They had been abused, starved and worked beyond endurance. They were living in shacks, dressed in rags. The Egyptians had taken their male babies and drowned them in the Nile River. Do you see a need for change here? The children of Israel prayed one prayer for 400 years. “Lord God of Abraham, Isaac and Jacob, send us a deliverer. Set us free from Pharaoh.” There was a great need for change, and a great desire for a change. But with change comes challenge. And challenge comes the ability to overcome in Christ Jesus: </p> 
                                                    
                                                    <p><strong>“For whatsoever is born of God overcometh the world; and this is the victory that overcometh the world, even our faith. Who is he that overcometh the world, but he that believeth that Jesus is the Son of God?</strong></p>
                                                    
                                                    <p class="nobottommargin tcolorgold"><strong>Source: 12 Sunday Mornings - Volume 2</strong></p>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
                        </div>

						
					<!-- September 2015 -->
                        <div class="entry entry-date-section notopmargin"><span>September 2015</span></div>

                        <div class="entry clearfix">
                            <div class="entry-timeline">
                                <div class="timeline-divider"></div>
                            </div>
                            
 <!-- Psalm: 29:11 - September 30, 2015 -->

                            <div class="entry-title">
                                <h2 class="tcolorgold">Psalm: 29:11 - September 30, 2015</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 9/30/2015</li>
                            </ul>
                            <div class="entry-content">
                                <p>The LORD will give strength to His people; The LORD will bless His people with peace...</p>
                                
                    
                                <button class="button button-3d button-small" data-toggle="modal" data-target=".sept302015">Read More</button>
        
                                <div class="modal fade sept302015" tabindex="-1" role="dialog" aria-labelledby="sept302015" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-body">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title heading tcolorgold" id="myModalLabel">Psalm: 29:11 - September 30, 2015</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p><strong>Living By God’s Principles Produces Love, Joy and Peace
Part Two -</strong></p>
                                                    <p>Get rid of grudges everyday.</p>
                                                    <p><strong>Ephesians 4:26 says if you’re angry, don’t sin by nursing a grudge.</strong>  “Don’t let the sun go down on your wrath.” Get over it quickly, for when you are angry, you give foothold to the devil.</p>
        
                                                    <p><strong>Spend some time every day getting to be more intimate with your spouse,</strong> your children your parents your brothers or sisters, because family harmony is essential to your mental and spiritual health.  Unresolved family conflicts will destroy you!</p>  
                                                    
                                                    <p><strong>Spend some time each week with committed Christians who are full of the spirit.</strong></p> 
                                                    
                                                    <p><strong>Proverbs 13:20 says, “He that walketh with wise men shall be wise:  but a companion of fools will be destroyed.”</strong>  Get with people of like precious faith and fellowship with them. When you run with a group of lounge lizards, you’ll be a lounge lizard. When you associate with crooks, you’ll be a crook. You want to be depressed?  Go in business with a crook. You don’ have friends?  The Bible says that if you want friends, you must show yourself friendly (Proverbs 18:24).</p>

                                                    <p><strong>Go to work on something that brings personal satisfaction to you.</strong></p>
                                                    
                                                    <p>The three faces of happiness are something to do, something to love, and something to hope for.</p>
                                                    
                                                    <p><strong>Do something nice for a special person every week.</strong></p>
                                                    
                                                    <p><strong>“Give and it shall be given unto you” (Luke 6:38).</strong> What do you want other people to give to you? Then give that to them. It can by your time, it can be your love, it can be flowers, it can be a card, or it can be a bowl of red beans, but give something special on a regular basis. You’ll find that by giving your life away, you’ll find it.</p>
                                                    
                                                    <p class="nobottommargin tcolorgold"><strong>Source: 12 Sunday Mornings - Volume 2</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
                        </div>
 
 <!-- Psalm: 29:11 - September 29, 2015 -->

						<div class="entry clearfix">
                            <div class="entry-timeline">
                                <div class="timeline-divider"></div>
                            </div>
                            
                            <div class="entry-title">
                                <h2 class="tcolorgold">Psalm: 29:11 - September 29, 2015</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 9/29/2015</li>
                            </ul>
                            <div class="entry-content">
                                <p>The LORD will give strength to His people; The LORD will bless His people with peace...</p>
                 
                                <button class="button button-3d button-small" data-toggle="modal" data-target=".sept292015">Read More</button>
        
                                <div class="modal fade sept292015" tabindex="-1" role="dialog" aria-labelledby="sept292015" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-body">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title heading tcolorgold" id="myModalLabel">Psalm: 71:2 - September 29, 2015</h4>
                                                </div>
                                                 <div class="modal-body">
                                                    <p><strong>Living By God’s Principles Produces Love, Joy and Peace
Part Two -</strong></p>
                                                    <p>Get rid of grudges everyday.</p>
                                                    <p><strong>Ephesians 4:26 says if you’re angry, don’t sin by nursing a grudge.</strong>  “Don’t let the sun go down on your wrath.” Get over it quickly, for when you are angry, you give foothold to the devil.</p>
        
                                                    <p><strong>Spend some time every day getting to be more intimate with your spouse,</strong> your children your parents your brothers or sisters, because family harmony is essential to your mental and spiritual health.  Unresolved family conflicts will destroy you!</p>  
                                                    
                                                    <p><strong>Spend some time each week with committed Christians who are full of the spirit.</strong></p> 
                                                    
                                                    <p><strong>Proverbs 13:20 says, “He that walketh with wise men shall be wise:  but a companion of fools will be destroyed.”</strong>  Get with people of like precious faith and fellowship with them. When you run with a group of lounge lizards, you’ll be a lounge lizard. When you associate with crooks, you’ll be a crook. You want to be depressed?  Go in business with a crook. You don’ have friends?  The Bible says that if you want friends, you must show yourself friendly (Proverbs 18:24).</p>

                                                    <p><strong>Go to work on something that brings personal satisfaction to you.</strong></p>
                                                    
                                                    <p>The three faces of happiness are something to do, something to love, and something to hope for.</p>
                                                    
                                                    <p><strong>Do something nice for a special person every week.</strong></p>
                                                    
                                                    <p><strong>“Give and it shall be given unto you” (Luke 6:38).</strong> What do you want other people to give to you? Then give that to them. It can by your time, it can be your love, it can be flowers, it can be a card, or it can be a bowl of red beans, but give something special on a regular basis. You’ll find that by giving your life away, you’ll find it.</p>
                                                    
                                                    <p class="nobottommargin tcolorgold"><strong>Source: 12 Sunday Mornings - Volume 2</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
                        </div>

	<!-- Psalm: 71:2 - September 28, 2015 -->
 						<div class="entry entry-date-section nomargin"></div>
						<div class="entry clearfix">
                            <div class="entry-timeline">
                                <div class="timeline-divider"></div>
                            </div>
                            
                            <div class="entry-title">
                                <h2 class="tcolorgold">Psalm: 71:2 - September 28, 2015</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 9/28/2015</li>
                            </ul>
                            <div class="entry-content">
                                <p>Deliver me in thy righteousness and cause me to escape: incline thine ear unto me, and save me...</p>
                 
                                <button class="button button-3d button-small" data-toggle="modal" data-target=".sept282015">Read More</button>
        
                                <div class="modal fade sept282015" tabindex="-1" role="dialog" aria-labelledby="sept282015" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-body">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title heading tcolorgold" id="myModalLabel">Psalm: 71:2 - September 28, 2015</h4>
                                                </div>
                                                 <div class="modal-body">
                                                    <p><strong>The Only Escape From Depression Is To Run To Almighty God!</strong></p>
                                                    <p>People all too often try to escape in ways which end up as exercises in futility.</p>
                                                    <p><strong>When you injure an arm, you automatically withdraw it, trying to keep other people from touching it.</strong> You do the same thing emotionally. When you’re hurt emotionally, you withdraw and try to insulate yourself, to keep other people from hurting you. Eventually you withdraw from reality. </p>
        
                                                    <p><strong>Alcoholics are trying to get in the bottle and escape the real world.</strong>  You can’t do it. There is no answer in alcohol or drugs. That is not the New Testament life-style. If you have to take a pill, take the “gos-pill.” It will give you’re the answers to life.</p>  
                                                    
                                                    <p><strong>Some people try to escape depression by eating too much.</strong> Sometimes when people get depressed or stressed out, they try to eat everything that’s in the refrigerator—every night. You know, when you get saved and you give up “the world,” you give up drinking, you give up drugs... all you can do is eat, but gluttony and drunkenness are equally bad.</p> 
                                                    
                                                    <p><strong>Exhibitionism is a way of trying to escape depression.</strong> A child will begin to have temper tantrums. I had two or three of those as a child. I lost my temper, and my dad helped me find it. Women will talk too much. Men will gamble obsessively. Teenage boys will drive recklessly or steal a car for a joy ride. A teenage girl will become promiscuous. Attempting suicide is a form of exhibitionism, an attempt to grab attention from family and friends. </p>

                                                    <p><strong>The only place you can run to for relief from depression is into the arms of Almighty God.  He will save you!</strong></p>
                                                    
                                                    <p class="nobottommargin tcolorgold"><strong>Source: 12 Sunday Mornings - Volume 2</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
                        </div>
                        
                        

	<!-- Psalm: 71:2 - September 27, 2015 -->

						<div class="entry clearfix">
                            <div class="entry-timeline">
                                <div class="timeline-divider"></div>
                            </div>
                            
                            <div class="entry-title">
                                <h2 class="tcolorgold">Psalm: 71:2 - September 27, 2015</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 9/27/2015</li>
                            </ul>
                            <div class="entry-content">
                                <p>Deliver me in thy righteousness and cause me to escape: incline thine ear unto me, and save me...</p>
                 
                                <button class="button button-3d button-small" data-toggle="modal" data-target=".sept272015">Read More</button>
        
                                <div class="modal fade sept272015" tabindex="-1" role="dialog" aria-labelledby="sept272015" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-body">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title heading tcolorgold" id="myModalLabel">Psalm: 71:2 - September 27, 2015</h4>
                                                </div>
                                                 <div class="modal-body">
                                                    <p><strong>The Only Escape From Depression Is To Run To Almighty God!</strong></p>
                                                    <p>People all too often try to escape in ways which end up as exercises in futility.</p>
                                                    <p><strong>When you injure an arm, you automatically withdraw it, trying to keep other people from touching it.</strong> You do the same thing emotionally. When you’re hurt emotionally, you withdraw and try to insulate yourself, to keep other people from hurting you. Eventually you withdraw from reality. </p>
        
                                                    <p><strong>Alcoholics are trying to get in the bottle and escape the real world.</strong>  You can’t do it. There is no answer in alcohol or drugs. That is not the New Testament life-style. If you have to take a pill, take the “gos-pill.” It will give you’re the answers to life.</p>  
                                                    
                                                    <p><strong>Some people try to escape depression by eating too much.</strong> Sometimes when people get depressed or stressed out, they try to eat everything that’s in the refrigerator—every night. You know, when you get saved and you give up “the world,” you give up drinking, you give up drugs... all you can do is eat, but gluttony and drunkenness are equally bad.</p> 
                                                    
                                                    <p><strong>Exhibitionism is a way of trying to escape depression.</strong> A child will begin to have temper tantrums. I had two or three of those as a child. I lost my temper, and my dad helped me find it. Women will talk too much. Men will gamble obsessively. Teenage boys will drive recklessly or steal a car for a joy ride. A teenage girl will become promiscuous. Attempting suicide is a form of exhibitionism, an attempt to grab attention from family and friends. </p>

                                                    <p><strong>The only place you can run to for relief from depression is into the arms of Almighty God.  He will save you!</strong></p>
                                                    
                                                    <p class="nobottommargin tcolorgold"><strong>Source: 12 Sunday Mornings - Volume 2</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
                        </div>
                        

	<!-- Philippians 3:13 - September 26, 2015 -->
						<div class="entry entry-date-section nomargin"></div>
						<div class="entry clearfix">
                            <div class="entry-timeline">
                                <div class="timeline-divider"></div>
                            </div>
                            
                            <div class="entry-title">
                                <h2 class="tcolorgold">Philippians 3:13 - September 26, 2015</h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> 9/26/2015</li>
                            </ul>
                            <div class="entry-content">
                                <p>Brethren, I do not count myself to have apprehended; but one thing I do, forgetting those things which are behind and reaching forward to those things which are ahead...</p>
                 
                                <button class="button button-3d button-small" data-toggle="modal" data-target=".sept262015">Read More</button>
        
                                <div class="modal fade sept262015" tabindex="-1" role="dialog" aria-labelledby="sept262015" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-body">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title heading tcolorgold" id="myModalLabel">Philippians 3:13 - September 26, 2015</h4>
                                                </div>
                                                 <div class="modal-body">
                                                    <p><strong>Who Stays Depressed? Those Who Choose To!</strong></p>
                                                    
                                                    <p><strong>Some of you are majoring in your problem.</strong> All you want to do is wade through the sewer of yesterday, remembering what happened to you back in 1982. I’m telling you in the name of Jesus Christ, forget it. Think on things that are good and wholesome, that are honest and true—and when you possess those thoughts, you will have a hope that is steadfast and sure. You won’t be thinking about your problems, you’ll be rejoicing in the solutions that Jesus Christ has given you on the authority of God’s word.</p>
        
                                                    <p><strong>The Bible says, “Hope thou in God” (Psalm 42:5). “Have not I commanded thee?  Be strong and of a good courage; be not afraid, neither be thou dismayed” (Joshua 1:9).</strong> Rejoice in the Lord always; and again I say, rejoice,” Paul wrote (Philippians 4:4). You rejoice by choice. It’s something you decide to do. You must be willing to face the fact that your mental attitude toward the circumstance, not the circumstance itself, is the cause of your unhappiness.</p>  
                                                    
                                                    <p class="nobottommargin tcolorgold"><strong>Source: 12 Sunday Mornings - Volume 2</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
                        </div>


                        

                    </div><!-- #posts end -->

                    <div id="load-next-posts" class="center">
                        <a href="#" class="button button-3d button-dark">Load More</a>
                    </div>

                    <script type="text/javascript">

                        jQuery(window).load(function(){

                            var $container = $('#posts');

                            $container.isotope({
                                itemSelector: '.entry',
                                masonry: {
                                    columnWidth: '.entry:not(.entry-date-section)'
                                }
                            });

                            $container.infinitescroll({
                                loading: {
                                    finishedMsg: '<i class="icon-line-check"></i>',
                                    msgText: '<i class="icon-line-loader icon-spin"></i>',
                                    img: "images/preloader-dark.gif",
                                    speed: 'normal'
                                },
                                state: {
                                    isDone: false
                                },
                                nextSelector: "#load-next-posts a",
                                navSelector: "#load-next-posts",
                                itemSelector: "div.entry",
                                behavior: 'portfolioinfiniteitemsloader'
                            },
                            function( newElements ) {
                                $container.isotope( 'appended', $( newElements ) );
                                var t = setTimeout( function(){ $container.isotope('layout'); }, 2000 );
                                SEMICOLON.initialize.resizeVideos();
                                SEMICOLON.widget.loadFlexSlider();
                                SEMICOLON.widget.masonryThumbs();
                                var t = setTimeout( function(){
                                    SEMICOLON.initialize.blogTimelineEntries();
                                }, 2500 );
                            });

                            var t = setTimeout( function(){
                                SEMICOLON.initialize.blogTimelineEntries();
                            }, 2500 );

                            $(window).resize(function() {
                                $container.isotope('layout');
                                var t = setTimeout( function(){
                                    SEMICOLON.initialize.blogTimelineEntries();
                                }, 2500 );
                            });

                        });

                    </script>

                </div>
			</div>

            <div class="clear"></div>
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>