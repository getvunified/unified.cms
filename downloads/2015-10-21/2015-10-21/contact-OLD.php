<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

	<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries - Contact Us</title>
</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	<div id="side-panel" class="dark">

		<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	</div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">


        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
		
         <!-- Page Title
        ============================================= -->
        <section id="page-title">

            <div class="container clearfix">
                <h1>Contact Us</h1>
                <span>Get in touch with John Hagee Ministries</span>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Contact</li>
                </ol>
            </div>

        </section><!-- #page-title end -->
        
        <!-- Content
		============================================= -->
		<section id="content">
        
			<div class="section noborder notopmargin nobottommargin">
				<div class="container topmargin-sm clearfix">
					
                    
                        <!-- Contact Form
                    ============================================= -->
                    <div class="col_half">

                        <h3>Send us an Email</h3>
                       
                        <div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>

                        <form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">

                            <div class="form-process"></div>

                            <div class="col_one_third">
                                <label for="template-contactform-name">Your Name <small>*</small></label>
                                <input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />
                            </div>

                            <div class="col_one_third">
                                <label for="template-contactform-email">Your Email <small>*</small></label>
                                <input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
                            </div>

                            <div class="col_one_third col_last">
                                <label for="template-contactform-phone">Your Phone</label>
                                <input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control" />
                            </div>

                            <div class="clear"></div>

                            <div class="col_two_third">
                                <label for="template-contactform-subject">Subject <small>*</small></label>
                                <input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" />
                            </div>

                            <div class="col_one_third col_last">
                                <label for="template-contactform-service">JHM Resources</label>
                                <select id="template-contactform-service" name="template-contactform-service" class="sm-form-control">
                                    <option value="">-- Select One --</option>
                                    <option value="GETV">GETV</option>
									<option value="Difference Media">Difference Media</option>
									<option value="Cornerstone Church">Cornerstone Church</option>
									<option value="Cornerstone School">Cornerstone School</option>
									<option value="Prayer Requests">Prayer Requests</option>
									<option value="News/Events">News/Events</option>
                                    <option value="Shop">Shop</option>
                                    <option value="Donations">Donations</option>
                                    <option value="Social Media">Social Media</option>
                                </select>
                            </div>

                            <div class="clear"></div>

                            <div class="col_full">
                                <label for="template-contactform-message">Message <small>*</small></label>
                                <textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
                            </div>

                            <div class="col_full hidden">
                                <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
                            </div>

                            <div class="col_full">
                                <button name="submit" type="submit" id="submit-button" tabindex="5" value="Submit" class="button button-3d nomargin">Submit Comment</button>
                            </div>

                        </form>

                        <script type="text/javascript">

                            $("#template-contactform").validate({
                                submitHandler: function(form) {
                                    $('.form-process').fadeIn();
                                    $(form).ajaxSubmit({
                                        target: '#contact-form-result',
                                        success: function() {
                                            $('.form-process').fadeOut();
                                            $('#template-contactform').find('.sm-form-control').val('');
                                            $('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
                                            SEMICOLON.widget.notifications($('#contact-form-result'));
                                        }
                                    });
                                }
                            });

                        </script>

                    </div><!-- Contact Form End -->

                    <!-- Google Map
                    ============================================= -->
                    <div class="col_half col_last">

                        <section id="google-map" class="gmap" style="height: 410px;"></section>

                        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                        <script type="text/javascript" src="js/jquery.gmap.js"></script>

                        <script type="text/javascript">

                            jQuery('#google-map').gMap({

                                address: 'P.O. Box 1400, San Antonio, Texas 78295-1400',
                                maptype: 'ROADMAP',
                                zoom: 14,
                                markers: [
                                    {
                                        address: "P.O. Box 1400, San Antonio, Texas 78295-1400",
                                        html: '<div style="width: 300px;"><h4 style="margin-bottom: 8px;">Welcome, we\'re <span>John Hagee Ministries</span></h4><p class="nobottommargin"><strong>The mission of John Hagee Ministries</strong> is to aggressively fulfill the commission that Jesus Christ gave to His followers to go into the world and make disciples of all people. Our purpose is to bring the lost to Jesus Christ and to build up and encourage those who are already believers. We pledge to our viewers & supporters to take... <strong>"The Gospel to all the World and to all Generations."</strong></p></div>',
                                        icon: {
                                            image: "images/icons/map-icon-red.png",
                                            iconsize: [32, 39],
                                            iconanchor: [32,39]
                                        }
                                    }
                                ],
                                doubleclickzoom: false,
                                controls: {
                                    panControl: true,
                                    zoomControl: true,
                                    mapTypeControl: true,
                                    scaleControl: false,
                                    streetViewControl: false,
                                    overviewMapControl: false
                                }

                            });

                        </script>

                    </div><!-- Google Map End -->

                    <div class="clear"></div>

                    <!-- Contact Info
                    ============================================= -->
                    <div class="row clear-bottommargin">

                        <div class="col-md-3 col-sm-6 bottommargin clearfix">
                            <div class="feature-box fbox-center fbox-bg fbox-plain">
                                <div class="fbox-icon">
                                    <a href="https://www.google.com/maps/dir//San+Antonio,+TX+78295/@29.43,-98.49,17z/data=!4m13!1m4!3m3!1s0x865c58ad3c284495:0xd1a346cf29304c71!2sSan+Antonio,+TX+78295!3b1!4m7!1m0!1m5!1m1!1s0x865c58ad3c284495:0xd1a346cf29304c71!2m2!1d-98.49!2d29.43" target="_blank"><i class="icon-map-marker2"></i></a>
                                </div>
                                <h3>Our Headquarters<span class="subtitle">San Antonio, Texas</span></h3>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 bottommargin clearfix">
                            <div class="feature-box fbox-center fbox-bg fbox-plain">
                                <div class="fbox-icon">
                                    <i class="icon-phone3"></i>
                                </div>
                                <h3>Speak With Us<span class="subtitle">1-800-854-9899</span></h3>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 bottommargin clearfix">
                            <div class="feature-box fbox-center fbox-bg fbox-plain">
                                <div class="fbox-icon">
                                    <a href="https://www.facebook.com/JohnHageeMinistries" target="_blank"><i class="icon-facebook2"></i></a>
                                </div>
                                <h3>JHM on Facebook<span class="subtitle">1,805,000+ Likes</span></h3>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 bottommargin clearfix">
                            <div class="feature-box fbox-center fbox-bg fbox-plain">
                                <div class="fbox-icon">
                                    <a href="https://twitter.com/intent/follow?source=followbutton&variant=1.0&screen_name=PastorJohnHagee" target="_blank"><i class="icon-twitter2"></i></a>
                                </div>
                                <h3>JHM on Twitter<span class="subtitle">205,000+ Followers</span></h3>
                            </div>
                        </div>

                    </div><!-- Contact Info End -->
				</div>
				<div class="line"></div>
                           		<!-- JHM Connect
					============================================= -->
                    <?php include "include/jhm-connect.php";?>


        </div>

        </section><!-- #content end -->

		<!-- Footer
		============================================= -->
        
					<!-- Footer
					============================================= -->
                    <?php include "include/footer.php";?>
                    
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>

</body>
</html>