<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
                <?php include "func.php"; ?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Daily Truth</title>
        <script type="text/javascript" src="js/sticky.js"></script>
	<script type="text/javascript">
            jQuery(document).ready(function() {
			jQuery(document).on("click", '.load-cal', function(e) {
				e.preventDefault();
				//remove ".html('loading...')" below if you do not want to show this while calendar is loading
				jQuery('#gl-cal').load(jQuery(this).attr('href'));
			});
                        //show filter on click
                jQuery('.filter-button').click(function(e) {
                    e.preventDefault();
                    if(jQuery(this).hasClass('closed')) {
                        jQuery('.filter-dropdown').hide();
                        jQuery(this.rel).slideDown(200);
                        jQuery(this).toggleClass('closed open');
                    } else {
                        jQuery(this.rel).slideUp(200);
                        jQuery(this).toggleClass('closed open');
                    }
                });
                    jQuery('#dt-date-nav').sticky({topSpacing:60});
		});
	</script>
</head>

<body class="stretched side-push-panel daily-truth daily-truth-single">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>

  		
       
       
		<!-- Page Header
		============================================= -->
        <?php include "include/pageheader-dailytruth-single.php";?>

		


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">
            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">Encouragement</a></li>
                    <li class="active blue-text">Daily #Truth</li>
                </ol>
            </div>
        </section><!-- #page-title end -->
        
                <!-- Date Navigation
        ============================================= -->
        <section id="dt-date-nav">
            <div class="container dt-single-center clearfix">
                <h3>September 10, 2015</h3>
                <!--<img src="images/daily-truth/social-grey.gif" />-->
                <div class="si-share">
                    <div>
                        <span>Share:</span>
                        <a href="#" class="social-icon si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Pinterst">
                            <i class="icon-pinterest"></i>
                            <i class="icon-pinterest"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-vimeo" data-toggle="tooltip" data-placement="top" title="Vimeo">
                            <i class="icon-vimeo"></i>
                            <i class="icon-vimeo"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="Instagram">
                            <i class="icon-instagram"></i>
                            <i class="icon-instagram"></i>
                        </a>
                    </div>
                </div>
                <a href="#" class="dt-date-click prev">Previous</a>
                <a href="#" class="dt-date-click next">Next</a>
            </div>
        </section>
		
		<!-- Selections
        ============================================= -->
		<section id="content">
            <div class="container clearfix">

            </div>
        <!-- selections end -->

		
		<!-- Content
		============================================= -->
		
		
            <div class="section noborder notopmargin nobottommargin toppadding-sm">
                <!-- Quote
                ============================================= -->
                <div data-animate="fadeIn" class="container clearfix border-full bottommargin">
                    <div class="entry-content">
                        <div class="entry-title topmargin-xsm dt-single-title">
                            <div class="col_half nobottommargin col_respon">
                                <h3 class="daily blue-text"><a class="blue-text" href="daily-truth-single-blog.php">Hope thou in God!</a><span>Pastor John Hagee</span></h3>
                            </div>
                            
                            <div class="col_half fright col_last nobottommargin col_respon">
                                <img src="images/daily-truth/quote.gif" />
                            </div>
			</div>
                    </div>

                    <div class="clear"></div>

                    <div class="dt-single-content dt-single-center clearfix entry">
                        <a href="daily-truth-single-blog.php"><img src="images/daily-truth/quote-pic.jpg" /></a>
                        <blockquote>
                            <p>"Hope Sees the invisible. Hope Feels the intangible. Hope Achieves the impossible. HOPE THOU IN GOD!"</p>
                            <footer>John C. Hagee</footer>
                        </blockquote>
                        <div class="dt-single-social">
                            <div class="si-share">
                    <div>
                        <span>Share:</span>
                        <a href="#" class="social-icon si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Pinterst">
                            <i class="icon-pinterest"></i>
                            <i class="icon-pinterest"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-vimeo" data-toggle="tooltip" data-placement="top" title="Vimeo">
                            <i class="icon-vimeo"></i>
                            <i class="icon-vimeo"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="Instagram">
                            <i class="icon-instagram"></i>
                            <i class="icon-instagram"></i>
                        </a>
                    </div>
                </div>
                        </div>
                    </div>
                </div>
                
                <!-- Blog Post
                ============================================= -->
                <div data-animate="fadeIn" class="container clearfix border-full bottommargin">
                    <div class="entry-content">
                        <div class="entry-title topmargin-xsm dt-single-title">
                            <div class="col_half nobottommargin col_respon">
                                <h3 class="daily"><a class="blue-text" href="daily-truth-single-blog.php">Whose Mess is this?</a><span>Pastor Matthew Hagee</span></h3>
                            </div>
                            
                            <div class="col_half fright col_last nobottommargin col_respon">
                                <img src="images/daily-truth/blog-post.gif" />
                            </div>
			</div>
                    </div>

                    <div class="clear"></div>

                    <div class="dt-single-content clearfix entry">
                        <a href="daily-truth-single-blog.php"><img src="images/daily-truth/blog-pic.jpg" class="fleft rightmargin-sm" /></a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <div class="dt-single-social dt-single-center">
                            <div class="si-share">
                    <div>
                        <span>Share:</span>
                        <a href="#" class="social-icon si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Pinterst">
                            <i class="icon-pinterest"></i>
                            <i class="icon-pinterest"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-vimeo" data-toggle="tooltip" data-placement="top" title="Vimeo">
                            <i class="icon-vimeo"></i>
                            <i class="icon-vimeo"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="Instagram">
                            <i class="icon-instagram"></i>
                            <i class="icon-instagram"></i>
                        </a>
                    </div>
                </div>
                        </div>
                    </div>
                </div>
                
                <!-- Bible Verse
                ============================================= -->
		<div data-animate="fadeIn" class="container clearfix border-full bottommargin">
                    <div class="entry-content">
                        <div class="entry-title topmargin-xsm dt-single-title">
                            <div class="col_half nobottommargin col_respon">
                                <h3 class="daily blue-text"><a class="blue-text" href="daily-truth-single-blog.php">Psalm 91:15-16</a></h3>
                            </div>
                            
                            <div class="col_half fright col_last nobottommargin col_respon">
                                <img src="images/daily-truth/bible-verse.gif" />
                            </div>
			</div>
                    </div>

                    <div class="clear"></div>

                    <div class="dt-single-content dt-single-center clearfix entry">
                        <a href="daily-truth-single-blog.php"><img src="images/daily-truth/bible-verse-pic.jpg" /></a>
                        <blockquote>
                            <p>"Hope Sees the invisible. Hope Feels the intangible. Hope Achieves the impossible. HOPE THOU IN GOD!"</p>
                            <footer>John C. Hagee</footer>
                        </blockquote>
                        <div class="dt-single-social">
                            <div class="si-share">
                    <div>
                        <span>Share:</span>
                        <a href="#" class="social-icon si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Pinterst">
                            <i class="icon-pinterest"></i>
                            <i class="icon-pinterest"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-vimeo" data-toggle="tooltip" data-placement="top" title="Vimeo">
                            <i class="icon-vimeo"></i>
                            <i class="icon-vimeo"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="Instagram">
                            <i class="icon-instagram"></i>
                            <i class="icon-instagram"></i>
                        </a>
                    </div>
                </div>
                        </div>
                    </div><!-- Daily Truths end -->
                </div>



            </div>
		</section>
				
			<div class="clear"></div>
            

			<!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>