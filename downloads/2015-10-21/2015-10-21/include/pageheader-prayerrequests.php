<div id="slider" class="parallax clearfix dark" style="background-image: url('images/prayer-requests.jpg'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-xlg">
        <div class="slider-caption slider-caption-center bottommargin">
            <h3 data-animate="fadeIn">Prayer Requests</h3>
            <p data-animate="fadeIn" class="lead">Your prayer request will be shared with our community, so that they may pray for you.</p>
        </div>
    </div>
</div>