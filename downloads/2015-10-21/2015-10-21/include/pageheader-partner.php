<div id="slider" class="parallax clearfix dark" style="background-image: url('images/partner-background.jpg'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-xlg">
        <div class="slider-caption slider-caption-center bottommargin">
            <h3 data-animate="fadeIn">Partner with JHM</h3>
            <p data-animate="fadeIn" class="lead">Discover new ways to engage with JHM through a gift or partnership</p>
        </div>
    </div>
</div>