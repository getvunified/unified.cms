<div id="slider" class="parallax clearfix dark" style="background-image: url('images/saltcovenant-partnership-background.jpg'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-xlg">
        <div class="slider-caption slider-caption-center bottommargin">
            <h3 data-animate="fadeIn">Salt Covenant Partnership</h3>
            <p data-animate="fadeIn" class="lead">Season all your grain offerings with salt. Do not leave the salt of the covenant of your God out of your grain offerings; add salt to all your offerings...</p>
        </div>
    </div>
</div>