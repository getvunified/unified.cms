<div id="slider" class="parallax clearfix dark" style="background-image: url('images/email-background.jpg'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-xlg">
        <div class="slider-caption slider-caption-center bottommargin">
            <h3 data-animate="fadeIn">Email Subscriptions</h3>
            <p data-animate="fadeIn" class="lead">Receive daily encouragement and motivation by connecting with John Hagee Ministries.</p>
        </div>
    </div>
</div>