<section id="slider" class="slider-parallax" style="background-color: #222;">

            <div id="oc-slider" class="owl-carousel">

                <a href="#"><img src="images/slide1.jpg" alt="Slider"></a>
                <a href="#"><img src="images/slide3.jpg" alt="Slider"></a>
            </div>

            <script>

            jQuery(document).ready(function($) {

                var ocSlider = $("#oc-slider");

                ocSlider.owlCarousel({
                    items: 1,
                    nav: true,
                    navText : ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
                    animateOut: 'fadeUp',
                    animateIn: 'fadeUp',
                    smartSpeed: 450,
                    autoplay: true,
                    loop: true
                });

            });

            </script>

        </section>
