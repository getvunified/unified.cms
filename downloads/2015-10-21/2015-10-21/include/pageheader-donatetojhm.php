<div id="slider" class="parallax clearfix dark" style="background-image: url('images/donatetojhm-background.jpg'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-xlg">
        <div class="slider-caption slider-caption-center">
            <img src="images/spine-logo-jgm.png" alt="Donate to JHM" class="" style="width:250px;">
            <h3 class="topmargin-xsm">Donate to JHM</h3>
        </div>
    </div>
</div>