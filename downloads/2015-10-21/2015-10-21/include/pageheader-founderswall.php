<div id="slider" class="parallax clearfix dark" style="background-image: url('images/founderswall-background.jpg'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-xlg">
        <div class="slider-caption slider-caption-center bottommargin">
            <h3 data-animate="fadeIn">Founders Wall</h3>
            <p data-animate="fadeIn" class="lead">Text here</p>
        </div>
    </div>
</div>