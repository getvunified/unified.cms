<div id="slider" class="parallax clearfix dark" style="background-image: url('images/email-background.jpg'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-lg">
        <div class="slider-caption slider-caption-center bottommargin">
            <h3 data-animate="fadeIn">Email Signup</h3>
            <p data-animate="fadeIn" class="lead">Our email signup offers a new way to connect with JHM.</p>
        </div>
    </div>
</div>