<!-- Top Links 
============================================= -->
<div class="top-links">
    <ul>
        <li><a href="#"> <i class="icon-line2-user"></i>Login</a>
            <div class="top-link-section">
                <form id="top-login" role="form">
                    <div class="input-group" id="top-login-username">
                        <span class="input-group-addon"><i class="icon-mail"></i></span>
                        <input type="email" class="form-control" placeholder="Email address" required="">
                    </div>
                    <div class="input-group" id="top-login-password">
                        <span class="input-group-addon"><i class="icon-key"></i></span>
                        <input type="password" class="form-control" placeholder="Password" required="">
                    </div>
                    <label class="checkbox">
                      <input type="checkbox" value="remember-me"> Remember me
                    </label>
                    <button class="button button-3d button-small btn-block" type="submit">Login</button>
                </form>
            </div>
        </li>
        <li><a href="login-register.php"><i class="icon-key"></i> Register</a>
            
        </li>
    </ul>
</div> <!--.top-links end -->