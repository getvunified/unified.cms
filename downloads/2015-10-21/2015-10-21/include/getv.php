<script>
function startVideo(){
var pElement3 = document.getElementById('myVideo');
pElement3.play();
}
</script>

<div class="section parallax nomargin notopborder dark">
    <div class="container center clearfix">
        <div class="slider-caption slider-caption-center bottommargin-lg topmargin-lg">
           <h2 data-animate="fadeIn">GETV Stream 24/7</h2>
            <p data-animate="fadeIn" class="lead">View our 24/7 live stream of exclusive and original content.</p>
            <a href="#" data-animate="fadeIn" class="button button-3d button-large">Live Stream</a>
        </div>
    </div>
    
    <div class="video-wrap">
        		<!-- Start EasyHtml5Video.com BODY section -->
			<style type="text/css">.easyhtml5video .eh5v_script{display:none}</style>

            <div class="easyhtml5video" style="position:relative;max-width:1920px;"><video id="myVideo" preload="auto" loop autoplay muted poster="images/videos/getv-video.jpg" style="width:100%" title="getv-video" loop onended="var v=this;setTimeout(function(){v.play()},300)">
            <source src="images/videos/getv-video.m4v" type="video/mp4" />
            <source src="images/videos/getv-video.webm" type="video/webm" />
            <source src="images/videos/getv-video.ogv" type="video/ogg" />
            <source src="images/videos/getv-video.mp4" />
            <object type="application/x-shockwave-flash" data="images/videos/flashfox.swf" width="1920" height="1080" style="position:relative;">
            <param name="movie" value="images/videos/flashfox.swf" />
            <param name="allowFullScreen" value="true" />
            <param name="flashVars" value="autoplay=true&controls=true&fullScreenEnabled=true&posterOnEnd=true&loop=true&poster=images/videos/getv-video.jpg&src=getv-video.m4v" />
             <embed src="images/videos/flashfox.swf" width="1920" height="1080" style="position:relative;"  flashVars="autoplay=true&controls=true&fullScreenEnabled=true&posterOnEnd=true&loop=true&poster=images/videos/getv-video.jpg&src=getv-video.m4v"	allowFullScreen="true" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_en" />
            <img alt="getv-video" src="images/videos/getv-video.jpg" style="position:absolute;left:0;" width="100%" title="Video playback is not supported by your browser" />
            </object>
            </video></div>
            <script src="images/videos/html5ext.js" type="text/javascript"></script>
            <!-- End EasyHtml5Video.com BODY section -->

		
        <div class="video-overlay" style="background-color: rgba(0,0,0,0.1);"></div>
    </div>
</div>

<script type="text/javascript">
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSURLRequest* request = [webView request];
    NSString *page = [request.URL lastPathComponent];
    if ([page isEqualToString:@"getv.php"]){
        NSString *js = @"startVideo();";
        [myWebMain stringByEvaluatingJavaScriptFromString:js];
    }
}
</script>