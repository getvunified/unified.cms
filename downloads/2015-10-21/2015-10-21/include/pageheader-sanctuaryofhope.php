<div id="slider" class="parallax clearfix dark" style="background-image: url('images/sanctuaryofhope-background.jpg'); " data-stellar-background-ratio="0.3">
    <div data-animate="fadeIn" class="container clearfix bottompadding-xlg">
        <div class="slider-caption slider-caption-center">
            <img src="images/sanctuary-of-hope-logo.png" alt="Sanctuary of Hope" class="" style="width:190px;">
            <h3 class="topmargin-xsm">Sanctuary of Hope</h3>
        </div>
    </div>
</div>