<div id="slider" class="parallax clearfix dark" style="background-image: url('images/jhmmagazine-background.jpg'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-lg">
        <div class="slider-caption slider-caption-center bottommargin">
            <h3 data-animate="fadeIn">JHM Digital Magazine</h3>
            <p data-animate="fadeIn" class="lead">JHM Magazine endeavors to publish imaginative poetry, short fiction, interviews, essays & translations of living poets from every corner of the world.</p>
        </div>
    </div>
</div>