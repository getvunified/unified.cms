<div id="slider" class="parallax clearfix dark" style="background-image: url('images/whoweare-background2.jpg'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-xlg">
        <div class="slider-caption slider-caption-center bottommargin">
            <h3 data-animate="fadeIn">Who We Are</h3>
            <p data-animate="fadeIn" class="lead">Our purpose at John Hagee Ministries is to bring the lost to Jesus Christ
 while encouraging those who are already believers.</p>
        </div>
    </div>
</div>