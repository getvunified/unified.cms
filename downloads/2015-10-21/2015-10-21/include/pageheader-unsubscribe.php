<div id="slider" class="parallax clearfix dark" style="background-image: url('images/unsubscribe-background.png'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-xlg">
        <div class="slider-caption slider-caption-center bottommargin">
            <h3 data-animate="fadeIn">Unsubscribe</h3>
            <p data-animate="fadeIn" class="lead">Use the tools below to disconnect from JHM.</p>
        </div>
    </div>
</div>