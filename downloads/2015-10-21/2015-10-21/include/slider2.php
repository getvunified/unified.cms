<section id="slider2" class="slider-parallax swiper_wrapper clearfix full-screen">

    <div class="swiper-container swiper-parent">
        <div class="swiper-wrapper">
            <div class="swiper-slide dark" style="background-image: url('images/home-slider2/1.jpg');">
                <div class="container clearfix">
                    <div class="slider-caption slider-caption-center">
                        <img src="images/home-slider2/sig1.png" />
                        <p class="nomargin">
                            Pastor John C. Hagee is the founder and Senior Pastor of Cornerstone Church in San Antonio, Texas, a non-denominational evangelical church with more than 20,000 active members. Pastor Hagee has served the Lord in the gospel ministry for over 57 years.
                        </p>
                        <p class="topmargin-xsm"><a href="#" class="button button-3d"><span>Read More</span></a></p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide dark" style="background-image: url('images/home-slider2/2.jpg');">
                <div class="container clearfix">
                    <div class="slider-caption slider-caption-center">
                        <img src="images/home-slider2/sig2.png" />
                        <p class="nomargin">Pastor Matthew Charles Hagee is the sixth generation in the Hagee family to carry the mantel of Gospel ministry. He serves as the Executive Pastor of the 20,000 member Cornerstone Church in San Antonio, Texas where he partners with his father, founder Pastor John Hagee.</p>
                        <p class="topmargin-xsm"><a href="#" class="button button-3d"><span>Read More</span></a></p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide dark" style="background-image: url('images/home-slider2/3.jpg');">
                <div class="container clearfix">
                    <div class="slider-caption slider-caption-center">
                        <img src="images/home-slider2/sig3.png" />
                        <p class="nomargin">Diana Hagee is the wife of Pastor John Hagee, founder and senior pastor of Cornerstone Church in San Antonio, Texas. She coordinates all special events for John Hagee Ministries, Cornerstone Church, Cornerstone Christian Schools, and Christians United for Israel.</p>
                        <p class="topmargin-xsm"><a href="#" class="button button-3d"><span>Read More</span></a></p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide dark" style="background-image: url('images/home-slider2/4.jpg');">
                <div class="container clearfix">
                    <div class="slider-caption slider-caption-center">
                        <img src="images/home-slider2/sig4.png" />
                        <p class="nomargin">Kendal Hagee is the wife of Pastor Matthew Hagee, Executive Pastor of Cornerstone Church in San Antonio, Texas, a non-denominational evangelical church with more than 20,000 active members.</p>
                        <p class="topmargin-xsm"><a href="#" class="button button-3d"><span>Read More</span></a></p>
                    </div>
                </div>
            </div>
        </div>
        <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
        <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
        <div class="swiper-pagination"></div>
    </div>

    <script>
        jQuery(document).ready(function($){
            var swiperSlider2 = new Swiper('#slider2 .swiper-parent',{
                paginationClickable: false,
                slidesPerView: 1,
                grabCursor: true,
                autoplay: 5000,
                speed: 150,
                loop: true,
				arrows: false,
                onSwiperCreated: function(swiper){
                    $('[data-caption-animate]').each(function(){
                        var $toAnimateElement = $(this);
                        var toAnimateDelay = $(this).attr('data-caption-delay');
                        var toAnimateDelayTime = 0;
                        if( toAnimateDelay ) { toAnimateDelayTime = Number( toAnimateDelay ) + 650; } else { toAnimateDelayTime = 650; }
                        if( !$toAnimateElement.hasClass('animated') ) {
                            $toAnimateElement.addClass('not-animated');
                            var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                            setTimeout(function() {
                                $toAnimateElement.removeClass('not-animated').addClass( elementAnimation + ' animated');
                            }, toAnimateDelayTime);
                        }
                    });
                    SEMICOLON.slider.swiperSliderMenu();
                },
                onSlideChangeStart: function(swiper){
                    $('#slide-number-current').html(swiper.activeLoopIndex + 1);
                    $('[data-caption-animate]').each(function(){
                        var $toAnimateElement = $(this);
                        var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                        $toAnimateElement.removeClass('animated').removeClass(elementAnimation).addClass('not-animated');
                    });
                    SEMICOLON.slider.swiperSliderMenu();
                },
                onSlideChangeEnd: function(swiper){
                    $('#slider2').find('.swiper-slide').each(function(){
                        if($(this).find('video').length > 0) { $(this).find('video').get(0).pause(); }
                        if($(this).find('.yt-bg-player').length > 0) { $(this).find('.yt-bg-player').pauseYTP(); }
                    });
                    $('#slider2').find('.swiper-slide:not(".swiper-slide-active")').each(function(){
                        if($(this).find('video').length > 0) {
                            if($(this).find('video').get(0).currentTime != 0 ) $(this).find('video').get(0).currentTime = 0;
                        }
                        if($(this).find('.yt-bg-player').length > 0) {
                            $(this).find('.yt-bg-player').getPlayer().seekTo( $(this).find('.yt-bg-player').attr('data-start') );
                        }
                    });
                    if( $('#slider2').find('.swiper-slide.swiper-slide-active').find('video').length > 0 ) { $('#slider2').find('.swiper-slide.swiper-slide-active').find('video').get(0).play(); }
                    if( $('#slider2').find('.swiper-slide.swiper-slide-active').find('.yt-bg-player').length > 0 ) { $('#slider2').find('.swiper-slide.swiper-slide-active').find('.yt-bg-player').playYTP(); }

                    $('#slider2 .swiper-slide.swiper-slide-active [data-caption-animate]').each(function(){
                        var $toAnimateElement = $(this);
                        var toAnimateDelay = $(this).attr('data-caption-delay');
                        var toAnimateDelayTime = 0;
                        if( toAnimateDelay ) { toAnimateDelayTime = Number( toAnimateDelay ) + 200; } else { toAnimateDelayTime = 200; }
                        if( !$toAnimateElement.hasClass('animated') ) {
                            $toAnimateElement.addClass('not-animated');
                            var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                            setTimeout(function() {
                                $toAnimateElement.removeClass('not-animated').addClass( elementAnimation + ' animated');
                            }, toAnimateDelayTime);
                        }
                    });
                }
            });
            
            $('#slider-arrow-left').on('click', function(e){
                e.preventDefault();
                swiperSlider2.swipePrev();
            });

            $('#slider-arrow-right').on('click', function(e){
                e.preventDefault();
                swiperSlider2.swipeNext();
            });

            $('#slide-number-current').html(swiperSlider2.activeLoopIndex + 1);
            $('#slide-number-total').html($('#slider2').find('.swiper-slide:not(.swiper-slide-duplicate)').length);
            
        });
    </script>

</section>
