<div id="slider" class="parallax clearfix dark" style="background-image: url('images/careers-background.jpg'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-xlg">
        <div data-animate="fadeIn" class="slider-caption slider-caption-center bottommargin">
            <img src="images/spine-logo-jgm.png" alt="John Hagee Ministries" class="" style="width:240px;">
            <p>Become part of our mission to spread all the Gospel to all the world and to all generations.</p>
        </div>
    </div>
</div>