<div class="section parallax notopborder nobottompadding notoppadding nomargin dark" style="background-image: url('images/prayer-requests.jpg'); background-color: #000000; padding-top:50px;" data-stellar-background-ratio="0.3">
	<div data-animate="fadeIn" class="container center clearfix">
        <div class="slider-caption slider-caption-center bottommargin-lg topmargin-lg">
            <h2>Prayer Requests</h2>
        	 <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        	<a href="prayer-requests.php" data-animate="fadeIn" class="button button-3d button-large">Send a Prayer</a>
        </div>
    </div>
</div>