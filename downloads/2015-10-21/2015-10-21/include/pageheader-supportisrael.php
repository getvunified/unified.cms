<div id="slider" class="parallax clearfix dark" style="background-image: url('images/supportisrael-background.jpg'); " data-stellar-background-ratio="0.3">
    <div data-animate="fadeIn" class="container clearfix bottompadding-xlg">
        <div class="slider-caption slider-caption-center">
            <img src="images/support-israel-logo.png" alt="Support Israel" class="" style="width:190px;">
            <h3 class="topmargin-xsm">Support Israel</h3>
        </div>
    </div>
</div>