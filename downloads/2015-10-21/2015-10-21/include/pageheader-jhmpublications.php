<div id="slider" class="parallax clearfix dark" style="background-image: url('images/jhmmagazine-background.jpg'); " data-stellar-background-ratio="0.3">
    <div class="container clearfix bottompadding-xlg">
        <div class="slider-caption slider-caption-center bottommargin">
            <h3 data-animate="fadeIn">JHM Publications</h3>
            <p data-animate="fadeIn" class="lead">John Hagee Ministries endeavors to publish content that encourages believers in their walk with Christ while keeping them informed of all that is happening and available at JHM.</p>
        </div>
    </div>
</div>