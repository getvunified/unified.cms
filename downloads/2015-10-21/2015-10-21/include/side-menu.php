<div id="side-panel" class="dark">
	<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>
	<div class="h2back bottommargin"><h4 class="heading">John Hagee Ministries</h4></div>
		<div class="side-panel-wrap">
		<div class="widget clearfix">
			
			<form class="bottommargin-sm topmargin-xsm leftmargin-xsm" role="search">
                <div class="input-group">
                  	<input type="search" class="form-control" placeholder="Search JHM">
					<span class="input-group-addon"><i class="icon-search3"></i></span>
            	</div>
			</form>
			<div class="tabs clearfix" id="tab-1">

                <ul class="tab-nav clearfix">
                    <li><a href="#tabs-1">Menu</a></li>
                    <li><a href="#tabs-2">Cart</a></li>
                    <li><a href="#tabs-3">Login</a></li>							
                </ul>

                <div class="tab-container">

                    <div class="tab-content clearfix" id="tabs-1">
                        <nav class="nav-tree nobottommargin">
                            <ul class="sf-js-enabled">
                                <li><a href="index.php"><i class=""></i>Home</a></li>
                                <li><a href="#"><i class=""></i>About</a>
                                    <ul>
                                        <li><a href="whoweare.php">Who We Are</a></li>
                                        <li><a href="contact-us.php">Contact Us</a></li>
                                        <li><a href="careers.php">Careers</a></li>
                                        <li><a href="mediainquiries.php">Media Inquiries</a></li>
                                    </ul>
                                </li>
                                <li><a href="events.php"><i class=""></i>Events</a></li>
                                <li><a href="#"><i class=""></i>Encouragement</a>
                                    <ul>
                                        <li><a href="prayer-requests.php">Prayer Requests</a></li>
                                        <li><a href="daily-devotional.php">Daily Devotional</a></li>
                                        <li><a href="daily-truth.php">Daily #Truth</a></li>
										<li><a href="how-to-become-christian.php"><div>How to Become Christian</div></a></li>
                                    </ul>
                                </li>
                                <li><a href="shop.php"><i class=""></i>Shop</a></li>
                                <li><a href="#"><i class=""></i>Connect</a>
                                    <ul>
                                        <li><a href="jhm-publications.php">JHM Publications</a></li>
                                        <li><a href="social-media.php">Social Media</a></li>
                                        <li><a href="email-subscriptions.php">Email Subscriptions</a></li>
 										<li><a href="ways-to-watch.php">Ways to Watch</a></li>
                                    </ul>
                                </li>
								<li><a href="partner.php"><i class=""></i>Partner</a></li>
                                <li><a href="giving-opportunities.php"><i class=""></i>Give</a></li>
                                    <!--<ul>
                                        <li><a href="donate-to-jhm.php">Donate to JHM</a></li>
                                        <li><a href="naming-opportunities.php">Naming Opportunities</a></li>
                                        <li><a href="support-israel.php">Support Israel</a></li>
                                        <li><a href="sanctuary-of-hope.php">Sanctuary of Hope</a></li>
                                        <li><a href="salt-covenant.php">Salt Covenant</a></li>
                                        <li><a href="legacy-giving.php">Legacy Giving</a></li>
                                    </ul>
                                </li>-->
                            </ul>
                        </nav>
                    </div>
                    <div class="tab-content clearfix" id="tabs-2">
                            
                        <div class="top-cart-content">
                            <div class="top-cart-items">
                                <div class="top-cart-item clearfix">
                                    <div class="top-cart-item-image">
                                        <a href="#"><img src="images/four-moons2.jpg" alt="Four Blood Moons" /></a>
                                    </div>
                                    <div class="top-cart-item-desc">
                                        <a href="#">Four Blood Moons</a>
                                        <span class="top-cart-item-price">$19.99</span>
                                    </div>
                                </div>
                                <div class="top-cart-item clearfix">
                                    <div class="top-cart-item-image">
                                        <a href="#"><img src="images/four-moons2.jpg" alt="Four Blood Moons" /></a>
                                    </div>
                                    <div class="top-cart-item-desc">
                                        <a href="#">Four Blood Moons</a>
                                        <span class="top-cart-item-price">$19.99</span>
                                    </div>
                                </div>
                            </div>
                            <div class="top-cart-action clearfix">
                                <span class="fleft top-checkout-price">$39.98</span>
                                <button class="button button-3d button-small nomargin fright">View Cart</button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content clearfix" id="tabs-3">
                        <div class="top-link-section">
                            <form id="top-login" role="form">
                                <div class="input-group" id="top-login-username">
                                    <span class="input-group-addon"><i class="icon-user"></i></span>
                                    <input type="email" class="form-control" placeholder="Email address" required="">
                                </div>
                                <div class="input-group" id="top-login-password">
                                    <span class="input-group-addon"><i class="icon-key"></i></span>
                                    <input type="password" class="form-control" placeholder="Password" required="">
                                </div>
                                <label class="checkbox">
                                  <input type="checkbox" value="remember-me"> Remember me
                                </label>
                                <button class="button button-3d button-small btn-block" type="submit">Login</button>
 								<a href="login-register.php" class="button button-3d button-small btn-block center">Register</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>



			<div class="line topmargin-sm bottommargin-sm"></div>
			
			
			<div class="content-wrap leftmargin-xsm notoppadding">
                <div class="tcolorwhite"><strong>CONTACT US</strong></div>
                
                <div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
                    <address>
                        Headquarters:<br>
                        P.O. Box 1400<br>
                        San Antonio, Texas 78295-1400<br>
                    </address>
                    Phone: 1-800-854-9899<br>
                   Email: <a href="mailto:support@jhm.org">support@jhm.org</a>
                </div>
    
                <div class="line topmargin-sm bottommargin-sm"></div>
                
                <div class="tcolorwhite bottommargin-xsm"><strong>OUR MINISTRIES</strong></div>
                    <a href="https://www.getv.org/" target="_blank">GETV Live Stream</a><br>
                    <a href="http://www.sacornerstone.org/" target="_blank">Cornerstone Church</a><br>
                    <a href="http://www.differencemedia.org/" target="_blank">Difference Media</a><br>
                    <a href="http://www.sa-ccs.org/home/" target="_blank">Cornerstone School</a>
                </ul>
                </div>
			</div>
		</div>
	</div>
</div>
