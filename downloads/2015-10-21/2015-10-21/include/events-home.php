 <div class="section noborder nomargin nopadding">
    <div data-animate="fadeIn" class="container center clearfix">
         <div class="heading-block noborder topmargin-sm bottommargin-sm">
            <h2>JHM Events</h2>
            <p class="lead">John Hagee Ministries events that are sure to give you insight and faith. </p>
        </div>
        
        <div id="oc-portfolio" data-animate="fadeIn" class="owl-carousel portfolio-carousel">
        
         	<div class="oc-item">
				<div class="iportfolio panel-body">
                    <div class="team team-list clearfix">
                        <div class="portfolio-image">
                            <img src="images/event1.jpg" alt="Canton Junction's Every Hallelujah Concert">
                        </div>
                        <div class="team-desc">
                            <div class="topmargin-xsm">
                                <div class="team-title"><h4 class="bottommargin-xsm name tcolorgold">Canton Junction's Every Hallelujah Concert</h4></div>
                            </div>
                            <div class="panel panel-default events-meta">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Event Overview</h3>
                                </div>
                                <div class="panel-body">
                                    <ul class="iconlist nobottommargin">
                                        <li><i class="icon-calendar3"></i> <strong>Date:</strong> September 24, 2015</li>
                                        <li><i class="icon-time"></i> <strong>Start Time:</strong> 8:00am - End Time: 5:00pm</li>
                                    </ul>
                                </div>
                            </div>
                            <a href="event-description.php" class="button button-3d bottommargin-sm">View Event</a>
                        </div>
                    </div>
                </div>
			</div>

           <div class="oc-item">
				<div class="iportfolio panel-body">
                    <div class="team team-list clearfix">
                        <div class="portfolio-image">
                            <img src="images/event2.jpg" alt="Canton Junction's Every Hallelujah Concert">
                        </div>
                        <div class="team-desc">
                            <div class="topmargin-xsm">
                                <div class="team-title"><h4 class="bottommargin-xsm name tcolorgold">Join John Hagee Ministries At Sea</h4></div>
                            </div>
                            <div class="panel panel-default events-meta">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Event Overview</h3>
                                </div>
                                <div class="panel-body">
                                    <ul class="iconlist nobottommargin">
                                        <li><i class="icon-calendar3"></i> <strong>Date:</strong> September 24, 2015</li>
                                        <li><i class="icon-time"></i> <strong>Start Time:</strong> 8:00am - End Time: 5:00pm</li>
                                    </ul>
                                </div>
                            </div>
                            <a href="event-description.php" class="button button-3d bottommargin-sm">View Event</a>
                        </div>
                    </div>
                </div>
			</div>
            
			<div class="oc-item">
				<div class="iportfolio panel-body">
                    <div class="team team-list clearfix">
                        <div class="portfolio-image">
                            <img src="images/event3.jpg" alt="Canton Junction's Every Hallelujah Concert">
                        </div>
                        <div class="team-desc">
                            <div class="topmargin-xsm">
                                <div class="team-title"><h4 class="bottommargin-xsm name tcolorgold">Feast of Tabernacles/Sukkot Celebration</h4></div>
                            </div>
                            <div class="panel panel-default events-meta">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Event Overview</h3>
                                </div>
                                <div class="panel-body">
                                    <ul class="iconlist nobottommargin">
                                        <li><i class="icon-calendar3"></i> <strong>Date:</strong> September 24, 2015</li>
                                        <li><i class="icon-time"></i> <strong>Start Time:</strong> 8:00am - End Time: 5:00pm</li>
                                    </ul>
                                </div>
                            </div>
                            <a href="event-description.php" class="button button-3d bottommargin-sm">View Event</a>
                        </div>
                    </div>
                </div>
			</div>

           
        </div><!-- #portfolio end -->
      </div>
    <div class="container center clearfix topmargin-sm bottommargin">
        <a href="events.php" class="button button-3d button-large">View All Events</a>
    </div>
</div>
<script type="text/javascript">

						jQuery(document).ready(function($) {

							var ocPortfolio = $("#oc-portfolio");

							ocPortfolio.owlCarousel({
								margin: 10,
								nav: false,
								navText: ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
								autoplay: false,
								autoplayHoverPause: false,
								dots: false,
								responsive:{
									 0:{ items:1 },
									325:{ items:1 },
									479:{ items:2},
									768:{ items:3 },
									1199:{ items:3 },
								}
							});

						});

					</script>