<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Who We Are</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-whoweare.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li><a href="#">About</a></li>
                    <li class="active">Who We Are</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="section noborder notopmargin nobottommargin nobottompadding">
	 
                <div data-animate="fadeIn" class="container clearfix">
                  <!-- Post Content
                            ============================================= -->
       		
					

                            <div class="team team-list clearfix">
                                <div class="team-image">
                                    <img src="images/whoweare-john-hagee.jpg" alt="John Hagee">
                                </div>
								<div class="team-desc">
                                    <div class="col_half nobottommargin col_respon">
										<div class="team-title"><h3 class="nobottommargin name">John Hagee</h3><span>Senior Pastor</span></div>
									</div>

 									<div class="col_half fright col_last nobottommargin col_respon">
                                		<div class="si-share notopmargin">
											<div>
                                                <span class="tcolorgold">Connect:</span>
                                                <a href="https://www.facebook.com/JohnHageeMinistries" target="_blank" class="social-icon si-small si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="John Hagee Facebook">
                                                    <i class="icon-facebook"></i>
                                                    <i class="icon-facebook"></i>
                                                </a>
                                                <a href="https://twitter.com/search?q=%40PastorJohnHagee&src=typd" target="_blank" class="social-icon si-small si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="John Hagee Twitter">
                                                    <i class="icon-twitter"></i>
                                                    <i class="icon-twitter"></i>
                                                </a>
                                                <a href="https://www.pinterest.com/hageeministries/" target="_blank" class="social-icon si-small si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="John Hagee Pinterest">
                                                    <i class="icon-pinterest"></i>
                                                    <i class="icon-pinterest"></i>
                                                </a>
                                                <a href="https://www.youtube.com/user/HageeMinistries" target="_blank" class="social-icon si-small si-borderless si-youtube" data-toggle="tooltip" data-placement="top" title="John Hagee YouTube">
                                                    <i class="icon-youtube-play"></i>
                                                    <i class="icon-youtube-play"></i>
                                                </a>
                                                <a href="https://instagram.com/HageeMinistries/" target="_blank" class="social-icon si-small si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="John Hagee Instagram">
                                                    <i class="icon-instagram"></i>
                                                    <i class="icon-instagram"></i>
                                                </a>
											</div>
                                       </div>
                                    </div>
									

									<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                                    <div class="team-content">
                                        <p><strong>Pastor John C. Hagee is the founder and Senior Pastor</strong> of Cornerstone Church in San Antonio, Texas, a non-denominational evangelical church with more than 20,000 active members. Pastor Hagee has served the Lord in the gospel ministry for over 57 years.</p>
                                    </div>

									<div class="toggle toggle-bg topmargin-sm">
										<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Read More</div>
										<div class="togglec"><p><strong>Pastor Hagee is founder and President of John Hagee Ministries,</strong> which telecasts his national radio and television teachings throughout America and the nations of the world, and is now seen worldwide twenty-four hours a day, seven days a week on GETV.org.</p>

<p><strong>Pastor John Hagee</strong> received a Bachelor of Science degree from Southwestern Assemblies of God University in Waxahachie, Texas and a second Bachelor of Science degree from Trinity University in San Antonio, Texas. He earned his Master’s Degree in Educational Administration from the University of North Texas in Denton, Texas.</p>

<p>Pastor Hagee has received Honorary Doctorates from Oral Roberts University, Canada Christian College, and Netanya Academic College of Israel.</p>

<p><strong>He is the author of 34 major books,</strong> many of which were on the New York Times Best Seller’s List including “Four Blood Moons”, which is now featured as a successful docudrama film. His most recent book, New York Times Best Seller “The Three Heavens” released last spring by Worthy Publishing. Among his other works are commentary study Bibles, novels and numerous devotionals.</p>

<p><strong>Over the years, John Hagee Ministries</strong> has given more than $85 million toward humanitarian causes in Israel. In 2006, Dr. Hagee founded, and is the National Chairman of Christians United For Israel, a grass roots national association through which every pro—Israel Christian ministry, para-church organization, or individual in America can speak and act with one voice in support of Israel and the Jewish people.</p>

<p><strong>Dr. Hagee</strong> gains support for this worthy cause by conducting A Night to Honor Israel in every major city in America and by organizing an annual Washington D.C.-Israel Summit where thousands of CUFI delegates from every state in the union have the opportunity to meet the members of congress face to face on behalf of Israel.</p>

<p><strong>Christians United for Israel</strong> has grown to become the largest Christian pro-Israel organization in the United States – with over 2.1 million members-- and one of the leading Christian grassroots movements in the world. Christians United for Israel has recently opened offices in the United Kingdom and Canada to combat anti-Semitism.</p>

<p><strong>Pastor Hagee and his wife Diana are blessed with five children and thirteen grandchildren.</strong></p></div>
									</div>
                                </div>
                            </div>
                      	<div class="line topmargin-xsm bottommargin-sm"></div>
				
				</div>
			

					<div class="container clearfix">
                  <!-- Post Content
                            ============================================= -->
       		
					

                            <div class="team team-list clearfix">
                                <div class="team-image">
                                    <img src="images/whoweare-matthew-hagee.jpg" alt="Matthew Hagee">
                                </div>
								<div class="team-desc">
                                    <div class="col_half nobottommargin col_respon">
										<div class="team-title"><h3 class="nobottommargin name">Matthew Hagee</h3><span>Executive Pastor</span></div>
									</div>
 									<div class="col_half fright col_last nobottommargin col_respon">
                                		<div class="si-share notopmargin">
											<div>
                                                <span class="tcolorgold">Connect:</span>
                                                <a href="https://www.facebook.com/pastormatthewandkendalhagee/" target="_blank" class="social-icon si-small si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Matthew Hagee Facebook">
                                                    <i class="icon-facebook"></i>
                                                    <i class="icon-facebook"></i>
                                                </a>
                                                <a href="https://twitter.com/search?q=%40PastorMattHagee&src=typd" target="_blank" class="social-icon si-small si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="Matthew Hagee Twitter">
                                                    <i class="icon-twitter"></i>
                                                    <i class="icon-twitter"></i>
                                                </a>
                                                <a href="https://www.pinterest.com/hageeministries/" target="_blank" class="social-icon si-small si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Matthew Hagee Pinterest">
                                                    <i class="icon-pinterest"></i>
                                                    <i class="icon-pinterest"></i>
                                                </a>
                                                <a href="https://instagram.com/MattHagee/" target="_blank" class="social-icon si-small si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="Matthew Hagee Instagram">
                                                    <i class="icon-instagram"></i>
                                                    <i class="icon-instagram"></i>
                                                </a>
                                            </div>
                                         </div>
                                     </div>
                                            

									<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                                    <div class="team-content">
                                        <p><strong>Pastor Matthew Charles Hagee is the sixth generation in the Hagee family to carry the mantel of Gospel ministry.</strong> He serves as the Executive Pastor of the 20,000 member Cornerstone Church in San Antonio, Texas where he partners with his father, founder Pastor John Hagee.</p>
										
                                    </div>

									<div class="toggle toggle-bg topmargin-sm">
										<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Read More</div>
										<div class="togglec"><p><strong>Pastor Matthew Hagee’s</strong> teachings and faith-based talk shows, The Difference and Hagee Hotline, are telecast throughout the world on radio and television, as well as twenty-four hours a day, seven days a week at GETV.org through John Hagee Ministries. He is fervently committed to preaching all the Gospel to all the World and to all generations.</p> 

<p><strong>Pastor Matthew is an accomplished vocalist</strong> and founder of the Dove-Award winning gospel quartet, Canton Junction. His love for music led to the creation of Difference Media, a music label with the goal of reaching the lost and spreading God’s truth through song.</p>

<p><strong>A graduate of Oral Roberts University School of Business,</strong> he is the author of “Shaken, Not Shattered” and "Response-Able", published by Strang Communications.</p> 

<p><strong>Pastor Matthew and his wife Kendal are blessed with four children, Hannah Rose, John William, Joel Charles and Madison Katherine.</strong></p>

<p><strong>As their family and ministry grow,</strong> Pastor Matthew and Kendal have a desire equip the church of tomorrow, to be the distinguished treasure God designed them to be.  Together they seek to fulfill their divine destiny with the passion and purpose that can only come from the power of family tradition and the anointed call of God on their lives.</p>

										</div>
									</div>
                                </div>
                            </div>
                      	<div class="line topmargin-xsm bottommargin-sm"></div>
				
				</div>


					<div class="container clearfix">
                  <!-- Post Content
                            ============================================= -->
       		
					

                            <div class="team team-list clearfix">
                                <div class="team-image">
                                    <img src="images/whoweare-diana-hagee.jpg" alt="Diana Castro Hagee">
                                </div>
								<div class="team-desc">
                                    <div class="col_half nobottommargin col_respon">
										<div class="team-title"><h3 class="nobottommargin name">Diana Castro Hagee</h3></div>
									</div>
 									<div class="col_half fright col_last nobottommargin col_respon">
                                		<div class="si-share notopmargin">
											<div>
                                                <span class="tcolorgold">Connect:</span>
                                                <a href="https://www.facebook.com/JohnHageeMinistries" target="_blank" class="social-icon si-small si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Diana Hagee Facebook">
                                                    <i class="icon-facebook"></i>
                                                    <i class="icon-facebook"></i>
                                                </a>
                                                <a href="https://www.pinterest.com/hageeministries/" target="_blank" class="social-icon si-small si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Diana Hagee Pinterest">
                                                    <i class="icon-pinterest"></i>
                                                    <i class="icon-pinterest"></i>
                                                </a>
                                                <a href="https://www.youtube.com/user/HageeMinistries" target="_blank" class="social-icon si-small si-borderless si-youtube" data-toggle="tooltip" data-placement="top" title="Diana Hagee YouTube">
                                                    <i class="icon-youtube-play"></i>
                                                    <i class="icon-youtube-play"></i>
                                                </a>
                                                <a href="https://instagram.com/dianahagee/" target="_blank" class="social-icon si-small si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="Diana Hagee Instagram">
                                                    <i class="icon-instagram"></i>
                                                    <i class="icon-instagram"></i>
                                                </a>
                                            </div>
                                       </div>
                                    </div>
                                                                                

									<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                                    <div class="team-content">
                                        <p><strong>Diana Hagee is the wife of Pastor John Hagee, founder and senior pastor of Cornerstone Church in San Antonio, Texas.</strong> She coordinates all special events for John Hagee Ministries, Cornerstone Church, Cornerstone Christian Schools, Christians United for Israel and leads the Women's Ministries at Cornerstone Church.</p>
										
                                    </div>

									<div class="toggle toggle-bg topmargin-sm">
										<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Read More</div>
										<div class="togglec"><p><strong>Diana is the author of the best-selling book The King's Daughter,</strong> which was awarded the Retailers Choice Award, The King’s Daughter Workbook, and Not by Bread Alone, a cookbook encouraging creative ministry through food as well as a book she co-authored with her husband entitled, What Every Man Wants in a Woman—What Every Woman Wants in a Man. Her most recent release is Ruth, the Romance of Redemption – A Love Story.</p>

<p><strong>She is founder and director of the King’s Daughter,</strong> Becoming a Woman of God National Conferences. She was presented the prestigious Lion of Judah award by the Jewish Federation of Greater Houston for the long-standing work she and Pastor Hagee do on behalf of Israel and the Jewish people. Diana is very active with Christians United for Israel, which Pastor Hagee founded in 2006.</p>

<p><strong>Diana holds a Bachelor’s Degree in Science from Trinity University in San Antonio, Texas. She and Pastor John Hagee have five children and twelve grandchildren.</strong></p>
										</div>
									</div>
                                </div>
                            </div>
                      	<div class="line topmargin-xsm bottommargin-sm"></div>
				
				</div>



					<div class="container clearfix">
                  <!-- Post Content
                            ============================================= -->
       		
					

                            <div class="team team-list clearfix bottommargin-sm">
                                <div class="team-image">
                                    <img src="images/whoweare-kendal-hagee.jpg" alt="Kendal Hagee">
                                </div>
								<div class="team-desc">
                                    <div class="col_half nobottommargin col_respon">
										<div class="team-title"><h3 class="nobottommargin name">Kendal Hagee</h3></div>
									</div>
 									<div class="col_half fright col_last nobottommargin col_respon">
                                		<div class="si-share notopmargin">
											<div>
                                                <span class="tcolorgold">Connect:</span>
                                                <a href="https://www.facebook.com/PastorMatthewAndKendalHagee" target="_blank" class="social-icon si-small si-borderless si-facebook" data-toggle="tooltip" data-placement="top" title="Kendal Hagee Facebook">
                                                    <i class="icon-facebook"></i>
                                                    <i class="icon-facebook"></i>
                                                </a>
                                                <a href="https://twitter.com/search?q=%40kendalhagee&src=typd" target="_blank" class="social-icon si-small si-borderless si-twitter" data-toggle="tooltip" data-placement="top" title="Kendal Hagee Twitter">
                                                    <i class="icon-twitter"></i>
                                                    <i class="icon-twitter"></i>
                                                </a>
                                                <a href="https://www.pinterest.com/hageeministries/" target="_blank" class="social-icon si-small si-borderless si-pinterest" data-toggle="tooltip" data-placement="top" title="Kendal Hagee Pinterest">
                                                    <i class="icon-pinterest"></i>
                                                    <i class="icon-pinterest"></i>
                                                </a>
                                                <a href="https://www.youtube.com/user/HageeMinistries" target="_blank" class="social-icon si-small si-borderless si-youtube" data-toggle="tooltip" data-placement="top" title="Kendal Hagee YouTube">
                                                    <i class="icon-youtube-play"></i>
                                                    <i class="icon-youtube-play"></i>
                                                </a>
                                                <a href="https://instagram.com/kendalehagee/" target="_blank" class="social-icon si-small si-borderless si-instagram" data-toggle="tooltip" data-placement="top" title="Kendal Hagee Instagram">
                                                    <i class="icon-instagram"></i>
                                                    <i class="icon-instagram"></i>
                                                </a>
											</div>
                                       </div>
                                    </div>

									<div class="line-short topmargin-xsm bottommargin-xsm"></div>
                                    <div class="team-content">
                                        <p><strong>Kendal Hagee is the wife of Pastor Matthew Hagee,</strong> Executive Pastor of Cornerstone Church in San Antonio, Texas, a non-denominational evangelical church with more than 20,000 active members.</p>
										
                                    </div>

									<div class="toggle toggle-bg topmargin-sm">
										<div class="togglet tcolorgold"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Read More</div>
										<div class="togglec"><p><strong>Kendal assists in coordinating and hosting many special events for both Cornerstone Church and Cornerstone Christian Schools.</strong> A gifted speaker, she also participates with her husband at prayer meetings on and off the church campus.</p>

<p><strong>Kendal diligently serves alongside Pastor Matthew</strong> to share the Word of God across the nations of the world through various humanitarian projects, relief efforts, community service initiatives and Gospel Rallies.</p>

<p><strong>Kendal co-hosts The Difference, a popular faith-based talk show,</strong> with her husband that is broadcast nationally and can be found online Wednesdays at 8PM CT on GETV.org. The inspiring topics and fascinating guests have quickly become a fan-favorite.</p>

<p><strong>Kendal holds a Bachelor’s of Science Degree in Nursing</strong> from the University of the Incarnate Word in San Antonio, Texas, and is a Registered Nurse. A loving wife and mother, Kendal and Pastor Matthew have four children: Hannah Rose, John William, Joel Charles and Madison Katherine.</p>
										</div>
									</div>
                                </div>
                            </div>
                      	
				
				</div>

			<!-- JHM Mission
            ============================================= -->
            <?php include "include/jhm-mission-whoweare.php";?>

            <div class="clear"></div>

			<div data-animate="fadeIn" class="container clearfix">
				<div class="heading-block noborder topmargin bottommargin-sm">
					<h2 class="center">Our Beliefs</h2>
				</div>
                <div class="col_one_fourth bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon1.png" alt="The Lord Jesus Christ">
                        </div>
                        <h3 class="tcolorgold">The Lord Jesus Christ</h3>
                        <p>We believe in the deity of Jesus Christ as the only begotten Son of God.  We believe in His substitutionary death for all men, His resurrection, and His eventual return to judge the world.</p>
                    </div>
					<div class="line-mini"></div>
                </div>
 				<div class="col_one_fourth bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon2.png" alt="The Lord Jesus Christ">
                        </div>
                        <h3 class="tcolorgold">Salvation</h3>
                        <p> We believe all men are born with a sinful nature and that the work of the Cross was to redeem man from the power of sin.  We believe that this salvation is available to all who will receive it.</p>
                    </div>
					<div class="line-mini"></div>
                </div>
 				<div class="col_one_fourth bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon3.png" alt="The Lord Jesus Christ">
                        </div>
                        <h3 class="tcolorgold">The Holy Spirit</h3>
                        <p>We believe in the existence of the Holy Spirit as the third person of the Trinity and in His interaction with man.  We believe in the baptism of the Holy Spirit as manifested by the fruit and the gifts of the Spirit.</p>
                    </div>
					<div class="line-mini"></div>
                </div>
 				<div class="col_one_fourth col_last bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon4.png" alt="The Lord Jesus Christ">
                        </div>
                        <h3 class="tcolorgold">The Sacred Scripture</h3>
                        <p>We believe in the scripture as the inspired Word of God and that it is the complete revelation of God's will for mankind.  We believe in the absolute authority of the scripture to govern the affairs of men.</p>
                    </div>
					<div class="line-mini"></div>
                </div>
			</div>
			
			<div class="clear"></div>

			<div class="container clearfix">
                <div class="col_one_fourth bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon5.png" alt="Stewardship">
                        </div>
                        <h3 class="tcolorgold">Stewardship</h3>
                        <p>We believe that every man is the steward of his life and resources which ultimately belong to God.  We believe that tithing is a measure of obedience to the scriptural principles of stewardship.</p>
                    </div>
					<div class="line-mini"></div>
                </div>
 				<div class="col_one_fourth bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon6.png" alt="The Church">
                        </div>
                        <h3 class="tcolorgold">The Church</h3>
                        <p>We believe in the Church as the eternal and universal Body of Christ consisting of all those who have accepted the work of the atonement. We believe in the need for a local assembly of believers for the purpose of evangelism and edification.</p>
                    </div>
					<div class="line-mini"></div>
                </div>
 				<div class="col_one_fourth bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon7.png" alt="Prayer and Praise">
                        </div>
                        <h3 class="tcolorgold">Prayer and Praise</h3>
                        <p>We believe in the worship of the Lord through singing, clapping, and the lifting of hands. We believe in the authority of the believer to ask freely of the Lord for his needs.</p>
                    </div>
					<div class="line-mini"></div>
                </div>
 				<div class="col_one_fourth col_last bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon8.png" alt="Body Ministry">
                        </div>
                        <h3 class="tcolorgold">Body Ministry</h3>
                        <p>We believe in the ministry of the Holy Spirit to the Church body through the anointing of oil by the elders of the church.</p>
                    </div>
					<div class="line-mini"></div>
                </div>
			</div>

			<div class="clear"></div>

			<div class="container clearfix">
                <div class="col_one_fourth bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon9.png" alt="Evangelism">
                        </div>
                        <h3 class="tcolorgold">Evangelism</h3>
                        <p>We believe that evangelism is the obligation of every follower of Jesus Christ. The Lord commands us to go out and make disciples of all the earth. We believe that each person is first responsible to evangelism in their own family as the Holy Spirit leads them and gives them the ability.</p>
                    </div>
					<div class="line-mini"></div>
                </div>
 				<div class="col_one_fourth bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon10.png" alt="Water Baptism">
                        </div>
                        <h3 class="tcolorgold">Water Baptism</h3>
                        <p>We believe in the ordinance of water baptism by immersion in obedience to the Word of God. All those who have accepted Jesus Christ as their personal savior should be baptized in water as a public profession of their faith in Christ and to experience what the Bible calls the "circumcision of the Spirit."</p>
                    </div>
					<div class="line-mini"></div>
                </div>
 				<div class="col_one_fourth bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon11.png" alt="Commitment to Israel">
                        </div>
                        <h3 class="tcolorgold">Commitment to Israel</h3>
                        <p>We believe in the promise of Genesis 12:3 regarding the Jewish people and the nation of Israel. We believe that this is an eternal covenant between God and the seed of Abraham to which God is faithful.</p>
                    </div>
					<div class="line-mini"></div>
                </div>
 				<div class="col_one_fourth col_last bottommargin-sm col_respon">
                    <div class="feature-box fbox-center fbox-plain noborder">
                        <div class="fbox-icon">
                            <img src="images/whoweare-icon12.png" alt="Priesthood of the Believer">
                        </div>
                        <h3 class="tcolorgold">Priesthood of the Believer</h3>
                        <p>We believe that every believer has a unique relationship to the Lord. As His children, every Christian has immediate access to the throne of Grace and the ability to manifest the power of the Lord Jesus Christ in ministry.</p>
                    </div>
					<div class="line-mini"></div>
                </div>
			</div>



<div class="clear"></div>

			<!-- JHM Connect
            ============================================= -->
            <?php include "include/jhm-connect.php";?>


</div>

            <div class="clear"></div>
    
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>