<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Email: Stay Informed</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>
       
       <!-- Page Header
		============================================= -->
        <?php include "include/pageheader-email-stayinformed.php";?>


		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">Connect</a></li>
                    <li class="active">Email: Stay Informed</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
           <div class="content-wrap noborder topmargin nobottommargin nopadding">

				<div data-animate="fadeIn" class="container bottommargin clearfix">

                    
					<!-- Email Signup Form
					============================================= -->
					<div class="nobottommargin">

						<p>Short text description asking you to fill the form out below if you'd like to receive emails from any of the stuff. Maybe explain how there's a description of each subject below.</p>

						<h3><span><span>*Denotes required fields</span></span></h3>
						<div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>

						<form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">

							<div class="form-process"></div>

                                <div class="col_one_third topmargin-xsm">
                                    
                                    <div class="fright">
                                        <ul class="list-unstyled">
                                            <li><input type="checkbox" id="template-contactform-name" name="template-contactform-name" value="" class="checkbox-inline" /> <label for="template-contactform-name">A Moment with Matt </label></li>
                                    
                                            <li><input type="checkbox" id="template-contactform-name" name="template-contactform-name" value="" class="checkbox-inline" /> <label for="template-contactform-name">Daily Blessings and Readings </label></li>
                                    
                                            <li><input type="checkbox" id="template-contactform-name" name="template-contactform-name" value="" class="checkbox-inline" /> <label for="template-contactform-name">Event Notifications </label></li>
                                    
                                            <li><input type="checkbox" id="template-contactform-name" name="template-contactform-name" value="" class="checkbox-inline" /> <label for="template-contactform-name">Weekly Update </label></li>
                                        </ul>
                                    </div>
                               	</div>
							


							<div class="col_two_third col_last">
								<label for="template-contactform-name">Full Name <small>*</small></label>
								<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />
							<div class="topmargin-xsm">
								<label for="template-contactform-email">Email <small>*</small></label>
								<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
							</div>
								<button class="button button-3d topmargin-sm" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Subscribe</button>
							</div>


							<div class="clear"></div>
						
						</form>

						<script type="text/javascript">

							$("#template-contactform").validate({
								submitHandler: function(form) {
									$('.form-process').fadeIn();
									$(form).ajaxSubmit({
										target: '#contact-form-result',
										success: function() {
											$('.form-process').fadeOut();
											$(form).find('.sm-form-control').val('');
											$('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
											SEMICOLON.widget.notifications($('#contact-form-result'));
										}
									});
								}
							});

						</script>

					</div><!-- .email signup form end -->
				</div>

			</div>

            <div class="clear"></div>
    
    	</section>



		<section id="content">
        
            <div class="section noborder notopmargin nobottommargin nopadding">

	 			<div class="heading-block noborder topmargin bottommargin center">
					<h2>JHM Subscriptions</h2>
					<p>Use the form above to subscribe to the content displayed below.</p>
				</div>

                <div data-animate="fadeIn" class="container clearfix border-full toppadding-sm bottompadding-sm bottommargin">
                  <!-- Post Content
                            ============================================= -->

                    <div class="team team-list clearfix">
                        <div class="team-image">
                            <img src="images/emailsub-blog.jpg" alt="A Moment With Matt">
                        </div>
                        <div class="team-desc">
                            <div class="col_two_third nobottommargin col_respon">
                                <div class="team-title"><h3 class="nobottommargin name">A Moment With Matt</h3></div>
                            </div>
                            <div class="col_one_third col_last nobottommargin col_respon fright">
                               <div class="tcolorgold"><i class="icon-calendar3"></i> Once a week</div>
                            </div>

                            <div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="team-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                
                            </div>
                        </div>
                    </div>
                	
				</div>

				<div data-animate="fadeIn" class="container clearfix border-full toppadding-sm bottompadding-sm bottommargin">
                  <!-- Post Content
                            ============================================= -->

                    <div data-animate="fadeIn" class="team team-list clearfix">
                        <div class="team-image">
                            <img src="images/emailsub-dailyblessings.jpg" alt="Daily Blessings">
                        </div>
                        <div class="team-desc">
                            <div class="col_two_third nobottommargin col_respon">
                                <div class="team-title"><h3 class="nobottommargin name">Daily Blessings and Readings</h3></div>
                            </div>
                            <div class="col_one_third col_last nobottommargin col_respon fright">
                                <div class="tcolorgold"><i class="icon-calendar3"></i> Everyday</div>
                            </div>

                            <div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="team-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                
                            </div>
                        </div>
                    </div>
                	
				</div>

				<div data-animate="fadeIn" class="container clearfix border-full toppadding-sm bottompadding-sm bottommargin">
                  <!-- Post Content
                            ============================================= -->

                    <div data-animate="fadeIn" class="team team-list clearfix">
                        <div class="team-image">
                            <img src="images/emailsub-weeklyupdate.jpg" alt="Weekly Update">
                        </div>
                        <div class="team-desc">
                            <div class="col_two_third nobottommargin col_respon">
                                <div class="team-title"><h3 class="nobottommargin name">Weekly Update</h3></div>
                            </div>
                            <div class="col_one_third col_last nobottommargin col_respon fright">
                                <div class="tcolorgold"><i class="icon-calendar3"></i> Once a week</div>
                            </div> 

                            <div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="team-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                
                            </div>
                        </div>
                    </div>
                	
				</div>

				<div data-animate="fadeIn" class="container clearfix border-full toppadding-sm bottompadding-sm bottommargin">
                  <!-- Post Content
                            ============================================= -->

                    <div data-animate="fadeIn" class="team team-list clearfix">
                        <div class="team-image">
                            <img src="images/emailsub-eventnotification.jpg" alt="Event Notofication">
                        </div>
                        <div class="team-desc">
                            <div class="col_two_third nobottommargin col_respon">
                                <div class="team-title"><h3 class="nobottommargin name">Event Notification</h3></div>
                            </div>
                            <div class="col_one_third col_last nobottommargin col_respon fright">
                                <div class="tcolorgold"><i class="icon-calendar3"></i> Day event is posted</div>
                            </div>

                            <div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="team-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                
                            </div>
                        </div>
                    </div>
                	
				</div>
			</div>
	</section><!-- #content end -->
			

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>