<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!-- Head
	============================================= -->
<head>

		<!-- External code
			============================================= -->
		<?php include "include/external-code.php";?>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="js/jquery.gmap.js"></script>


    <!-- Document Title
    ============================================= -->
	<title>John Hagee Ministries | Partner</title>

</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

	
 		<!-- Side Menu
			============================================= -->
		<?php include "include/side-menu.php";?>

	

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar 
        ============================================= -->
        <div id="top-bar" class="hidden-xs">

            <div class="container-fluid clearfix">

                <div class="col_half nomargin fright">

        <!-- Top Links
		============================================= -->
        <?php include "include/top-links.php";?>

                </div>

                
            </div>

        </div> <!-- #top-bar end --> 
        
        <!-- Header
		============================================= -->
        <?php include "include/header.php";?>


 		<!-- Page Header
		============================================= -->
        <?php include "include/pageheader-partner.php";?>
       

		<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <ol class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2"></i></a></li>
                    <li class="active">Partner</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
        
            <div class="section noborder notopmargin nobottommargin" style="background-color:#e2e2e2;">
	 
                <div data-animate="fadeIn" class="container clearfix">

					<div class="heading-block noborder center">
                        <h2>Partnership</h2>
                        <p>Through our partnership programs you can become a partner with JHM</p>
                    </div>
                  
				<!-- Post Content
                ============================================= -->
       		
                    <div class="team team-list border-full topbottompadding pad15 clearfix">
                        <div class="team-image">
                            <img src="images/partnership-img1.jpg" alt="Salt Covenant Partnership">
                        </div>
                        <div class="team-desc">
                            <div class="col_two_third nobottommargin col_respon">
                                <div class="team-title"><h3 class="nobottommargin name">Salt Covenant Partnership <span>Monthly Donation</span></div>
                            </div>
                            

                            <div class="line-short topmargin-xsm bottommargin-xsm"></div>
                            <div class="team-content">
                                <p>"Season all your grain offerings with salt. Do not leave the salt of the covenant of your God out of your grain offerings; add salt to all your offerings...</p>
								<a href="salt-covenant-partnership.php" class="button button-3d bottommargin-sm">Learn More</a>
                               
                            </div>
                        </div>
                    </div>
				
				</div>

    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    
            <!-- Footer
            ============================================= -->
            <?php include "include/footer.php";?>
            
	</div>
	
</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>


</body>
</html>