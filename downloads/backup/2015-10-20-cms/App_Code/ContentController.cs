﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using umbraco;
using Umbraco.Web.WebApi;
using Newtonsoft.Json;
using System.Web.Http;
using System.Net.Http.Formatting;
using System.Web;
public class ContentController : UmbracoApiController
{
    string domain = "http://" + HttpContext.Current.Request.Url.Host;

    [HttpGet]
    public string date()
    {
        return DateTime.Now.ToString();
    }

    public Biographies GetContents(string id)
    {
        GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());

        Biographies oBiographies = new Biographies();
        try
        {
            umbraco.NodeFactory.Node node = uQuery.GetNodesByName(id).FirstOrDefault();
            if (node != null)
            {
                oBiographies.items = new List<BiographieItem>();
                var page = Umbraco.Content(node.Id);
                oBiographies.id = page.Id;
                oBiographies.date = page.UpdateDate.ToString("yyyy-MM-dd");
                foreach (var child in page.Children)
                {
                    oBiographies.items.Add(new BiographieItem()
                    {
                        id = child.Id,
                        type = child.DocumentTypeAlias,
                        typeName = child.ItemType.ToString(),
                        title = child.GetPropertyValue("Name"),
                        subtitle = child.GetPropertyValue("Title"),
                        text = child.GetPropertyValue("Biography").ToString()

                    });
                }
            }
        }
        catch (Exception Ex)
        {
            //oBiographies.ex = Ex.Message;
        }

        return oBiographies;
    }

    [HttpGet]
    public Biographies Biographies()
    {
        GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());

        Biographies oBiographies = new Biographies();
        try
        {
            umbraco.NodeFactory.Node node = uQuery.GetNodesByName("Biographies").FirstOrDefault();
            if (node != null)
            {
                oBiographies.items = new List<BiographieItem>();
                var page = Umbraco.Content(node.Id);
                oBiographies.id = page.Id;
                oBiographies.date = page.UpdateDate.ToString("yyyy-MM-dd");
                foreach (var child in page.Children)
                {
                    oBiographies.items.Add(new BiographieItem()
                    {
                        id = child.Id,
                        type = child.DocumentTypeAlias,
                        typeName = child.ItemType.ToString(),
                        title = child.GetPropertyValue("Name"),
                        subtitle = child.GetPropertyValue("Title"),
                        text = child.GetPropertyValue("Biography").ToString()

                    });
                }
            }
        }
        catch (Exception Ex)
        {
            //oBiographies.ex = Ex.Message;
        }

        return oBiographies;
    }

    [HttpGet]
    public site JHM()
    {
        GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());

        site osite = new site();
        Sliders oSliders = new Sliders();
        DailyTruth oDailyTruth = new DailyTruth();
        try
        {
            umbraco.NodeFactory.Node node = uQuery.GetNodesByName("JHM.org").FirstOrDefault();
            if (node != null)
            {
                var page = Umbraco.Content(node.Id);
                foreach (var cNode in page.Children)
                {
                    if (cNode.Name == "Slider Collection")
                    {
                        oSliders.items = new List<SliderItem>();
                        oSliders.id = cNode.Id;
                        oSliders.date = cNode.UpdateDate.ToString("yyyy-MM-dd");
                        foreach (var child in cNode.Children)
                        {
                            string imgUrl = "";
                            if (child.GetPropertyValue("sliderImageCropper") != null)
                            {
                                var m = Umbraco.Media(child.GetPropertyValue("sliderImageCropper"));
                                imgUrl = domain + m.Url;
                            }
                            oSliders.items.Add(new SliderItem()
                            {
                                id = child.Id,
                                title = child.GetPropertyValue("FirstLineofText"),
                                subtitle = child.GetPropertyValue("SecondLineofText") + child.GetPropertyValue("ThirdLineofText"),
                                image_url = imgUrl,
                                link = child.GetPropertyValue("ButtonUrl"),
                                uri_link = umbraco.library.NiceUrl(child.Id)
                            });
                        }
                    }
                    else if (cNode.Name == "Daily Truths")
                    {
                        oDailyTruth.items = new List<DailyTruthItem>();
                        oDailyTruth.id = cNode.Id;
                        oDailyTruth.date = cNode.UpdateDate.ToString("yyyy-MM-dd");
                        foreach (var child in cNode.Children)
                        {
                            oDailyTruth.items.Add(new DailyTruthItem()
                            {
                                id = child.Id,
                                type = child.DocumentTypeAlias,
                                typeName = child.ItemType.ToString(),
                                title = child.Name
                            });
                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            oDailyTruth.ex = Ex.Message;
        }

        osite.slider = oSliders;
        osite.daily_truth = oDailyTruth;
        return osite;
    }

    [HttpGet]
    public site SACornerstone()
    {
        GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());

        site osite = new site();
        Sliders oSliders = new Sliders();
        DailyTruth oDailyTruth = new DailyTruth();
        try
        {
            umbraco.NodeFactory.Node node = uQuery.GetNodesByName("SACornerstone.org").FirstOrDefault();
            if (node != null)
            {
                var page = Umbraco.Content(node.Id);
                foreach (var cNode in page.Children)
                {
                    if (cNode.Name == "Slider Collection")
                    {
                        oSliders.items = new List<SliderItem>();
                        oSliders.id = cNode.Id;
                        oSliders.date = cNode.UpdateDate.ToString("yyyy-MM-dd");
                        foreach (var child in cNode.Children)
                        {
                            var m = Umbraco.Media(child.GetPropertyValue("Image"));
                            oSliders.items.Add(new SliderItem()
                            {
                                id = child.Id,
                                title = child.GetPropertyValue("FirstLineofText"),
                                subtitle = child.GetPropertyValue("SecondLineofText") + child.GetPropertyValue("ThirdLineofText"),
                                image_url = domain + m.Url,
                                link = child.GetPropertyValue("ButtonUrl"),
                                uri_link = umbraco.library.NiceUrl(child.Id)
                            });
                        }
                    }
                    else if (cNode.Name == "Daily Truths")
                    {
                        oDailyTruth.items = new List<DailyTruthItem>();
                        oDailyTruth.id = cNode.Id;
                        oDailyTruth.date = cNode.UpdateDate.ToString("yyyy-MM-dd");
                        foreach (var child in cNode.Children)
                        {
                            oDailyTruth.items.Add(new DailyTruthItem()
                            {
                                id = child.Id,
                                type = child.DocumentTypeAlias,
                                typeName = child.ItemType.ToString(),
                                title = child.Name
                            });
                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            oDailyTruth.ex = Ex.Message;
        }

        osite.slider = oSliders;
        osite.daily_truth = oDailyTruth;
        return osite;
    }

    [HttpGet]
    public PrayerWall PrayerWall()
    {
        GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());

        PrayerWall oPrayerWall = new PrayerWall();
        try
        {
            oPrayerWall.id = DateTime.Now.Millisecond;
            oPrayerWall.date = DateTime.Now.ToString("yyyy-MM-dd");
            oPrayerWall.text = "Please pray for my family";
            oPrayerWall.user = new PrayerWallUser()
            {
                id = DateTime.Now.Millisecond + DateTime.Now.Minute,
                username = "mknippen",
                location = "Chicago, IL",
                email = "matthew@zwiffer.com"
            };
        }
        catch (Exception Ex)
        {
            //oBiographies.ex = Ex.Message;
        }

        return oPrayerWall;
    }

    [HttpPost]
    public PrayerWall PrayerWall(PrayerWall oPrayerWall)
    {
        GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());


        return oPrayerWall;
    }

    [HttpGet]
    public List<Video> Videos()
    {
        GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());

        List<Video> VideoList = new List<Video>();
        try
        {
            VideoList.Add(new Video()
            {
                id = DateTime.Now.Millisecond,
                date = DateTime.Now.ToString("yyyy-MM-dd"),
                category = "classic",
                thumbnail_url = "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
                title = "Hagee Hotline - September 1st, 2015",
                description = "Pastor Matthew Hagee conducts unedited commentary on the state of our nation and current geopolitical events from a Scriptural perspective.",
                video_url = "http://www.sample-videos.com/video/mp4/360/big_buck_bunny_360p_1mb.mp4",
                is_downloadable = true,
                login_required = true
            });
        }
        catch (Exception Ex)
        {
            //oBiographies.ex = Ex.Message;
        }

        return VideoList;
    }


    [HttpGet]
    public List<Event> Events()
    {
        GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());

        List<Event> EventList = new List<Event>();
        try
        {
            EventList.Add(new Event()
            {
                id = DateTime.Now.Millisecond,
                start_date = DateTime.Now.ToString("yyyy-MM-dd"),
                end_date = DateTime.Now.AddDays(20).ToString("yyyy-MM-dd"),
                title = "Semi-Annual Food Drive",
                description = "Join us to help bring food to those in need.",
                imageURL = "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
                url = "http://www.google.com"
            });
        }
        catch (Exception Ex)
        {
            //oBiographies.ex = Ex.Message;
        }

        return EventList;
    }

    [HttpGet]
    public DailyTruth DailyTruth()
    {
        GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());

        DailyTruth oDailyTruth = new DailyTruth();

        try
        {
            oDailyTruth.items = new List<DailyTruthItem>();
            oDailyTruth.id = DateTime.Now.Millisecond;
            oDailyTruth.date = DateTime.Now.ToString("yyyy-MM-dd");

            oDailyTruth.items.Add(new DailyTruthItem()
            {
                id = DateTime.Now.Millisecond + DateTime.Now.Second,
                type = "video",
                typeName = "Blog",
                title = "Held (Official Music Video)",
                subtitle = "Natalie Grant",
                text = "text blob goes here",
                media_url = "http://www.sample-videos.com/video/mp4/360/big_buck_bunny_360p_1mb.mp4",
                thumbnail_url = "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
            });

        }
        catch (Exception Ex)
        {
            //oBiographies.ex = Ex.Message;
        }

        return oDailyTruth;
    }

}

public class site
{
    public Sliders slider { get; set; }
    public DailyTruth daily_truth { get; set; }
}

public class Biographies
{
    public int id { get; set; }
    public string date { get; set; }
    public List<BiographieItem> items { get; set; }
}

public class BiographieItem
{
    public int id { get; set; }
    public string type { get; set; }
    public string typeName { get; set; }
    public string title { get; set; }
    public string subtitle { get; set; }
    public string text { get; set; }
}

public class Sliders
{
    public string ex { get; set; }
    public int id { get; set; }
    public string date { get; set; }
    public List<SliderItem> items { get; set; }
}

public class SliderItem
{
    public int id { get; set; }
    public string title { get; set; }
    public string subtitle { get; set; }
    public string image_url { get; set; }
    public string link { get; set; }
    public string uri_link { get; set; }
}

public class DailyTruth
{
    public string ex { get; set; }
    public int id { get; set; }
    public string date { get; set; }
    public List<DailyTruthItem> items { get; set; }
}

public class DailyTruthItem
{
    public int id { get; set; }
    public string type { get; set; }
    public string typeName { get; set; }
    public string thumbnail_url { get; set; }
    public string title { get; set; }
    public string subtitle { get; set; }
    public string text { get; set; }
    public string media_url { get; set; }
}

public class PrayerWall
{
    public int id { get; set; }
    public string date { get; set; }
    public string text { get; set; }
    public PrayerWallUser user { get; set; }
}

public class PrayerWallUser
{
    public int id { get; set; }
    public string username { get; set; }
    public string location { get; set; }
    public string email { get; set; }
}

public class Video
{
    public int id { get; set; }
    public string date { get; set; }
    public string category { get; set; }
    public string thumbnail_url { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public string video_url { get; set; }
    public bool is_downloadable { get; set; }
    public bool login_required { get; set; }
}

public class Event
{
    public int id { get; set; }
    public string start_date { get; set; }
    public string end_date { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public string imageURL { get; set; }
    public string url { get; set; }
}
