﻿angular.module("umbraco")
.controller("ListPicker.PrevaluesController", function ($scope, $log) {

    //if (!$scope.model.value) {
    //    $scope.model.value = {
    //        type: '',
    //        items: []
    //    }
    //}

    $scope.update = function (position) {
        //$log.info();
        if ($scope.model.value.type !== 'checkbox') {
            angular.forEach($scope.model.value.items, function (item, index) {
                if (position != index)
                    item.checked = false;
            });
        }
    };

    $scope.add = function () {
        if ($scope.model.value.items == null) {
            $scope.model.value.items = [];
        }

        $scope.model.value.items.push({
            checked: false,
            text: '',
            value: ''
        });
    };

    $scope.delete = function (listValue) {
        if (confirm("Are you sure you want to delete this value fro the list?")) {
            var index = $scope.model.value.items.indexOf(listValue);
            $scope.model.value.items.splice(index, 1);
        }
    }

});