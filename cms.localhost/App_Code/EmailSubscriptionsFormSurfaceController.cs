﻿using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.Models;
using System.Linq;
using System;
using Custom.Models;

namespace Custom.Controllers
{
    public class EmailSubscriptionsFormSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {

        [ChildActionOnly]
        public ActionResult EmailSubscriptions()
        {
            //var currentNode = Umbraco.TypedContent(UmbracoContext.PageId.GetValueOrDefault());
            var model = new EmailSubscriptionsFormModel();

            model.FullName = "Enter your name";

            return PartialView("~/Views/Partials/EmailSubscriptionsFormViewModel.cshtml", model);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PostEmailSubscriptions(EmailSubscriptionsFormModel model)
        {
            // server validation here
            // TempData["ErrorMessage"] = "Error processing field ...";

            if (ModelState.IsValid)
            {
                var currentNode = Umbraco.TypedContent(UmbracoContext.PageId.GetValueOrDefault());
                var homeNode = currentNode.AncestorsOrSelf().First();

                var sendEmailsFrom = homeNode.GetPropertyValue<string>("sendEmailsFrom") ?? "msarich@securedevsite.com";
                var sendEmailsTo = homeNode.GetPropertyValue<string>("sendEmailsTo") ?? "msarich@securedevsite.com";

                var body = String.Format("From: {0}, Email: {1} and ListsToSubscribeTo: {2}", model.FullName, model.Email, model.ListsToSubscribeTo);
                var subject = "Email Subscription Form Message Sent";

                try
                {
                    umbraco.library.SendMail(sendEmailsFrom, sendEmailsTo, subject, body, true);

                    TempData["InfoMessage"] = "Your message has been successfully sent and we will be in touch soon...";

                    // Clear all the form fields
                    ModelState.Clear();
                    model.FullName = string.Empty;
                    model.Email = string.Empty;
                    model.ListsToSubscribeTo = string.Empty;

                    //redirect to current page to clear the form
                    return RedirectToCurrentUmbracoPage();
                }
                catch (Exception ex)
                {
                    TempData["ErrorMessage"] = ex.Message + ex.StackTrace;
                }
            }

            return CurrentUmbracoPage();

        }



    }
}