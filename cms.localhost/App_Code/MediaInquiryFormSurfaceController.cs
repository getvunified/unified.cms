﻿using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.Models;
using System.Linq;
using System;
using Custom.Models;

namespace Custom.Controllers
{
    public class MediaInquiryFormSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {

        [ChildActionOnly]
        public ActionResult MediaInquiry()
        {
            //var currentNode = Umbraco.TypedContent(UmbracoContext.PageId.GetValueOrDefault());
            var model = new MediaInquiryFormModel();

            model.FullName = "Enter your name";

            return PartialView("~/Views/Partials/MediaInquiryFormViewModel.cshtml", model);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PostMediaInquiry(MediaInquiryFormModel model)
        {
            // server validation here
            // TempData["ErrorMessage"] = "Error processing field ...";

            if (ModelState.IsValid)
            {
                var currentNode = Umbraco.TypedContent(UmbracoContext.PageId.GetValueOrDefault());
                var homeNode = currentNode.AncestorsOrSelf().First();

                var sendEmailsFrom = homeNode.GetPropertyValue<string>("sendEmailsFrom") ?? "msarich@securedevsite.com";
                var sendEmailsTo = homeNode.GetPropertyValue<string>("sendEmailsTo") ?? "msarich@securedevsite.com";

                var body = String.Format("FullName: {0}\r\nEmail: {1}\r\nPhone: {2}\r\nCompanyName: {3}\r\nWhoAreYou: {4}\r\nCountry: {5}\r\nWebSiteUrl: {6}", model.FullName, model.Email, model.Phone,
                   model.CompanyName, model.WhoAreYou, model.Country, model.WebSiteUrl);
                
                var subject = "Media Inquiry Form Message Sent";

                try
                {
                    umbraco.library.SendMail(sendEmailsFrom, sendEmailsTo, subject, body, true);

                    TempData["InfoMessage"] = "Your message has been successfully sent and we will be in touch soon...";

                    // Clear all the form fields
                    ModelState.Clear();
                    model.FullName = string.Empty;
                    model.Email = string.Empty;
                    model.Country = string.Empty;
                    model.Message = string.Empty;
                    //model.SubscribeToNewsletter = false;

                    //redirect to current page to clear the form
                    return RedirectToCurrentUmbracoPage();
                }
                catch (Exception ex)
                {
                    TempData["ErrorMessage"] = ex.Message + ex.StackTrace;
                }
            }

            return CurrentUmbracoPage();

        }



    }
}