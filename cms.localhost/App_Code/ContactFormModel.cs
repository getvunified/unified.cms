﻿using System.ComponentModel.DataAnnotations;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using umbraco.BusinessLogic;

namespace Custom.Models
{

    public class ContactFormModel 
    {
        [Required]
        public string FullName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        [Display(Name = "Enter a comment")]
        public string Message { get; set; }

        [Required]
        public bool SubscribeToNewsletter { get; set; }

    }

}