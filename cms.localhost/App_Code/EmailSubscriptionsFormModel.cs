﻿using System.ComponentModel.DataAnnotations;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using umbraco.BusinessLogic;

namespace Custom.Models
{

    public class EmailSubscriptionsFormModel
    {
        [Required]
        public string FullName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string ListsToSubscribeTo { get; set; }

    }

}