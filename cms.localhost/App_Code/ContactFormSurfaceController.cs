﻿using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.Models;
using System.Linq;
using System;
using Custom.Models;

namespace Custom.Controllers
{
    public class ContactFormSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {

        [ChildActionOnly]
        public ActionResult ContactForm()
        {
            //var currentNode = Umbraco.TypedContent(UmbracoContext.PageId.GetValueOrDefault());
            var model = new ContactFormModel();

            model.FullName = "Enter your name";

            return PartialView("~/Views/Partials/ContactFormViewModel.cshtml", model);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PostContactForm(ContactFormModel model)
        {
            // server validation here
            // TempData["ErrorMessage"] = "Error processing field ...";

            if (ModelState.IsValid)
            {
                var currentNode = Umbraco.TypedContent(UmbracoContext.PageId.GetValueOrDefault());
                var homeNode = currentNode.AncestorsOrSelf().First();

                var sendEmailsFrom = homeNode.GetPropertyValue<string>("sendEmailsFrom") ?? "msarich@securedevsite.com";
                var sendEmailsTo = homeNode.GetPropertyValue<string>("sendEmailsTo") ?? "msarich@securedevsite.com";

                var body = String.Format("FullName: {0}\r\nEmail: {1}\r\nSubscribeToNewsletter:\r\n {3}Message:\r\n {2}", model.FullName, model.Email, model.Message, model.SubscribeToNewsletter);
                var subject = "Contact Form Message Sent";

                try
                {
                    umbraco.library.SendMail(sendEmailsFrom, sendEmailsTo, subject, body, true);

                    TempData["InfoMessage"] = "Your message has been successfully sent and we will be in touch soon...";

                    // Clear all the form fields
                    ModelState.Clear();
                    model.FullName = string.Empty;
                    model.Email = string.Empty;
                    model.Country = string.Empty;
                    model.Message = string.Empty;
                    model.SubscribeToNewsletter = false;

                    //redirect to current page to clear the form
                    return RedirectToCurrentUmbracoPage();
                }
                catch (Exception ex)
                {
                    TempData["ErrorMessage"] = ex.Message + ex.StackTrace;
                }
            }


            return CurrentUmbracoPage();

        }



    }
}