﻿using System.ComponentModel.DataAnnotations;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using umbraco.BusinessLogic;

namespace Custom.Models
{

    public class PrayerRequestFormModel
    {
        [Required]
        public string FullName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string PrayerCategory { get; set; }

        [Required]
        public string CityStateZip { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        [Display(Name = "Enter a comment")]
        public string PrayerRequest { get; set; }

        [Required]
        public bool SubscribeToNewsletter { get; set; }


    }

}