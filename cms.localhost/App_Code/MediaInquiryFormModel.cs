﻿using System.ComponentModel.DataAnnotations;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using umbraco.BusinessLogic;

namespace Custom.Models
{

    public class MediaInquiryFormModel
    {
        [Required]
        public string FullName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public string CompanyName { get; set; }

        [Required]
        public string WhoAreYou { get; set; }

        [Required]
        public string WebSiteUrl { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        [Display(Name = "Enter a comment")]
        public string Message { get; set; }


    }

}